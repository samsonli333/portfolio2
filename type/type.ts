import { getProcess } from "../redux/store"


export type tvChannel = {
    genrecode: string
    genreName: string
    channelCode: string
    channelName: string
    channelDesc: string
    channelImage: string
    channelImageType: string
}



export type tvChannelList = {
    code: number
    msg: string
    resultList: tvChannel[]
}

//////////////////////////////////////////////////////////////////////////////////////

export type premiumList = {
    premiumCode: string
    premiumName: string
    is_free: boolean
    free_detail: string | null
    value: string | null
    premiumImage: string
    premiumImageType: string
    grouping: string
}

export type planServiceResult2 = {
    planType: string
    is_new: boolean
    is_hot: boolean
    planCode: string
    planImage: string
    planImageType: string
    planCategory: string
    planName: string
    priceAdv: number
    monthlyFee: number
    contractPeriod: string
    introTxt: string | null
    introTxtRed: string | null
    planContent: string | null
    planRemark: string | null
    free: string | null
    freeDetail: string | null
    addonList: string | null
    premiumList: premiumList[]
    genreList: string | null
    converterRental: string | null
    planImgUrl: string
}

export type planServiceList2 = {
    code: number | ""
    msg: string
    result: planServiceResult2
}

export type personalData = {
     Gender:string
     Surname:string
     Givenname:string
     IdentityNo:string
     IdentityNo2:string
     passBookOn:boolean | undefined |string
     passBookNo:string
     dateOfBirthYrs:string
     dateOfBirthMth:string
     dateOfBirthDay:string
     AppoDate:string | undefined
     AppoTime:string | undefined
     AppoCal:string
     fullAddress:string
     isntFloor:string
     isntRoom:string
     CommsLang:string
     ContactMobile:string
     ContactHome:string
     email:string
     CommAddress:string
     Lang:string
     AddrDiff:boolean | undefined | string
     diffAddrTxt:string
}

export type visaData = {
    cardNo:string
    cardHolder:string
    expiryDate:string
}

////////////////////////////////////////////////////////////////////////////////////////////////////

export type banner = {
    bannerImage: string
    bannerImageType: string
    bannerName: string
  }
  
export type getBanner = {
    code: number
    msg: string
    resultList: banner[]
  }


export type bannerProp = {
    banner:banner[] | undefined
  }


/////////////////////////////////////////////////////////////////////////////////////////////////////////


export type addResultList = {
    sid: string,
    full_address: string,
    weighting: number
}

export type addResult = {
    code: number,
    msg: string,
    resultList: Array<addResultList>
}


export type getClassId = {
    code: number
    msg: string
    result: {
        cssClass: string
        htmlID: string
    }
}

export type IdAndClass = {
    class: { class1: getClassId | undefined, class2: getClassId | undefined }
}


//////////////////////////////////////////////////////////////////////////////////////////////

export type getImage = {
    code: number
    msg: string
    result: string
}


//////////////////////////////////////////////////////////////////////////////////////////////

export type success = {
    code: number
    msg: string
    result: string
}



//////////////////////////////////////////////////////////////////////////////////////////////

export type processNewSub = {
    code: number
    msg: string
    result: { processID: string, status: string }
}

export type processSubP1 = {
    code: number
    msg: string
    result: { processID: string, status: string }
}

export type errMsgOn = {
    errMsgOn1: boolean, errMsgOn2: boolean, errMsgOn3: boolean, errMsgOn4: boolean,
    errMsgOn5: boolean, errMsgOn6: boolean, errMsgOn7: boolean, errMsgOn8: boolean,
    errMsgOn9: boolean, errMsgOn10: boolean, errMsgOn11: boolean, errMsgOn12: boolean, errMsgOn13: boolean
    errMsgOn14: boolean, errMsgOn19: boolean
}

export type Appointment = { calId: string, dateStr: string, timeStr: string, timeId: string }


export type getAppointment = {
    code: number
    msg: string
    resultList: Appointment[]
}



///////////////////////////////////////////////////////////////////////////////////////////////////////


export type Token = {
    code: number
    msg: string
    result: { token_id: string, co_branded: string }
}

export type SubP2 = {
    code: number
    msg: string
    result: { status: string }
}

export type PlanPayment = {
    groupNum: number
    groupName: string
    itemNum: number
    itemName: string
    priceStr: string
}


export type getPlanPaymentInfo = {
    code: number
    msg: string
    resultList: PlanPayment[]
}

export type page4ErrMsgOn = {
    errMsgOn1: boolean, errMsgOn2: boolean, errMsgOn3: boolean
}

export type page4ErrMsgContent = {
    errMsgContent1: string, errMsgContent2: string, errMsgContent3: string
}


///////////////////////////////////////////////////////////////////////////////////////////

export type CardInfo = { Title: string, Total: string, Detail: { Content: string, Amount: string, underLine: boolean }[] } 


////////////////////////////////////////////////////////////////////////////////////////////

export type onChange = { onChange1: (x: any) => void, onChange2: (x: any) => void, onChange3: (x: any) => void ,onChange4:(x:any) => void , onChange5:(x:any) => void}
export type value = { value1: string | undefined, value2: string|undefined, value3: string|undefined }

/////////////////////////////////////////////////////////////////////////////////////////////

export type CardInfoPage5 = { Title: string, Item: string, Total: string, Detail: { Content: string, Amount: string, underLine: boolean }[] }


export type info = {
    info: (getProcess["result"]) | undefined

}

export type TncContent = {
    tncID: number
    content: string
    is_Compulsory: boolean
    is_Checked: boolean
}

export type Tnc = {
    code: number,
    msg: string,
    result: {
        tnc: string
        tncDetails: TncContent[]
    }
}

export type planSubmit = {
    code: number
    msg: string
    result: { status: string }
}