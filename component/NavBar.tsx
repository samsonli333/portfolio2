import React, { Dispatch, ReactNode } from 'react'
import { connect, ConnectedProps } from 'react-redux';
import { Logo } from '../public/photo/photo';
import { CombineRootState } from '../redux/store';
import Customer from './Customer';
import Image from 'next/image'
import {withRouter} from 'next/router'
import { WithRouterProps } from 'next/dist/client/with-router';


type Props = {
    children?: ReactNode
} & WithRouterProps & CombineRootState["switchCom"] & ConnectedProps<typeof connector>

class NavBar extends React.Component<Props> {
    constructor(props: Props) {
        super(props)
    }

    backtoPage1 = () => {
        // store.dispatch({ type: "serAdr", serAdr: "" })
        this.props.serAdr({ type: "serAdr", serAdr: "" })
        this.props.router.push('/')
    }



    public render(): React.ReactNode {
        return (
            <>
                <section className="section">
                    <div className="nav d-flex justify-content-between align-items-center">
                        <div className="d-flex justify-content-between align-items-center customer">
                        <div className="d-flex align-items-center align-items-center">
                            <Image className="cursor" onClick={this.backtoPage1} src={Logo} alt=""/>
                            <span className="section1-txt">網上登記</span>
                        </div>
                        <Customer/>
                        </div>
                        <div className="stepper">{this.props.children}</div>
                    </div>
                </section>
            </>
        )
    }
}

const MaptoState = (state: CombineRootState) => {
    return state.switchCom
}

const MaptoDispatch = (dispatch:Dispatch<any>) => ({
    serAdr:(x:{ type: "serAdr", serAdr: "" }) => dispatch({...x})
})


const connector = connect(MaptoState,MaptoDispatch)
export default withRouter(connector(NavBar));