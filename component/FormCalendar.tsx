import React from 'react'
import Image from 'next/image'
import CalendarCustom from 'react-calendar'
import dayjs from 'dayjs'
import { calendar } from '../public/photo/photo';
import { Appointment } from '../type/type';




interface State {
    open: Boolean
}

type Prop = {
    title: string,
    title2: string | undefined,
    Appointment: Appointment[],
    onChange?: (x: any) => void
    errMsgOn?:boolean
}

class FormCalendar extends React.Component<Prop, State>{
    constructor(props: Prop) {
        super(props)
        this.state = {
            open: false,
        }
    }

    openCalen = () => this.setState((state: State) => ({ open: !state.open }))


    public render() {
        return (
            <div className={`${(this.props.errMsgOn && !this.props.title2) && "b-red"} w-100 border border-secondary border-opacity-50 rounded-2`}>
                <label onClick={this.openCalen}  className={`formlabel d-flex justify-content-between 
                align-items-center p-2 calendar-padding-font ${this.state.open && "rounded-2 bg-secondary bg-opacity-25"}`}>
                    <div className="d-flex flex-column">
                        <span className="special-color text-secondary text-opacity-50 w-100 fs-7">{this.props.title}</span>
                        <span className="w-100">{this.props.title2 || "您的預約安裝日期"}</span>
                    </div>
                    <Image className="formlabel-img" src={calendar} alt=""/>
                </label>
                {this.state.open && <div className="mt-3"><MyCalendar sendTime={this.openCalen} onChange={this.props.onChange} Appointment={this.props.Appointment} /></div>}
            </div>
        )
    }
}

type Prop2 = {
    sendTime(): void
    onChange?: (x: any) => void
    Appointment: Appointment[]
}

const MyCalendar = (props: Prop2) => {
    const installRange = new Set<string>()
    props.Appointment.forEach((item) => {
        installRange.add(item.dateStr)
    })
    const Arr = Array.from(installRange)
    const formatShortWeekday = (locale: any, date: Date) => ['日', '一', '二', '三', '四', '五', '六'][date.getDay()]
    const formatDay = (locale: any, date: Date) => dayjs(date).format('D')

    return <>
        <div>
            <CalendarCustom minDate={new Date(Arr[0]||0)} maxDate={new Date(Arr[Arr.length - 1]||0)}
                onChange={(e: any) => { props.onChange?.(dayjs(e).format('YYYY-MM-DD'));setTimeout(props.sendTime, 100) }}
                formatShortWeekday={formatShortWeekday} formatDay={formatDay} calendarType="Hebrew" showNeighboringMonth={false} />
        </div>
    </>
}


export default FormCalendar;


