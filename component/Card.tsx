import React , {Dispatch} from 'react'
import { Collapse } from 'react-bootstrap';
import {planServiceResultList } from '../redux/store';
import { tvChannel } from '../type/type';
import { withRouter } from 'next/router'
import { WithRouterProps } from 'next/dist/client/with-router';
import { connect ,ConnectedProps} from 'react-redux';



type Props = {
    outline: boolean,
    caption: boolean,
    planSerResult: planServiceResultList,
    planCode?: string
    smImgList: tvChannel[] | null
    planSid:string | undefined
} & WithRouterProps & ConnectedProps<typeof connector>

interface State {
    collap: boolean
}

class Card extends React.Component<Props, State>{
    public state: State
    public smImgList: tvChannel[] | null
    constructor(props: Props) {
        super(props)
        this.state = {
            collap: false,
        }
        this.smImgList = this.props.smImgList;
    }


    collapSwitch = () => {
        this.setState((state: State) => ({ collap: !state.collap }))
    }


    gotoPage3 = (planCode: string | undefined,planSid:string | undefined) => {
        // store.dispatch({ type: "selectedPlanCode", selectedPlanCode: planCode })
        this.props.selectedPlanCode({ type: "selectedPlanCode", selectedPlanCode: planCode })
        this.props.router.push(`/page3?selectedPlanCode=${planCode}&planSid=${planSid}`)
    }

    public render(): React.ReactNode {
        return (
            <>
                <div className={`${(this.props.outline || this.props.planSerResult?.is_hot) && "b-red"} mx-auto my-3 card-outer shadow`}>
                    {(this.props.caption || this.props.planSerResult?.is_hot) && <div className="text-white db-red text-center card-title">最佳選擇</div>}
                    <div className="p-3">
                        <div className="d-flex justify-content-center">
                            <div className="w-25 me-3 outer-photo"><img src={`${this.props.planSerResult.planImage}`} className="card-photo" /></div>
                            <section className="section-left d-flex card-item align-items-start align-content-start flex-wrap w-75">
                                <div className="text">
                                    <span className="border-right ps-2">{this.props.planSerResult?.planCategory}</span><br />
                                    <span className="fs-5 fw-bold text-dark">{this.props.planSerResult?.planName}</span><br />
                                    <div className="text3 contract-hide-large">{`${this.props.planSerResult?.contractPeriod} 合約`}</div>
                                </div>
                                <div className="text-end text3">
                                    <span className="dt-red fs-4 fw-bold">{`$${this.props.planSerResult?.monthlyFee}`}</span><span className="fs-6">/月</span>
                                </div>
                                <div className="text3 contract-hide-small">{`${this.props.planSerResult?.contractPeriod} 合約`}</div>
                                <div className="text3 dt-red">{this.props.planSerResult.introTxtRed}</div>
                            </section>
                        </div>
                        <Collapse in={this.state.collap}>
                            <div>
                                <div style={{ height: "1px" }} className="mt-3 mb-4 bg-opacity-25 bg-secondary"></div>
                                {this.props.planSerResult.introTxt?.split('\n').map(item => <div className="text-dark fs-7">{item}</div>)}
                                <div className="mt-3"></div>
                                {this.props.planSerResult.planContent?.split('\n').map(item => <div className="text-dark fs-7">{item}</div>)}
                                <div className="mt-4 text-dark fs-8">{this.props.planSerResult.planRemark}</div>
                            </div>
                        </Collapse>
                        {<div className="align-items-center card-btn">
                            <div className="cursor dt-red b-red me-2 rounded-pill p-2 px-3 card-btn-width d-flex justify-content-between align-items-center text-nowrap">
                                {this.state.collap ? <span onClick={this.collapSwitch} className="d-iline-block me-3">顯示較少</span> : <span onClick={this.collapSwitch} className="d-inline-block me-2">詳情</span>}
                                {this.state.collap ? <i className="fa-solid fa-angle-up dt-red"></i> : <i className="fa-solid fa-angle-down dt-red"></i>}
                            </div>
                            <div onClick={() => this.gotoPage3(this.props.planCode,this.props.planSid)} className="text-white db-red p-2 px-3 rounded-pill text-nowrap cursor">申請此服務</div>
                        </div>}
                    </div>
                </div>
            </>
        )
    }
}

const CardCollapse = (props: { open: boolean, bigImg: string | undefined, bigImgType: string, smImgList: tvChannel[] | undefined }) => {
    const Jpeg = `data:image/${props.bigImgType};base64,`
    const image = Jpeg + props.bigImg
    return (
        <>
            {props.open && <div className="border-0 border-top mt-3 py-2">
                <Collapse in={props.open}>
                    <div id="example-collapse-text">
                        <div className="card-collapse-text text-dark text-opacity-75">
                            44 條頻道<br />優惠期 12個月<br />計劃月費 $ 58<br /><br /><span className="card-collapse-text2">送迎新禮品包括︰</span><br />
                            <img className="card-collapse-photo" src={image} /><br />
                            歡樂組合 ( 44 條)
                        </div>
                        <div className="d-flex flex-wrap mt-2">
                            {props.smImgList?.map((item) => {
                                console.log(item)
                                return (
                                    <div className="card-collapse-photoframe">
                                        <img className="card-collapse-smallphoto" src={`data:image/${item.channelImageType};base64,${item.channelImage}`} />
                                    </div>
                                )
                            })}
                        </div>
                    </div>
                </Collapse>
            </div>}
        </>
    );
}

const MaptoDispatch = (dispatch:Dispatch<any>) => ({
    selectedPlanCode:(x:{ type: string, selectedPlanCode:string|undefined }) => dispatch({...x})
})

const connector = connect(null,MaptoDispatch)
export default withRouter(connector(Card));