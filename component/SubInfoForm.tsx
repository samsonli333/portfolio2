import React from 'react'
import { errMsgOn, personalData } from '../type/type';
import Form3 from './Form3';
import Form from './Form'
import FormRadio from './FormRadio';
import Term from './Term';
import SelectItem from './SelectItem';


interface State {
    Arr: { title1: string, title2: string[], disabled: boolean ,value:string | undefined}[]
}

type Prop = {
    nameGroup: {
        name1: string
        name2: string
        name3: string
        name4: string
        name5: string
        name6: string
        name7: string
        name8: string
    }
    tranFunc: (x: string) => string
    errMsgOn: errMsgOn
    errIdContent: string,
    PbookonChange?(): void
    PbookOn: boolean | undefined
    innerRef?: React.ForwardedRef<any>
    personalData:personalData | undefined
}

const BrithDate: string[] = []
for (let x = 1; x < 32; x++) {
    BrithDate.push(x.toString())
}
const BrithMonth: string[] = []
for (let x = 1; x < 13; x++) {
    BrithMonth.push(x.toString())
}
const BrithYear: string[] = []
for (let x = 1942; x < 2005; x++) {
    BrithYear.push(x.toString())
}

class SubInfoForm extends React.Component<Prop, State> {
    public state: State
    constructor(props: Prop) {
        super(props)
        this.state = {
            Arr: [{ title1: "出生日期", title2: ["---請選擇---", ...BrithDate], disabled: false, value:this.props.personalData?.dateOfBirthDay},
            { title1: "出生月份", title2: ["---請選擇---", ...BrithMonth], disabled: false ,value:this.props.personalData?.dateOfBirthMth},
            { title1: "出生年份", title2: ["---請選擇---", ...BrithYear], disabled: false,value:this.props.personalData?.dateOfBirthYrs}],
        }
    }

    public render(): React.ReactNode {
        const { errMsgOn2, errMsgOn3, errMsgOn4, errMsgOn5, errMsgOn6, errMsgOn7 } = this.props.errMsgOn
        return (
            <div ref={this.props.innerRef} className="subinfoform">
                <div className="border-0 border-start b-red border-3 ps-2 mb-2 fs-5 fw-semibold">基本資料</div>
                <FormRadio  selected={this.props.personalData?.Gender} errMsgOn={errMsgOn2} title={'性別'} name={this.props.nameGroup.name1} selectOption={["女","男"]} tranFunc={this.props.tranFunc}/>
                <div className="d-flex mt-2">
                    <Form3 spanName={"姓氏"} htmlFor={"info1"} id={"info1"} errMsgOn={errMsgOn3} name={this.props.nameGroup.name2} inputBackground={"bg-transparent"} width={"w-25"} value={this.props.personalData?.Surname} errMsgContent={''} />
                    <div className="me-2"></div>
                    <Form3 spanName={"名字"} htmlFor={"info2"} id={"info2"} errMsgOn={errMsgOn4} name={this.props.nameGroup.name3} inputBackground={"bg-transparent"} width={"w-75"} value={this.props.personalData?.Givenname} errMsgContent={''} />
                </div>
                <div className="row mt-2">
                    <Form spanName={"您的身份證號碼"} enable={this.props.PbookOn} htmlFor={"info3"} errMsgOn={this.props.PbookOn ? false : errMsgOn5} swType={false} id={"info3"} disabled={this.props.PbookOn ? true : false} name={this.props.nameGroup.name4} inputBackground={`${this.props.PbookOn ? "bg-secondary bg-opacity-25" : "bg-transparent"}`} width={"col-10 hkid1"} value={this.props.PbookOn ? "" : (this.props.personalData?.IdentityNo || "")} errMsgContent={this.props.PbookOn ? "" : "證件號碼不正碓"} specialClass={this.props.PbookOn ? "hkid" : ""} maxLength={8} />
                    <div className="d-flex justify-content-around align-items-center col-2 hkid2">
                        <span className="me-2">(</span>
                        <Form spanName={""} enable={this.props.PbookOn} errMsgOn={this.props.PbookOn ? false : errMsgOn5} swType={false} htmlFor={"info4"} id={"info4"} name={this.props.nameGroup.name5} inputBackground={`${this.props.PbookOn ? "bg-secondary bg-opacity-25" : "bg-transparent"}`} width={"w-100"} value={this.props.PbookOn ? '' : (this.props.personalData?.IdentityNo2 || "")} disabled={this.props.PbookOn ? true : false} errMsgContent={this.props.PbookOn ? "" : <span className="text-light">AB</span>} specialClass={this.props.PbookOn ? "hkid" : ""} maxLength={1} />
                        <span className="ms-2">)</span>
                    </div>
                </div>
                <div className="text-primary fs-8">Demo HKID : "AB9876543" / Real HKID for test</div>
                <div className="mt-2"></div>
                <Term termContent={"我想使用護照號碼代替身份證號碼。"} onChange={this.props.PbookonChange} name={this.props.nameGroup.name6} errMsgContent={''} value={this.props.PbookOn} errMsgOn={false} checked={this.props.PbookOn}/>
                {this.props.PbookOn && <div className="mb-2"><Form htmlFor={"info5"} id={"info5"} errMsgOn={errMsgOn5} spanName={'您的護照號碼'} inputBackground={"bg-transparent"} width={"w-100"} value={this.props.personalData?.passBookNo} name={this.props.nameGroup.name7} errMsgContent={''} /></div>}
                <div className="row">
                    {this.state.Arr.map((item, i) => {
                        return <div className={`${i === 2 ? "col-4 birth-select2" : "col-4 birth-select"}`}><SelectItem key={i} errMsgOn={errMsgOn7} title1={item.title1} title2={item.title2} value={item.value} disabled={item.disabled} name={this.props.nameGroup.name8 + i} /></div>
                    })}
                </div>
                <div className="subinfo-text">可選擇是否填寫"月"及"日"</div>
            </div>
        )
    }
}

export default React.forwardRef((props: Prop, ref: React.ForwardedRef<any>) => <SubInfoForm {...props} innerRef={ref} />);


function tranFunc2(item: string|undefined) {
    switch (item) {
        case "M":
            return "男"
        case "F":
            return "女"
        default:
            return undefined
    }
}