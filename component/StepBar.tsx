import React from 'react'

type Prop = {
   className1:string|null
   className2:string|null
   className3:string|null
}

class StepBar extends React.Component<Prop>{
    constructor(props:Prop){
        super(props)
    }

    public render(): React.ReactNode {
        return(
            <div className="col-8 me-3 stepbar-width">
                <div className={`${this.props.className1?this.props.className1:""} rounded-start d-inline-block step bg-secondary bg-opacity-25 position-relative`}></div>
                <div className={`${this.props.className2?this.props.className2:""} d-inline-block step2 bg-secondary bg-opacity-25 position-relative`}></div>
                <div className={`${this.props.className3?this.props.className3:""} rounded-end d-inline-block step3 bg-secondary bg-opacity-25 position-relative`}></div>
            </div>
        )
    }
}

export default StepBar;