import React from 'react'
import { ImageMaster, ImageVisa } from '../public/photo/photo';
import { getClassId, page4ErrMsgContent, page4ErrMsgOn, visaData } from '../type/type';
import Form from './Form';
import Image from 'next/image'
import VisaForm from './VisaForm';




type Prop = {
    value: { value1: string | undefined, value2: string | undefined }
    onChange?: { onChange1?: (x: string) => void, onChange2?: (x: string) => void }
    name: { name1: string, name2: string, name3: string }
    errMsgOn?: page4ErrMsgOn
    errMsgContent: page4ErrMsgContent
    cardType?:string
    visaData?:visaData
    class?: getClassId | undefined
} 

interface State {
    class: getClassId | undefined
}

class PaymentInfo extends React.Component<Prop , State>{
    constructor(props: Prop ) {
        super(props)
        this.state = {
            class: undefined
        }
    }


    public render() {
        return (
            <>
                <div className="">
                    <div className="border-0 border-start border-3 b-red px-2 mb-3"><span className="fs-5 fw-bold">付款資料</span></div>
                    <div className="fs-6">服務成功安裝及啟動後，「有線電視」或「有線寬頻」才會收取費用 (有關費用並不會馬上支付)。</div>
                    <div className="visa-narrow m-auto">
                        <div className="p-3"><Visa errMsgContent={this.props.errMsgContent} cardType={this.props.cardType} visaData={this.props.visaData} name={this.props.name} value={this.props.value} onChange={this.props.onChange} errMsgOn={this.props.errMsgOn} /></div>
                        <div className="mt-3 mb-5 fs-8 text-dark">此網站己使用SSL憑證保護您的私人資訊，與以https開頭的任何位址進行交換的資訊在傳輸之前都將使用SSL加密。<br /><br />
                            本人授權香港有線電視有限公司/ 有線寬頻電訊有限公司由上信用卡賬戶 (或日後由本人或發卡機構重新提供之信用卡) 收取「有線電視」及「有線寬頻」之一切有關服務費用。此授權書於本人的信用卡有效期後仍然生效。</div>
                        <button id={this.props.class?.result.htmlID} className={`${this.props.class?.result.cssClass} w-100 border-0 db-red rounded-pill py-2 px-5 text-white text-center mb-5`}>下一步</button>
                    </div>
                </div>
            </>
        )
    }
}


const Visa = (prop: Prop) => {
    console.log("visaData",prop.visaData)
    return (
        <div>
            <div className="py-4 rounded-5 m-auto border-2 border border-secondary border-opacity-50">
                <div className="py-2 bg-secondary bg-opacity-50 mb-2"></div>
                <div className="px-2 pt-4 pb-2 visa-form">
                    <VisaForm errMsgContent={prop.errMsgContent.errMsgContent1} spanName={'信用咭號碼'} htmlFor={"paymentinfo1"} id={"paymentinfo1"} errMsgOn={prop.errMsgOn?.errMsgOn1} name={prop.name.name1} value={prop.value.value1} onChange={prop.onChange?.onChange1} inputBackground={prop.cardType || ""} width={'w-100'} placeholder="0000 0000 0000 0000" />
                    <div className="text-primary fs-8">Demo cardNo : 4916108926268679</div>
                    <div className="mb-2"></div>
                    <div className="d-flex">
                        <Form spanName={'持咭人姓名'} errMsgContent={prop.errMsgContent.errMsgContent2} htmlFor={"paymentinfo2"} id={"paymentinfo2"} errMsgOn={prop.errMsgOn?.errMsgOn2} inputBackground={''} width={'w-75'} value={prop.visaData?.cardHolder} name={prop.name.name2} placeholder=" "/>
                        <div className="me-2"></div>
                        <VisaForm spanName={'有效期至'} errMsgContent={prop.errMsgContent.errMsgContent3} htmlFor={"paymentinfo3"} id={"paymentinfo3"} errMsgOn={prop.errMsgOn?.errMsgOn3} name={prop.name.name3} inputBackground={''} onChange={prop.onChange?.onChange2} width={'w-25 width-35'} value={prop.value.value2} placeholder="MM/YY" />
                    </div>
                </div>
            </div>
            <div className="d-flex align-items-center">只接受
            <Image className="visa-img me-2" src={ImageVisa} alt=""/>
             <Image className="master-img" src={ImageMaster} alt=""/>
             </div>
        </div>
    )
}


export default PaymentInfo;
