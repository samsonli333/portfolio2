import React from 'react'
import Image from 'next/image'
import { featureWhatsapp } from '../public/photo/photo';





class Whatsapp extends React.Component{
    constructor(props:{}){
        super(props)
    }

    public render(): React.ReactNode {
        return(
            <>
            <a id="go-to-whatsapp" href="https://wa.me/85267043035?text=Instant%20Inquiry%20即時詢問" target="_blank" className="whatsApp_float">
              <Image src={featureWhatsapp} alt="" width={60} height={60}/>
            </a>
            </>
        )
    }
}

export default Whatsapp;