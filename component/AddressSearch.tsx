import React ,{Dispatch} from 'react'
import { connect ,ConnectedProps} from 'react-redux'
import  { CombineRootState, planServiceList} from '../redux/store'
import {withRouter} from 'next/router'
import { WithRouterProps } from 'next/dist/client/with-router'
import { addResultList } from '../type/type'


type Prop = {resultList: addResultList[]} & CombineRootState["switchCom"] & CombineRootState["miscell"] & WithRouterProps & ConnectedProps<typeof connector>



class AddressSearch extends React.Component<Prop >{
    constructor(props: Prop) {
        super(props)
    }

    goToSignup = (add: string | undefined) => {
        // Store.dispatch({ type: "serAdr", serAdr: add })
        this.props.signup({ type: "serAdr", serAdr: add })
        this.props.router.push(`/page2?planSid=${this.props.planSid}`)
    }


    inquiry = async (fullAdr: string) => {
        let Body = {}
        let finalSite: string = ""
        this.props.resultList.filter((item) => item.full_address === fullAdr).map((item) => {
            finalSite = item.full_address
            // Store.dispatch({ type: "planSid", planSid: item.sid })
            this.props.inquiry({ type: "planSid", planSid: item.sid })
            return Body = {
                sid: item.sid,
                lang: "C"
            }
        })
        const res = await fetch(`${process.env.NEXT_PUBLIC_API3}`, { method: "POST", headers: { "Content-type": "application/json" }, body: JSON.stringify(Body) })
        const result: planServiceList = await res.json()
        console.log(result)
        if (result.code === 0) this.props.planService({ type: "planService", planService: result })
        this.goToSignup(finalSite)
    }


    componentWillUnmount = () => {
        window.scroll({
            top: 0,
            behavior: "smooth"
        })
    }


    public render(): React.ReactNode {
        return (
            <>
                <div className="w-100">
                    <ul className="px-4 pb-2 w-100 bg-white border border-top-0 m-0 address-search">
                        {this.props.resultList.map((result, i) => {
                            return <li key={i} onClick={(result.full_address === "找不到相關資料") ? () => {} : () => this.inquiry(result.full_address)} className='border-bottom p-2 cursor'>{result.full_address}</li>
                        })}
                    </ul>
                    <div style={{ fontSize: "15px" }} className="address-title-bottom text-dark rounded-bottom shadow border border-1 border-top-0">如並沒有合適的結果，建議輸入更多關鍵字或利用<span onClick={() => this.props.swCom({ type: "com2", com2: true })} className="address-search-text cursor">「地址分類搜尋」</span>以便搜索地址</div>
                </div>
            </>
        )
    }
}

const MaptoState = (state: CombineRootState) => {
    return {
        ...state.switchCom,
        ...state.miscell
    }
}

const mapDispatchToProps = (dispatch: Dispatch<any>) => ({
    signup: (x:{type:string,serAdr:string|undefined}) => dispatch({...x}),
    inquiry:(x:{ type:string, planSid:string }) => dispatch({...x}),
    planService:(x:{ type:string, planService: planServiceList }) => dispatch({...x}),
    swCom:(x:{ type: string, com2: boolean }) => dispatch({...x})
});

const connector = connect(MaptoState,mapDispatchToProps)
export default withRouter(connector(AddressSearch));