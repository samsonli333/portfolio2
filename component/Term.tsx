import React from 'react'

type Prop = {
    termContent: string | JSX.Element
    name: string
    errMsgOn: boolean
    errMsgContent: string
    onChange?(): void
    value?:boolean
    checked?:boolean
}

interface State {
    value: boolean
}

class Term extends React.Component<Prop, State>{
    public state: State
    constructor(props: Prop) {
        super(props)
        this.state = {
            value: false
        }
    }

    public render(): React.ReactNode {
        let x;
        return (
            <div className={`d-flex checkbox ${(this.props.errMsgOn && !this.state.value) && "checkbox-error"}`}>
                <div className="me-3"><input name={this.props.name} value={this.props.value?.toString()} onChange={this.props.onChange || (() => this.setState(state => ({ value: !state.value })))} className="position-relative" type="checkbox" checked={this.props.checked}/></div>
                <div>
                    <div className="text">{this.props.termContent}</div>
                    {(this.props.errMsgOn && !this.state.value) ? <div className="text-danger fs-8 error-text">{this.props.errMsgContent || "請輸入你的地址關鍵詞 (如大廈屋苑名稱)"}</div> : ""}
                </div>
            </div>
        )
    }
}

export default Term;