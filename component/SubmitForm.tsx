import React from 'react'
import Form from './Form';

type Prop = {
    errMsg: {
        errMsg1: { errMsgOn: boolean, errMsgContent: string, errFunc: (email: string) => "" | JSX.Element ,swFunc:boolean},
        errMsg2: { errMsgOn: boolean, errMsgContent: string, errFunc: (emaii: string) => "" | JSX.Element ,swFunc:boolean},
        errMsg3: { errMsgOn: boolean, errMsgContent: string, errFunc: (email: string) => "" | JSX.Element ,swFunc:boolean},
        errMsg4: { errMsgOn: boolean, errMsgContent: string, errFunc: (email: string) => "" | JSX.Element ,swFunc:boolean},
        errMsg5: { errMsgOn: boolean, errMsgContent: string, errFunc: (email: string) => "" | JSX.Element ,swFunc:boolean},
        errMsg6: { errMsgOn: boolean, errMsgContent: string, errFunc: (email: string) => "" | JSX.Element ,swFunc:boolean}
    },
    name: { name1: string, name2: string, name3: string, name4: string },
}

class SubmitForm extends React.Component<Prop>{
    constructor(props: Prop) {
        super(props)
    }

    public render(): React.ReactNode {
        return (
            <>
                <div className="my-5">
                    <Form value={""} name={this.props.name.name1} inputBackground={""} spanName={"您的姓名"} width={""} errMsgOn={this.props.errMsg.errMsg2.errMsgOn} errMsgContent={'請輸入正確的姓名'} />
                    <div className="m-2"></div>
                    <div className="d-flex my-2">
                        <Form value={"+852"} name={this.props.name.name2} inputBackground={"district-code"} spanName={"區號"} width={"w-25"} errMsgOn={this.props.errMsg.errMsg2.errMsgOn} errMsgContent={''} />
                        <div className="mx-1"></div>
                        <Form value={""} name={this.props.name.name3} inputBackground={""} spanName={"您的聯絡電話"} width={"w-75"} swFunc={this.props.errMsg.errMsg3.swFunc} errFunc={this.props.errMsg.errMsg3.errFunc} errMsgOn={this.props.errMsg.errMsg3.errMsgOn} errMsgContent={'請輸入正確的聯絡電話'} />
                    </div>
                    <Form value={""} name={this.props.name.name4} inputBackground={""} spanName={"您的電郵地址"} width={""} swFunc={this.props.errMsg.errMsg4.swFunc} errFunc={this.props.errMsg.errMsg4.errFunc} errMsgOn={this.props.errMsg.errMsg4.errMsgOn} errMsgContent={'請輸入正確的電郵地址'}/>
                </div>
            </>
        )
    }
}



export default SubmitForm;