import {useState } from "react";
import { Carousel } from "react-bootstrap";
import { bannerProp , banner } from "../type/type";
import Spinner from "./Spinner";




const CarousellCus = ({banner}:bannerProp) => {
  const [index, setIndex] = useState(0);
  const [Image, setImage] = useState<(banner[]) | undefined>(banner)


  function select(e: any) {
    setIndex(e)
  }

  return (
    <>
      <div className="position-relative carosell">
        {Image === undefined ? <Spinner /> : (<><div className="carousel1">
          <Carousel onSlide={select}  variant="dark" indicators={true} className="position-absolute start-0 top-0 w-100 h-100">
            {Image.map((item, i) => {
              return (
                // <Carousel.Item key={i} interval={3500} style={{ backgroundImage: `url(data:image/${item.bannerImageType};base64,${item.bannerImage})` }} className="outer-img h-100">
                // </Carousel.Item>
                 <Carousel.Item key={i} interval={3500} style={{ backgroundImage: `url(${item.bannerImage})` }} className="outer-img h-100">
                 </Carousel.Item>
                )
            })}
          </Carousel>
        </div>
          <div className=" carousel2">
            <Carousel activeIndex={index}  pause={false} variant="dark" prevIcon={false} nextIcon={false} indicators={false} slide={false} className="position-absolute carousel2-ratio start-50 top-50 translate-middle h-100">
              {Image.map((item, i) => {
                return (
                  // <Carousel.Item style={{ backgroundImage: `url(data:image/${item.bannerImageType};base64,${item.bannerImage})` }} className="inner-img2 h-100">
                  // </Carousel.Item>
                    <Carousel.Item style={{ backgroundImage: `url(${item.bannerImage})` }} className="inner-img2 h-100">
                    </Carousel.Item>
                  )
                  
              })}
            </Carousel>
          </div></>)}
      </div>
    </>
  )
}

export default CarousellCus;


