import React, { Dispatch } from 'react'
import { connect ,ConnectedProps} from 'react-redux';
import { CombineRootState } from '../redux/store';
import { Tnc } from '../type/type';

type Props = {
    info: string
} & CombineRootState["miscell"] & ConnectedProps<typeof connector>

interface State {
    info: string
}

class TAndC extends React.Component<Props, State>{
    public state: State
    constructor(props: Props) {
        super(props)
        this.state = {
            info: this.props.info
        }
    }


    close = () => {
        const arr: { a: string, b: boolean }[] = [{ a: "com1", b: false }, { a: "com2", b: false }, { a: "com3", b: false },
        { a: "com4", b: false }, { a: "com5", b: false }, { a: "com6", b: false }, { a: "com7", b: false }, { a: "com8", b: true },
        { a: "com9", b: false }, { a: "com10", b: false }, { a: "com11", b: false }]
        for (let i of arr) {
            // store.dispatch({ type: i["a"], [i["a"]]: i["b"] })
            this.props.obj({ type: i["a"], [i["a"]]: i["b"] })
        }
        document.body.style.overflow = ""
    }

    public render(): React.ReactNode {
        console.log(this.state.info)
        return (
            <div className="position-fixed start-0 top-0 bg-secondary w-100 vh-100 d-flex justify-content-center align-items-center py-5 px-2">
                <div className="bg-white rounded-5 tnc-outer p-3 position-relative">
                    <div className="text-center fw-bold">服務計劃條款</div>
                    <div><i onClick={this.close} className="fa-solid fa-xmark cross-icon fs-5 mb-3 p-0"></i></div>
                    <div className=" fs-6 tnc-overflow" dangerouslySetInnerHTML={{ __html: this.state.info }}></div>
                    <div onClick={this.close} className="cursor db-red text-white text-center position-absolute tnc-confirm-btn rounded-pill py-2 mb-2">確定</div>
                </div>

            </div>
        )
    }
}


const MaptoState = (state: CombineRootState) => {
    return state.miscell
}

const MaptoDispatch = (dispatch:Dispatch<any>) => ({
    obj:(x:any) => dispatch({...x})
})

const connector = connect(MaptoState,MaptoDispatch)
export default connector(TAndC);

