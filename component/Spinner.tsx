export default function Spinner(){
    return (
      <div className="position-absolute start-50 top-50 translate-middle lds-spinner"><div></div><div></div><div></div>
      <div></div><div></div><div></div><div></div><div></div><div>
      </div><div></div><div></div><div></div>
    </div>
    )
  }