import React from 'react'



class Footer extends React.Component {
    constructor(props: {}) {
        super(props)
    }

scrollToTop = () => {
    window.scroll(0,0)
}

    public render(): React.ReactNode {
        return (
            <>
                <div className="ft-text p-3 w-100 text-center border-0 border-top bg-white bottom-0">
                    <div className="position-fixed backtotop"><i onClick={this.scrollToTop} className="cursor fa-solid fa-angles-up fs-5 rounded-top text-dark bg-white p-2 opacity-75 position-absolute end-0 bottom-0"></i></div>
                    © 2022 Hong Kong Cable Television Limited. All Rights Reserved.
                </div>
            </>
        )
    }
}

export default Footer;