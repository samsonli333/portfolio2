import React, { Dispatch } from 'react'
import { Collapse } from 'react-bootstrap';
import { connect, ConnectedProps } from 'react-redux';
import { planServiceResultList } from '../redux/store';
import { tvChannel, tvChannelList } from '../type/type';




type Prop = {
    outline: boolean,
    caption: boolean,
    planSerResult: planServiceResultList,
    planCode?: string
}

type Props = Prop & ConnectedProps<typeof connector>

interface State {
    collap: boolean
}

class PageCard extends React.Component<Props, State>{
    public state: State
    public smImgList: tvChannel[] | undefined
    constructor(props: Props) {
        super(props)
        this.state = {
            collap: false,
        }
        this.smImgList = [];
    }


    componentDidMount(): void {
        this.getTvChannel(this.props.planSerResult?.planCode)
    }


    getTvChannel = async (item: string | undefined) => {
        const Body = {
            planCode: item,
            lang: "C"
        }
        const res = await fetch(`${process.env.NEXT_PUBLIC_API4}`, { method: "POST", headers: { "Content-type": "application/json" }, body: JSON.stringify(Body) });
        const result: tvChannelList = await res.json()
        if (result) this.smImgList = result.resultList
    }

    collapSwitch = () => {
        this.setState((state: State) => ({ collap: !state.collap }))
    }


    gotoPage3 = (planCode: string | undefined) => {
        // store.dispatch({ type: "selectedPlanCode", selectedPlanCode: planCode })
        this.props.selectedPlanCode({ type: "selectedPlanCode", selectedPlanCode: planCode })
        const Arr = [["com1", false], ["com2", false], ["com3", false], ["com4", true]]
        for (let x of Arr) {
            this.props.Arr({ type: x[0], [`${x[0]}`]: x[1] })
        }
    }

    public render(): React.ReactNode {
        return (
            <>
                <div className={`${(this.props.outline || this.props.planSerResult?.is_hot) && "b-red"} mx-auto my-4 card-outer shadow`}>
                    {(this.props.caption || this.props.planSerResult?.is_hot) && <div className="text-white db-red text-center card-title">最佳選擇</div>}
                    <div className="p-3">
                        <div className="d-flex justify-content-center">
                            <div className="w-25 me-2 outer-photo"><img src={`${this.props.planSerResult?.planImage}`} className="card-photo" /></div>
                            <section className="section-left d-flex card-item align-items-start align-content-start flex-wrap w-75">
                                <div className="text">
                                    <span className="border-right ps-2">{this.props.planSerResult?.planCategory}</span><br />
                                    <span className="fs-5 fw-bold text-dark">{this.props.planSerResult?.planName}</span><br />
                                    <div className="text3 contract-hide-large pdf-contract-hide-large">{`${this.props.planSerResult?.contractPeriod} 合約`}</div>
                                </div>
                                <div className="text-end text3">
                                    <span className="dt-red fs-4 fw-bold">{`$${this.props.planSerResult?.monthlyFee}`}</span><span className="fs-6">/月</span>
                                </div>
                                <div className="text3 contract-hide-small pdf-contract-hide-small">{`${this.props.planSerResult?.contractPeriod} 合約`}</div>
                                <div className="text3 dt-red">{this.props.planSerResult?.introTxtRed}</div>
                            </section>
                        </div>
                        {this.props.planSerResult?.premiumList?.filter((_, i) => i === 0).map((item, i) => {
                            console.log(item, i)
                            return <CardCollapse key={i} open={this.state.collap} bigImg={item.premiumImage} smImgList={this.smImgList} bigImgType={item.premiumImageType} />
                        })}
                    </div>
                </div>
            </>
        )
    }
}

const CardCollapse = (props: { open: boolean, bigImg: string | undefined, bigImgType: string, smImgList: tvChannel[] | undefined }) => {
    const Jpeg = `data:image/${props.bigImgType};base64,`
    const image = Jpeg + props.bigImg
    return (
        <>
            {props.open && <div className="border-0 border-top mt-3 py-2">
                <Collapse in={props.open}>
                    <div id="example-collapse-text">
                        <div className="card-collapse-text text-dark text-opacity-75">
                            44 條頻道<br />優惠期 12個月<br />計劃月費 $ 58<br /><br /><span className="card-collapse-text2">送迎新禮品包括︰</span><br />
                            <img className="card-collapse-photo" src={image} /><br />
                            歡樂組合 ( 44 條)
                        </div>
                        <div className="d-flex flex-wrap mt-2">
                            {props.smImgList?.map((item) => {
                                console.log(item)
                                return (
                                    <div className="card-collapse-photoframe">
                                        <img className="card-collapse-smallphoto" src={`data:image/${item.channelImageType};base64,${item.channelImage}`} />
                                    </div>
                                )
                            })}
                        </div>
                    </div>
                </Collapse>
            </div>}
        </>
    );
}

const MaptoDispatch = (dispatch:Dispatch<any>) => ({
    selectedPlanCode:(x:{ type: string, selectedPlanCode: string|undefined }) => dispatch({...x}),
    Arr:(x:any) => dispatch({...x})
})

const connector = connect(null,MaptoDispatch)
export default connector(PageCard);