import React, { Dispatch, ReactNode } from 'react'
import { connect , ConnectedProps} from 'react-redux';
import { CombineRootState } from '../redux/store';
import {withRouter} from 'next/router'
import { WithRouterProps } from 'next/dist/client/with-router';



type Prop = {
    step:string,
    title:string
    content:JSX.Element;
    children:ReactNode,
    backToPage:(() => void )| null
}

type Props = Prop & CombineRootState["switchCom"] & WithRouterProps & ConnectedProps<typeof connector>
class FirstSection extends React.Component<Props> {
    constructor(props:Props) {
        super(props)
    }

backToPage1 = () => {
    // store.dispatch({type:"serAdr",serAdr:""})
    this.props.serAdr({type:"serAdr",serAdr:""})
    this.props.router.push('/')
}

    public render(): React.ReactNode {
        return (
            <>
                <section className="firstsection d-flex justify-content-center pt-5">
                    <div id="page2">
                       {/* <i onClick={this.props.backToPage??this.backToPage1} className="cursor fa-solid fa-angle-left fs-5"></i> */}
                        <section className="outermost bg-transparent d-flex flex-column align-items-center px-5">
                        <i onClick={this.props.backToPage??this.backToPage1} className="cursor fa-solid fa-angle-left fs-5 align-self-start ps-5"></i>
                            <div className="text-center mb-4"><span className="dt-red text3">{this.props.step}</span><br /><span className="fs-5 fw-bold">{this.props.title || "請選擇服務"}</span></div>
                            {this.props.content}
                        </section>
                        {this.props.children}
                    </div>
                </section>
            </>
        )
    }
}

const MaptoState = (state: CombineRootState) => {
    return state.switchCom
}

const MaptoDispatch = (dispatch:Dispatch<any>) => ({
    serAdr:(x:{type:"serAdr",serAdr:""}) => dispatch({...x})
})


const connector = connect(MaptoState,MaptoDispatch)
export default withRouter(connector(FirstSection));