import React from 'react'



type Prop = {
    title: string,
    selectOption: string[],
    name:string,
    className?: string
    tranFunc: (x:string) => string 
    errMsgOn:boolean
    selected?:string | undefined
}


interface State{
    value:string | undefined
}

class FormRadio extends React.Component<Prop,State> {
    public state:State
    constructor(props: Prop) {
        super(props)
        this.state = {
            value:undefined
        }
    }

    public render() {
        return (
            <>
                <div className={`${(this.props.errMsgOn && !this.state.value) && "b-red"} p-2 px-3 border border-secondary border-opacity-25 rounded-2 text-dark w-100 d-flex justify-content-start align-items-center`}>
                    {this.props.title}
                    <div className="ms-auto">
                        {this.props.selectOption.map((item, i) => {
                            return <>
                                <input type="radio" onChange={(e) => this.setState({value:e.target.value})} className="btn-check" value={this.props.tranFunc(item)} name={this.props.name} id={this.props.name + i} autoComplete="off" checked={(this.state.value || this.props.selected) === this.props.tranFunc(item)}/>
                                <label className={`${this.props.className?this.props.className:"form-radio p-2 px-3 text-opacity-50"} d-flex-block me-3`} htmlFor={this.props.name + i}>{item}</label>
                            </>
                        })}
                    </div>
                </div>
            </>
        )
    }
}

export default FormRadio;


