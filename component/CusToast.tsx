import React, { Dispatch } from 'react'
import { Toast, ToastContainer } from 'react-bootstrap'
import { CombineRootState } from '../redux/store'
import { ConnectedProps, connect } from 'react-redux'



type Props = CombineRootState['switchCom'] & ConnectedProps<typeof connector>

class CusToast extends React.Component<Props> {
    constructor(prop:Props){
    super(prop)
   }

   toggle = () => {this.props.closeToast({type:'comMount',comMount:false})}  

   public render(){
    return (
        <>
        <ToastContainer position={"top-center"}>
        <Toast bg={"danger"} onClose={this.toggle}  show={this.props.comMount || false} delay={5000} autohide>
          <Toast.Header>
            <img
              src="holder.js/20x20?text=%20"
              className="rounded me-2"
              alt=""
            />
            <strong className="me-auto"></strong>
            <small></small>
          </Toast.Header>
          <Toast.Body className="text-white fs-6 text-center">電郵已發送至你的郵箱，請查閱。</Toast.Body>
        </Toast>
        </ToastContainer>
        </>
    )
   }
}

const MaptoState = (state: CombineRootState) => {
  return state.switchCom
}

const MaptoDispatch = (dispatch:Dispatch<any>) => ({
  closeToast:(x:{ type:string, comMount:boolean }) => dispatch({...x})
})

const connector = connect(MaptoState,MaptoDispatch)

export default connector(CusToast)