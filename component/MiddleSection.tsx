import React, { ChangeEvent, Dispatch, FormEvent } from 'react'
import { connect, ConnectedProps } from 'react-redux';
import { withRouter } from 'next/router'
import { CombineRootState } from '../redux/store';
import AddressSearch from './AddressSearch';
import Image from 'next/image'
import { Book, Icon, Icon2 } from '../public/photo/photo';
import { WithRouterProps } from 'next/dist/client/with-router';
import { addResult, getClassId, IdAndClass } from '../type/type';



interface State {
    addKeyword: string,
    addResult: addResult | "",
    errorMsg: string | ""
    class: { class1: getClassId | undefined, class2: getClassId | undefined }
}



type Props = CombineRootState["switchCom"] & CombineRootState["planService"] & WithRouterProps & IdAndClass & ConnectedProps<typeof connector>

class MiddleSection extends React.Component<Props, State> {
    public state: State
    constructor(props: Props) {
        super(props)
        this.state = {
            addKeyword: '',
            addResult: "",
            errorMsg: '',
            class: { class1: undefined, class2: undefined }
        }
    }


    onSubmit = async (e: FormEvent<HTMLFormElement> | undefined = undefined) => {
        e?.preventDefault();
        type Address = {
            keyword: string
        }
        const address: Address = {
            keyword: this.state.addKeyword
        }
        const res = await fetch(`${process.env.NEXT_PUBLIC_API1}`, {
            method: "post", headers: { "Content-Type": "application/json" },
            body: JSON.stringify(address)
        })
        const result: addResult = await res.json()
        if (result.code === 0) {
            this.setState({ addResult: result, errorMsg: "" })
        } else if (result.code === 21101) {
            this.setState({ addResult: { ...result, resultList: [{ sid: "", full_address: "找不到相關資料", weighting: 0 }] } })
        } else if (result.code === 21102) {
            this.setState({ errorMsg: result.msg, addResult: "" })
        }
    }


    onSearch = (e: ChangeEvent<HTMLInputElement>) => {
        this.setState({ addKeyword: e.target.value });
        const checkValue = () => {
            if (!this.state.addKeyword) {
                this.setState({ addResult: "" })
            } else if (this.state.addKeyword.length > 3) {
                this.onSubmit();
            }
        };
        setTimeout(checkValue, 0);
    }


    goToPage2 = () => {
        this.props.planService && 
        // store.dispatch({ type: "planService", planService: "" });
        this.props.planService({ type: "planService", planService: "" });
        // store.dispatch({ type: "keyword", keyword: this.state.addKeyword });
        this.props.keyword({ type: "keyword", keyword: this.state.addKeyword });
        this.props.router.push('/page2');
    }


    public render(): React.ReactNode {
        return (
            <>
                <div className="outer-section">
                    <section className="section2">
                        <section className="inner-section">
                            <div className="first"><span className="firstText">新用戶登記</span><br />請輸入地址</div>
                            <div className="second">
                            <div className="text-primary fs-8 ps-2">請輸入"Lok Fu 或 Wong Tai Sin"搜尋</div>
                                <form onSubmit={this.onSubmit}>
                                    <label htmlFor="inputform" className="second-input d-flex justify-content-center align-items-center p-1 position-relative">
                                        <input id="inputform" placeholder="搜尋我的地址" className="pb-1 pt-4" defaultValue={this.state.addKeyword} type="search" onChange={(e) => this.onSearch(e)} />
                                        <span style={{ color: "#ADB5BD" }} className="position-absolute top-0 start-0 text-secondary text-opacity-75">搜尋大廈、屋苑、街道名稱</span>
                                        <button className="second-button"></button>
                                    </label>
                                </form>
                                {this.state.addResult ? <AddressSearch resultList={this.state.addResult.resultList} /> : ''}
                                <div className={`${this.state.errorMsg ? "text-danger text2" : ''} text ms-3 mt-2`}>{this.state.errorMsg ? "請輸入你的地址關鍵詞 (如大廈屋苑名稱)" : ""}</div>
                            </div>
                            <div id={this.props.class.class1?.result.htmlID} onClick={this.goToPage2} className={`${this.props.class?.class1?.result.cssClass} third cursor`}>查詢網絡覆蓋及月費計劃</div>
                            {/* <div id={this.props.class.class2?.result.htmlID} onClick={() => { store.dispatch({ type: "com2", com2: true }) }} className={`${this.props.class.class2?.result.cssClass} fourth cursor`}>以地址分類搜尋</div> */}
                            <div id={this.props.class.class2?.result.htmlID} onClick={() => { this.props.swCom({ type: "com2", com2: true }) }} className={`${this.props.class.class2?.result.cssClass} fourth cursor`}>以地址分類搜尋</div>
                        </section>
                    </section>
                    <section className="section3">
                        <section className="inner-section">
                            <div className="second">登記流程</div>
                            <div className="fourth">
                                <div className="first-row">
                                    <Image src={Icon2} alt="" />
                                    <div><span className="first-span">第一步</span><br /><span className="second-span">查詢網絡覆蓋並選擇所需服務計劃</span></div>
                                </div>
                            </div>
                            <div className="fifth">
                                <div className="first-row">
                                    <Image src={Book} alt="" />
                                    <div><span className="first-span">第二步</span><br /><span className="second-span">輸入基本資料及安裝日期</span></div>
                                </div>
                            </div>
                            <div className="sixth">
                                <div className="first-row">
                                    <Image className="Icon-img" src={Icon} alt="" />
                                    <div><span className="first-span">第三步</span><br /><span className="second-span">提交付款資料</span></div>
                                </div>
                            </div>
                            <div className="eighth">
                                成功登記後，將有專人與你聯絡確認申請。 月費會於成功安裝後，從客戶提交的付款資料中扣除。
                            </div>
                        </section>
                    </section>
                </div>
            </>
        )
    }
}

const MaptoState = (state: CombineRootState) => {
    return {
        ...state.switchCom,
        ...state.planService
    }
}

const MaptoDispatch = (dispatch:Dispatch<any>) => ({
    planService:(x:{ type: string, planService:string }) => dispatch({...x}),
    keyword:(x:{ type:string, keyword:string }) => dispatch({...x}),
    swCom:(x:{ type: string, com2: boolean }) => dispatch({...x})
})


const connector = connect(MaptoState,MaptoDispatch)
export default withRouter(connector(MiddleSection));