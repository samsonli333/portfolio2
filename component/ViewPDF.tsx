import dayjs from 'dayjs'
import React, { Dispatch, useState } from 'react'
import { Collapse } from 'react-bootstrap'
import { connect,ConnectedProps } from 'react-redux'
import ReactDOMServer from 'react-dom/server'
//@ts-ignore
import { getCreditCardNameByNumber } from 'creditcard.js'
import ViewHTML2 from './ViewHTML2'
import { CombineRootState, planServiceResultList } from '../redux/store'
import { CardInfoPage5, getPlanPaymentInfo, info, planServiceList2, Tnc, TncContent } from '../type/type'
import metaData from '../function/MetaData'
import NavBar from './NavBar'
import FirstSection from './FirstSection'
import Term from './Term'
import Term2 from './Term2'
import PageCard from './PageCard'
import Script from 'next/script'
import { textMarshal } from 'text-marshal'


interface State {
    planInfo: getPlanPaymentInfo
    result: {
        tnc: string
        tncDetails: TncContent[]
    }
    termIndex: boolean[]
    term: { tncID: number }[]
    term2: boolean
    errMsgOn: boolean
    loader: boolean
    tncErrMsgOn: boolean[]
}

declare var window: any

type Prop = {
    miscell: CombineRootState["miscell"]
    planService: CombineRootState["planService"]
    cardInfo: CombineRootState["cardInfo"]
    processInfo: CombineRootState["processInfo"]
} 

type Props = Prop & ConnectedProps<typeof connector>


class ViewPDF extends React.Component<Props, State>{
    public term: { tncID: number }[]
    public planImgUrl: string
    constructor(props: Props) {
        super(props)
        this.term = []
        this.planImgUrl = ""
        this.state = {
            planInfo: {
                code: 0, msg: "", resultList: [{
                    groupNum: 0,
                    groupName: "",
                    itemNum: 0,
                    itemName: "",
                    priceStr: ""
                }]
            },
            result: {
                tnc: "",
                tncDetails: []
            },
            termIndex: [],
            term: [],
            term2: false,
            errMsgOn: false,
            loader: false,
            tncErrMsgOn: [],
        }
    }

    componentDidMount = async () => {
        await this.getPlanImgUrl();
        await this.getPayment();
        await this.getPlanService();
        await this.getTNC();
        setTimeout(this.genPDF, 300);
        setTimeout(async () => {
            const res = await fetch(`${process.env.NEXT_PUBLIC_API20}`,
                {
                    method: "POST", headers: { "Content-Type": "application/json" },
                    body: JSON.stringify(this.sendHtml())
                })
            const result = await res.json()
            if (result) console.log(result)
        }, 500)
        setTimeout(this.closeViewPDF, 1000);
    }

    getPlanImgUrl = async () => {
        // const Body = {
        //     planCode: this.props.miscell.selectedPlanCode,
        //     lang: "C"
        // }
        // const res = await fetch(`${process.env.NEXT_PUBLIC_API8}`, { method: "POST", headers: { "Content-type": "application/json" }, body: JSON.stringify(Body) })
        // const result: planServiceList2 = await res.json()
        // if (result.code === 0) this.planImgUrl = result.result.planImgUrl;
        this.planImgUrl = process.env.NEXT_PUBLIC_IMG || ""
    }

    closeViewPDF = () => {
        // store.dispatch({type:"com11",com11:false})
        this.props.swCom({type:"com11",com11:false})
        this.props.openToast({type:"comMount",comMount:true})
    }

    getPayment = async () => {
        const Body = {
            planCode: this.props.miscell.selectedPlanCode,
            lang: "C"
        }
        const res = await fetch(`${process.env.NEXT_PUBLIC_API6}`, { method: "POST", headers: { "Content-type": "application/json" }, body: JSON.stringify(Body) })
        const result: getPlanPaymentInfo = await res.json();
        if (result.code === 0) this.setState({ planInfo: result })
    }

    getPlanService = () => {
        const filterPlan: any = this.props.planService.planService?.resultList.filter((item, _) => {
            if (item.planCode === this.props.miscell.selectedPlanCode) {
                return item
            }
        })
        return filterPlan[0]
    }

    getTNC = async () => {
        const Body = {
            planCode: this.props.miscell.selectedPlanCode,
            lang: "C"
        }
        const res = await fetch(`${process.env.NEXT_PUBLIC_API15}`, { method: "POST", headers: { "Content-type": "application/json" }, body: JSON.stringify(Body) });
        const result: Tnc = await res.json();
        this.setState({ result: result.result })
    }

    genPDF = () => {
        const page: any = document.getElementById('pdf');
        var opt = {
            margin: 0.3,
            filename: this.props.miscell.processId,
            pagebreak: { mode: ['avoid-all'], before: '.page-break' },
            image: { type: 'jpeg', quality: 0.98 },
            html2canvas: { scale: 2 },
            jsPDF: { unit: 'in', format: 'letter', orientation: 'portrait' }
        };

        window.html2pdf().set(opt).from(page).toPdf().get('pdf').then(function (pdf: any) {
            var totalPages = pdf.internal.getNumberOfPages();
            for (let i = 1; i <= totalPages; i++) {
                pdf.setPage(i);
                pdf.setFontSize(10);
                pdf.setTextColor(150);
                pdf.text('Page ' + i + ' of ' + totalPages, (pdf.internal.pageSize.getWidth() / 2.25), (pdf.internal.pageSize.getHeight() - 0.3));
                pdf.text(' Printed at ' + getPrintDateTime(), (pdf.internal.pageSize.getWidth() / 1.35), (pdf.internal.pageSize.getHeight() - 0.3));
            }
        }).save()

        function getPrintDateTime() {
            var d = new Date();
            var datestring = d.getFullYear() + "-" + ("0" + (d.getMonth() + 1)).slice(-2) + "-" + ("0" + d.getDate()).slice(-2)
                + " " + ("0" + d.getHours()).slice(-2) + ":" + ("0" + d.getMinutes()).slice(-2) + ":" + ("0" + d.getSeconds()).slice(-2);
            return datestring;
        }
    }

    sendHtml = () => {
        const Data = ReactDOMServer.renderToStaticMarkup(<ViewHTML2 miscell={this.props.miscell}
            planService={this.props.planService}
            cardInfo={this.props.cardInfo}
            processInfo={this.props.processInfo}
            result={this.state.result}
            payment={this.state.planInfo}
            imgUrl={this.planImgUrl} />)
        const Body = { mail: metaData(Data), recipientEmail: this.props.processInfo.body?.email }
        return Body
    }


    public render(): React.ReactNode {
        return (
            <div id="pdf">
                <Script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.5.3/jspdf.min.js"></Script >
                <Script src="https://cdnjs.cloudflare.com/ajax/libs/html2pdf.js/0.9.3/html2pdf.bundle.min.js"></Script>
                <NavBar></NavBar>
                <FirstSection title={"確認資料"} step={''} content={<div className="mb-5 fs-5 fw-bold">{`你的網上登記編號：${this.props.miscell.processId}`}</div>} backToPage={null}>
                    <div className="page5-width mx-auto px-3">
                        <PageRowOrdered info={this.getPlanService()} />
                        <div className="mt-5"></div>
                        <PageRowBasic info={this.props.processInfo.body} />
                        <div className='mt-5'></div>
                        <PageRowInstall info={this.props.processInfo.body} />
                        <div className="html2-df_page-break page-break"></div>
                        <div className="mt-5"></div>
                        <PageRowContact info={this.props.processInfo.body} />
                        <div className="mt-5"></div>
                        <PageRowMonth code={0} msg={''} resultList={this.state.planInfo.resultList} />
                        <div className="mt-5"></div>
                        <PageRowPayment body={this.props.cardInfo.body} />
                        <form id="term">
                            <section className="terms mt-5">
                                {this.state.result.tncDetails.filter((item, _) => item.is_Compulsory === true).map((item, i) => {
                                    return <>
                                        <Term key={i} termContent={item.content} name={`term${i + 1}`} errMsgOn={this.state.tncErrMsgOn[i]} errMsgContent={'請勾選該欄位'} checked={true} />
                                        <div className="mt-2"></div>
                                    </>
                                })}
                                <Term2 termContent={""} errMsgOn={this.state.errMsgOn} name={""} errMsgContent={'未能成功提交申請，請稍後再次嘗試。'} checked={true} />
                            </section>
                        </form>
                    </div>
                </FirstSection>
            </div>
        )
    }
}

const MaptoState = (state: Prop) => {
    return state
}

const MaptoDispatch = (dispatch:Dispatch<any>) => ({
    swCom:(x:{type:string,com11:boolean}) => dispatch({...x}),
    openToast:(x:{type:string,comMount:boolean}) => dispatch({...x})
})

const PageRowOrdered = (prop: { info: planServiceResultList }) => {
    return (
        <>
            <div className="fs-5 border-0 b-red border-start border-4 ps-2 mb-2 fw-bold pdf-page">已選擇的服務</div>
            <div id="pdf-pagecard"><PageCard outline={true} caption={false} planSerResult={prop.info} /></div>
        </>
    )
}

const PageRowBasic = (prop: info) => {
    return (
        <>
            <div className="fs-5 border-0 b-red border-start border-4 ps-2 mb-2 fw-bold">基本資料</div>
            <div className="row justify-content-start ps-2 border-0 border-start border-4 border-white">
                <div className="col-auto"><span className="text-secondary text-opacity-50">性別</span><br />{prop.info?.gender === "M" ? "男" : "女"}</div>
                <div className="col-auto"><span className="text-secondary text-opacity-50">姓氏</span><br />{prop.info?.surname}</div>
                <div className="col-auto"><span className="text-secondary text-opacity-50">名字</span><br />{prop.info?.givenname}</div>
                <div className="col-auto"><span className="text-secondary text-opacity-50">出生日期 (YYYY/MM/DD)</span><br />{`${prop.info?.dateOfBirthYrs}/${prop.info?.dateOfBirthMth}/${prop.info?.dateOfBirthDay}`}</div>
                <div className="col-auto"><span className="text-secondary text-opacity-50">護照號碼</span><br />{prop.info?.identityNo}</div>
            </div>
        </>
    )
}

const PageRowInstall = (prop: info) => {
    return (
        <>
            <div className="fs-5 border-0 b-red border-start border-4 ps-2 mb-2 fw-bold">安裝詳情</div>
            <div className="row justify-content-start ps-2 border-0 border-start border-4 border-white">
                <div className="col-auto"><span className="text-secondary text-opacity-50">預約安裝日期</span><br />{prop.info?.appoDate}</div>
                <div className="col-auto"><span className="text-secondary text-opacity-50">預約安裝時間</span><br />{prop.info?.appoTimeStr}</div>
                <div className="col-auto"><span className="text-secondary text-opacity-50">地址</span><br />{prop.info?.addrDiff === "Y" ? prop.info.commsAddress : `${prop.info?.commsAddress} ${prop.info?.instFloor && prop.info?.instFloor + "樓"} ${prop.info?.instRoom && prop.info?.instRoom + "室"}`}</div>
            </div>
        </>
    )
}


const PageRowContact = (prop: info) => {
    return (
        <>
            <div className="fs-5 border-0 b-red border-start border-4 ps-2 mb-2 fw-bold">聯絡資料</div>
            <div className="row justify-content-start ps-2 border-0 border-start border-4 border-white">
                <div className="col-auto"><span className="text-secondary text-opacity-50">通訊語言選擇</span><br />{prop.info?.commsLang === "C" ? "中文" : "英文"}</div>
                <div className="col-auto"><span className="text-secondary text-opacity-50">手提電話</span><br />{prop.info?.contactMobile}</div>
                <div className="col-auto"><span className="text-secondary text-opacity-50">住宅電話</span><br />{prop.info?.contactHome}</div>
                <div className="col-auto"><span className="text-secondary text-opacity-50">電郵地址</span><br />{prop.info?.email}</div>
                <div className="col-auto"><span className="text-secondary text-opacity-50">通訊地址</span><br />{prop.info?.addrDiff === "Y" ? prop.info.commsAddress : `${prop.info?.commsAddress} ${prop.info?.instFloor && prop.info?.instFloor + "樓"} ${prop.info?.instRoom && prop.info?.instRoom + "室"}`}</div>
            </div>
        </>
    )
}


const PageRowMonth = (prop: getPlanPaymentInfo) => {
    const [open, setOpen] = useState(true)
    const [open2, setOpen2] = useState(true)
    const Arr: CardInfoPage5[] = []
    return (
        <>
            <div className="fs-5 border-0 b-red border-start border-4 ps-2 mb-3 fw-bold">月費詳情</div>
            <div >
                {prop.resultList.reduce((prevItem, currentItem) => {
                    console.log("currentItem", currentItem)
                    console.log("prevItem", prevItem)
                    const func = () => {
                        let y = true
                        for (let x of prevItem) {
                            if (x.Title === currentItem.groupName && currentItem.itemNum === 99) {
                                x.Total = currentItem.priceStr
                                x.Item = currentItem.itemName
                                return
                            } else if (x.Title === currentItem.groupName) {
                                x.Detail = [...x.Detail, { Content: currentItem.itemName, Amount: currentItem.priceStr, underLine: false }]
                                y = false
                                return
                            }
                        }
                        return y
                    }
                    if (func()) prevItem = [...prevItem, { Title: currentItem.groupName, Item: "", Total: "", Detail: [{ Content: currentItem.itemName, Amount: currentItem.priceStr, underLine: false }] }]
                    console.log("finalPrevItem", prevItem)
                    return prevItem
                }, Arr).map((item, i) => {
                    return <div className="mb-3"><FeeCard Title={item.Title} Item={item.Item} Total={item.Total} Detail={item.Detail} /></div>
                })}
            </div>
        </>
    )
}


const PageRowPayment = (prop: CombineRootState["cardInfo"]) => {
    const data = textMarshal({
        input: prop.body.card_no,
        template: "xxxx************",
        disallowCharacter: [/[a-z]/],
    })
    const MM = dayjs(prop.body.expiry_date).format('MM')
    const YY = dayjs(prop.body.expiry_date).format('YY')
    return (
        <>
            <div className="fs-5 border-0 b-red border-start border-4 ps-2 mb-2 fw-bold">付款資料</div>
            <div className="row justify-content-start ps-2 border-0 border-start border-4 border-white">
                <div className="col-auto"><span className="text-secondary text-opacity-50">信用卡號碼</span><br />{data.marshaltext}</div>
                <div className="col-auto"><span className="text-secondary text-opacity-50">信用卡種類</span><br />{getCreditCardNameByNumber(prop.body.card_no).toUpperCase()}</div>
                <div className="col-auto"><span className="text-secondary text-opacity-50">持卡人姓名</span><br />{prop.body.card_holder}</div>
                <div className="col-auto"><span className="text-secondary text-opacity-50">有效期至 </span><br />{`${MM}/${YY}`}</div>
            </div>
        </>
    )
}



const FeeCard = (props: CardInfoPage5) => {
    const [open, setOpen] = useState(true)
    return (
        <div className="border rounded-4 shadow p-3">
            <div onClick={() => setOpen(state => !state)} className="d-flex justify-content-between align-items-center cursor">
                <div className="fs-6 fw-bold mb-2">{props.Item}</div>
                <div className="d-flex justify-content-between align-items-center">
                    <div className="fs-5 fw-bold dt-red me-3">{props.Total}</div>
                    {/* {open ? <i className="fa-solid fa-angle-up text-secondary"></i> : <i className="fa-solid fa-angle-down text-secondary"></i>} */}
                </div>
            </div>
            <Collapse in={open}>
                <div>
                    <FeeItem key={"default"} Content={''} Amount={''} underLine={true} />
                    {props.Detail.map((item, i) => {
                        return <FeeItem key={i} Content={item.Content} Amount={item.Amount} underLine={item.underLine} />
                    })
                    }
                </div>
            </Collapse>
        </div>
    )
}



const FeeItem = (props: { Content: string, Amount: string, underLine: boolean }) => {
    return (
        <>
            <div className={`d-flex py-2 justify-content-between align-items-center ${props.underLine ? "border-0 border-bottom border-1 border-secondary border-o border-opacity-25" : ""}`}>
                <div className="fs-7">{props.Content || "項目"}</div>
                <div className="fs-7">{props.Amount || "金額 (HK$)"}</div>
            </div>
        </>
    )
}

const connector = connect(MaptoState,MaptoDispatch)
export default connector(ViewPDF);