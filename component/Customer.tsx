import React from 'react'



class Customer extends React.Component {
    constructor(props: {}) {
        super(props)
    }

    public render() {
        return (
            <div className="customer-appearance btn-text rounded-pill border-secondary border text-secondary px-5 py-2 text-center">
                <a style={{textDecoration:"none",color:"#6C757D"}} target="_blank" href="https://apps3.i-cable.com/MobileCSOnlineCentre/loginPage">現有客戶</a>
            </div>
        )
    }
}

export default Customer;