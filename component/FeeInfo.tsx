import React from 'react'
import { CardInfo, PlanPayment } from '../type/type'






type Prop = {
    PlanInfo: PlanPayment[] | null
}

class FeeInfo extends React.Component<Prop> {
    constructor(props: Prop) {
        super(props)
    }

    public render(): React.ReactNode {
        const Arr:CardInfo[] = []
        return (
            <div>
                <div className="border-0 border-start border-3 b-red px-2 mb-4"><div className="fs-5 fw-bold">月費詳情</div></div>
                {this.props.PlanInfo?.reduce((prevItem, currentItem) => {
                    console.log("currentItem", currentItem)
                    console.log("prevItem", prevItem)
                    const func = () => {
                        let y = true
                        for (let x of prevItem) {
                            if (x.Title === currentItem.groupName && currentItem.itemNum === 99) {
                                x.Total = currentItem.priceStr
                                return
                            } else if (x.Title === currentItem.groupName) {
                                x.Detail = [...x.Detail, { Content: currentItem.itemName, Amount: currentItem.priceStr, underLine: false }]
                                y = false
                                return
                            }
                        }
                        return y
                    }
                    if (func()) prevItem = [...prevItem, { Title: currentItem.groupName, Total: "", Detail: [{ Content: currentItem.itemName, Amount: currentItem.priceStr, underLine: false }] }]
                    // if (currentItem.itemNum === 99) prevItem = [...prevItem, { Title: "其後每月支付", Total: "", Detail: [{Content:" ",Amount:" ",underLine:false}] }]
                    console.log("finalPrevItem", prevItem)
                    return prevItem
                }, Arr).map((item, i) => {
                    return <div className="mb-3"><FeeCard Title={item.Title} Total={item.Total} Detail={item.Detail} /></div>
                })}
            </div>
        )
    }
}


const FeeCard = (props: CardInfo) => {
    return (
        <div className="rounded-3 p-3 shadow">
            <div className="fs-6 fw-bold mb-2">{props.Title}</div>
            <FeeItem key={"default"} Content={''} Amount={''} underLine={true} />
            {props.Detail.map((item, i) => {
                return <FeeItem key={i} Content={item.Content} Amount={item.Amount} underLine={item.underLine} />
            })
            }
            <div className="text-end border-0 border-top border-1 border-secondary border-opacity-25">總額：<span className="fs-5 fw-bold dt-red">{props.Total}</span></div>
        </div>
    )
}


const FeeItem = (props: { Content: string, Amount: string, underLine: boolean }) => {
    return (
        <>
            <div className={`d-flex py-2 justify-content-between align-items-center ${props.underLine ? "border-0 border-bottom border-1 border-secondary border-o border-opacity-25" : ""}`}>
                <div className="fs-6">{props.Content || "項目"}</div>
                <div className="fs-6">{props.Amount || "金額 (HK$)"}</div>
            </div>
        </>
    )
}

export default FeeInfo;