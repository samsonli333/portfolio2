
import { WithRouterProps } from 'next/dist/client/with-router'
import { withRouter } from 'next/router'
import React, { Dispatch } from 'react'
import { connect, ConnectedProps } from 'react-redux'



type Props = WithRouterProps & ConnectedProps<typeof connector>

class Inquiry extends React.Component<Props> {
    constructor(props: Props) {
        super(props)
    }


    close = () => {
        this.props.router.push('/')
        // store.dispatch({ type: "com5", com5:false })
        this.props.swCom({ type: "com5", com5:false })
    }

    public render() {
        return (
            <div className="position-fixed px-3 w-100 h-100 bg-dark bg-opacity-50 d-flex justify-content-center align-items-center inquiry-outermost">
                <div className="inquiry-width rounded-3 bg-white py-3 text-center">
                    <div className="fs-6 pt-1 pb-3 border-0 border-2 border-bottom b-red">多謝查詢</div>
                    <div className="px-4">
                        <div className="py-3 pb-5">資料經已提交，我們稍後有專人與您聯絡，感謝您的支持!</div>
                        <div onClick={this.close} className="rounded-pill db-red text-white py-2 mb-3 cursor">返回主頁</div>
                    </div>
                </div>
            </div>
        )
    }
}

const MaptoDispatch = (dispatch:Dispatch<any>) => ({
    swCom:(x:{ type: "com5", com5:false }) => dispatch({...x})
})

const connector = connect(null,MaptoDispatch)
export default withRouter(connector(Inquiry));