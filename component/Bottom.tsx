import React , {Dispatch} from 'react'
import { connect,ConnectedProps } from 'react-redux'
import { arrowRight } from '../public/photo/photo'
import { CombineRootState } from '../redux/store'
import Image from 'next/image'


type Props = CombineRootState & ConnectedProps<typeof connector>

class Bottom extends React.Component<Props>{
    constructor(props:Props) {
        super(props)
    }


goToQuestion = () => {
    const Arr = [["com1",true],["com2",false],["com3",false],["com4",false],["com5",false],["com6",true]]
    for (let x of Arr){
        // store.dispatch({type:x[0],[`${x[0]}`]:x[1]})
        this.props.Arr({type:x[0],[`${x[0]}`]:x[1]})
    }
}

    public render(): React.ReactNode {
        return (
            <>
                <section className="section4">
                    <div className="first-row">
                        <div onClick={this.goToQuestion} className="text cursor">常見問題</div>
                        <Image src={arrowRight} alt=""/>
                    </div>
                </section>
            </>
        )
    }
}

const MaptoState = (state:CombineRootState) => {
    return state
}

const MaptoDispatch = (dispatch:Dispatch<any>) => ({
     Arr:(x:any) => dispatch({...x})
})


const connector = connect(MaptoState,MaptoDispatch)
export default connector(Bottom);