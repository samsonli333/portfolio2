import React from 'react'
import { connect } from 'react-redux';
import { checkMobile } from '../function/generalFunc';
import { CombineRootState } from '../redux/store';
import { errMsgOn, getClassId, personalData } from '../type/type';
import Form from './Form';
import FormRadio from './FormRadio';
import Term3 from './Term3';
import Form4 from './Form4'
import { checkForm2,checkForm } from './SecondSection';

// interface State {
//     class: getClassId | undefined
// }

type value = { value2: string | undefined, value3: string | undefined ,value4:string| undefined }

type Prop = {
    nameGroup: {
        name1: string
        name2: string
        name3: string
        name4: string
        name5: string
        name6: string
        name7: string
        name8: string
    }
    tranFunc: (x: string) => string
    onChange: (x:any) => void
    onChangeTerm:(x:any) => void
    errMsgOn: errMsgOn
    value: value 
    innerRef?: React.ForwardedRef<any>
    personalData: personalData | undefined
    swAddr:boolean | undefined
    class: getClassId | null
}

class ContactForm extends React.Component<Prop & CombineRootState["switchCom"]> {
    constructor(props: Prop & CombineRootState["switchCom"]) {
        super(props)
    }


    // componentDidMount = () => {
    //     this.getClassId("NSSbtn11")
    // }


    componentWillUnmount = () => {
        window.scroll({
            top: 0,
            behavior: "smooth"
        })
    }


    // getClassId = async (x: string) => {
    //     const Body = {
    //         eleID: x,
    //         page: "sub",
    //         lang: "C"
    //     }
    //     const res = await fetch(`${process.env.NEXT_PUBLIC_API18}`, { method: "POST", headers: { "Content-Type": "application/json" }, body: JSON.stringify(Body) })
    //     const result: getClassId = await res.json();
    //     if (result) this.setState({ class: result })
    // }

    public render(): React.ReactNode {
        const fullAddr = (this.props.serAdr + " " + (this.props.value.value2 ? this.props.value.value2 + "樓層":"") + " " + (this.props.value.value3 ? this.props.value.value3 + "室":""))
        const Flat = this.props.value.value2?.length || 0
        const Room = this.props.value.value3?.length || 0
        const wholeAddr = this.props.serAdr.length + Flat + Room
        
        return (
            <div ref={this.props.innerRef} className="Contactform">
                <div className="border-0 border-start b-red border-3 ps-2 mb-2 fs-5 fw-semibold">聯絡資料</div>
                <FormRadio errMsgOn={this.props.errMsgOn.errMsgOn19} title={'通訊語言選擇'} name={this.props.nameGroup.name1} selectOption={["中文", "英文"]} tranFunc={this.props.tranFunc} selected={this.props.personalData?.CommsLang} />
                <div className="d-flex my-2">
                    <Form spanName={'區號'} htmlFor={"info6"} id={"info6"} name={this.props.nameGroup.name2} inputBackground={'bg-secondary bg-opacity-25'} width={'w-25'} value={"+852"} errMsgContent={''} />
                    <div className="me-2"></div>
                    <Form spanName={'您的手提電話'} htmlFor={"info7"} id={"info7"} swFunc={true} errFunc={checkForm1} errMsgOn={this.props.errMsgOn.errMsgOn10} name={this.props.nameGroup.name3} inputBackground={'bg-transparent'} width={'w-75'} value={this.props.personalData?.ContactMobile} errMsgContent={''} />
                </div>
                <div className="d-flex my-2">
                    <Form spanName={'區號'} name={this.props.nameGroup.name4} htmlFor={"info8"} id={"info8"} inputBackground={'bg-secondary bg-opacity-25'} width={'w-25'} value={"+852"} errMsgContent={''} />
                    <div className="me-2"></div>
                    <Form spanName={'您的住宅電話(選填)'} name={this.props.nameGroup.name5} swFunc={true} errFunc={checkForm2} errMsgOn={undefined} htmlFor={"info9"} id={"info9"} inputBackground={'bg-transparent'} width={'w-75'} value={this.props.personalData?.ContactHome} errMsgContent={''} />
                </div>
                <Form spanName={'您的電郵地址'} errMsgOn={this.props.errMsgOn.errMsgOn11} swFunc={true} errFunc={checkForm} htmlFor={"info10"} id={"info10"} name={this.props.nameGroup.name6} inputBackground={'bg-transparent'} width={'w-100'} value={this.props.personalData?.email} errMsgContent={''} />
                <div className="mt-2"></div>
                <Form4 spanName={'您的通訊地址'} switchValue={true} onChange={this.props.onChange} errMsgOn={this.props.errMsgOn.errMsgOn14} htmlFor={"info11"} id={"info11"} name={this.props.nameGroup.name7} inputBackground={this.props.swAddr ? "bg-transparent" : 'bg-secondary bg-opacity-25 fw-bold'} width={'w-100'} value={this.props.swAddr ? this.props.value.value4 : (wholeAddr > this.props.serAdr.length) ? fullAddr : (this.props.serAdr||this.props.personalData?.fullAddress)} errMsgContent={''} />
                <div className="mt-2"></div>
                <Term3 termContent={'安裝地址與通訊地址不同'} name={this.props.nameGroup.name8} onChange={this.props.onChangeTerm} errMsgOn={false} errMsgContent={''} value={this.props.swAddr} checked={this.props.swAddr} />
                <button id={this.props.class?.result.htmlID} className={`${this.props.class?.result.cssClass} my-5 cursor rounded-pill db-red py-2 text-center text-white w-100 border-0`}>下一步</button>
            </div>
        )
    }
}


function checkForm1(tel: string) {
    return (checkMobile(tel) ? <div className="text-danger fs-8 error-text">電話號碼無效</div> : "")
}

const MaptoState = (state: CombineRootState) => {
    return state.switchCom
}

const newContactForm = React.forwardRef((props: Prop & CombineRootState["switchCom"], ref: React.ForwardedRef<any>) => <ContactForm {...props} innerRef={ref} />);

export default connect(MaptoState, null, null, { forwardRef: true })(newContactForm);