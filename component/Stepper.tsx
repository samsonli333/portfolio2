import React, { ReactNode } from 'react'
import StepBar from './StepBar';

type Prop = {
    className1: string | null
    className2: string | null
    className3: string | null
    step: string
    content?:string
}


class Stepper extends React.Component<Prop> {
    constructor(props: Prop) {
        super(props)
    }


    public render(): ReactNode {
        const { step, ...rest } = this.props
        return (
            <div className="w-100 d-flex align-items-end justify-content-start stepper-wrap">
                <div className="dt-red col-4 stepper-block-top text-start fw-bold stepper-text">{this.props.content || `還有 ${step || ""} 步完成登記`}</div>
                <StepBar {...rest || null} />
                <div className="dt-red col-4 stepper-block-bottom text-start fw-bold stepper-text">{this.props.content || `還有 ${step || ""} 步完成登記`}</div>
            </div>
        )
    }
}

export default Stepper;