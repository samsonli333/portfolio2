import React from 'react'




type Prop = {
    spanName: string,
    inputBackground: string,
    width: string
    value: string | number | undefined
    errMsgOn?: boolean | undefined
    name: string
    errMsgContent: string
    errFunc?: (email: string) => "" | JSX.Element
    swFunc?: boolean
    onBlur?:(x:string) => void
    onChange?:(x:string) => void
    id?:string
    htmlFor?:string
    placeholder?:string
}

interface State {
    value: string
}

class VisaForm extends React.Component<Prop, State>{
    public state: State
    constructor(props: Prop) {
        super(props)
        this.state = {
            value: ""
    }
}


    
    public render(): React.ReactNode {
        return (
            <div className={`${this.props.width} form`}>
                <label htmlFor={this.props.htmlFor || "inputfield"} className={`${(this.props.errMsgOn && ((!this.state.value) && (!this.props.value))) && "b-red border-1"} position-relative border border-secondary border-opacity-25 rounded-2 w-100`}>
                    <input id={this.props.id || "inputfield"} name={this.props.name} onBlur={(e) => this.props.onBlur?.(e.target.value)} onChange={(e) => this.props.onChange?.(e.target.value)} value={this.props.value || this.state.value} className={`${this.props.inputBackground} border-0 rounded-2 pt-4 pb-2 px-3 w-100`} type="text" placeholder={this.props.placeholder} />
                    <span style={{color:"#ADB5BD"}} className="position-absolute top-0 start-0 form-text">{this.props.spanName}</span>
                </label>
                {this.props.errMsgOn && <div className="text-danger fs-8 error-text">{this.props.errMsgContent}</div>}
            </div>
        )
    }
}


export default VisaForm;