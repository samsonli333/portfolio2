import React, { Dispatch } from 'react'
import { Spinner } from 'react-bootstrap'
import { connect ,ConnectedProps} from 'react-redux'
import { getImage } from '../type/type'



type Props = {
    image: getImage | undefined
} & ConnectedProps<typeof connector>

interface State {
    image: getImage | undefined
}

class Question extends React.Component<Props> {
    public state: State
    constructor(props: Props) {
        super(props)
        this.state = {
            image: this.props.image
        }
    }

 
    close = () => {
        const arr: { a: string, b: boolean }[] = [{ a: "com1", b: true }, { a: "com2", b: false }, { a: "com3", b: false },
        { a: "com4", b: false }, { a: "com5", b: false }, { a: "com6", b: false }]
        for (let i of arr) {
            // store.dispatch({ type: i["a"], [i["a"]]: i["b"] })
            this.props.obj({ type: i["a"], [i["a"]]: i["b"] })
        }
    }

    public render(): React.ReactNode {
        return (
            <div style={{zIndex:"3"}} className="bg-dark bg-opacity-50 position-fixed w-100 h-100 px-2 d-flex justify-content-center align-items-center z">
                <div className="bg-white question-width p-2 d-flex align-items-center flex-column rounded-3">
                    {this.state.image === undefined ? <Spinner/> :
                        <>
                            <div className="align-self-start"><i onClick={this.close} className="fa-solid fa-xmark cross-icon fs-5 mb-4"></i></div>
                            <div className="px-3"><img className="img-fluid" src={this.state.image?.result} /></div>
                        </>}
                </div>
            </div>
        )
    }
}


const MaptoDispatch = (dispatch:Dispatch<any>) => ({
     obj:(x:any) => dispatch({...x})
})

const connector = connect(null,MaptoDispatch)
export default connector(Question);


