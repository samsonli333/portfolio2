import React from 'react'
import { Collapse } from 'react-bootstrap';
import { planServiceResultList } from '../redux/store';
import { tvChannel, tvChannelList } from '../type/type';




type Prop = {
    outline: boolean,
    caption: boolean,
    planSerResult: planServiceResultList,
    planCode?: string
    imgUrl?: string
}

interface State {
    collap: boolean
}

class PageCardHTML extends React.Component<Prop, State>{
    public state: State
    public smImgList: tvChannel[] | undefined
    constructor(props: Prop) {
        super(props)
        this.state = {
            collap: false,
        }
        this.smImgList = [];
    }


    componentDidMount(): void {
        this.getTvChannel(this.props.planSerResult.planCode)
    }


    getTvChannel = async (item: string | undefined) => {
        const Body = {
            planCode: item,
            lang: "C"
        }
        const res = await fetch(`${process.env.NEXT_PUBLIC_API4}`, { method: "POST", headers: { "Content-type": "application/json" }, body: JSON.stringify(Body) });
        const result: tvChannelList = await res.json()
        if (result) this.smImgList = result.resultList
    }

    collapSwitch = () => {
        this.setState((state: State) => ({ collap: !state.collap }))
    }


    // gotoPage3 = (planCode: string | undefined) => {
    //     store.dispatch({ type: "selectedPlanCode", selectedPlanCode: planCode })
    //     const Arr = [["com1", false], ["com2", false], ["com3", false], ["com4", true]]
    //     for (let x of Arr) {
    //         store.dispatch({ type: x[0], [`${x[0]}`]: x[1] })
    //     }
    // }

    public render(): React.ReactNode {
        return (
            <>
                <table style={{ width: "100%" }}>
                    <tr>
                        <td style={{ paddingLeft: "16px", borderLeft: "2px solid red" }}>
                            <h2 style={{ fontSize: "20px", fontWeight: "normal" }} >已選擇的服務</h2>
                        </td>
                    </tr>
                    <tr>
                        <td style={{ paddingBottom: "10px" }}>&nbsp;</td>
                    </tr>
                </table>
                <table border={0} cellPadding={0} style={{ padding: "8px 16px 8px 016px", lineHeight: "normal", width: "100%", maxWidth: "700px", border: "1px solid #e3e3e3", boxShadow: "5px 5px 10px 5px #e3e3e3", backgroundColor: "white", borderRadius: "15px 15px 15px 15px" }}>
                    <tr>
                        <td rowSpan={6} style={{ textAlign: "center" }}><img style={{ minWidth: "148px", minHeight: "111px", height: "100%", maxHeight: "300px", backgroundColor: "grey" }} src={this.props.imgUrl || `data:image/${this.props.planSerResult?.planImageType};base64,${this.props.planSerResult.planImage}`} /></td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td style={{ borderLeft: "2px solid red", paddingLeft: "16px" }}><h3 style={{ fontSize: "16px", fontWeight: "lighter", textAlign: "left" }}>{this.props.planSerResult.planCategory}</h3>
                        </td>
                    </tr>
                    <tr>
                        <td style={{ paddingLeft: "16px", wordBreak: "break-all" }}>
                            <h4 style={{ fontSize: "18px", fontWeight: "lighter", textAlign: "left" }}>{this.props.planSerResult.planName}</h4>
                        </td>
                    </tr>
                    <tr>
                        <td style={{ paddingLeft: "16px" }}><h4 style={{ fontSize: "24px", fontWeight: "lighter", textAlign: "left", color: "red" }}>${this.props.planSerResult.monthlyFee}<span style={{ fontSize: "18px", fontWeight: "lighter", color: "grey" }}>/月</span></h4>
                        </td>
                    </tr>
                    <tr>
                        <td style={{ paddingLeft: "16px" }}><h3 style={{ fontSize: "12px", fontWeight: "lighter" }}>{this.props.planSerResult.contractPeriod} 合約</h3></td>
                    </tr>
                    <tr>
                        <td style={{ paddingLeft: "16px" }}><h3 style={{ fontSize: "12px", fontWeight: "lighter", color: "red" }}>{this.props.planSerResult.introTxtRed}</h3></td>
                    </tr>
                </table>
            </>
        )
    }
}

const CardCollapse = (props: { open: boolean, bigImg: string | undefined, bigImgType: string, smImgList: tvChannel[] | undefined }) => {
    const Jpeg = `data:image/${props.bigImgType};base64,`
    const image = Jpeg + props.bigImg
    return (
        <>
            {props.open && <div className="border-0 border-top mt-3 py-2">
                <Collapse in={props.open}>
                    <div id="example-collapse-text">
                        <div className="card-collapse-text text-dark text-opacity-75">
                            44 條頻道<br />優惠期 12個月<br />計劃月費 $ 58<br /><br /><span className="card-collapse-text2">送迎新禮品包括︰</span><br />
                            <img className="card-collapse-photo" src={image} /><br />
                            歡樂組合 ( 44 條)
                        </div>
                        <div className="d-flex flex-wrap mt-2">
                            {props.smImgList?.map((item) => {
                                console.log(item)
                                return (
                                    <div className="card-collapse-photoframe">
                                        <img className="card-collapse-smallphoto" src={`data:image/${item.channelImageType};base64,${item.channelImage}`} />
                                    </div>
                                )
                            })}
                        </div>
                    </div>
                </Collapse>
            </div>}
        </>
    );
}

export default PageCardHTML;