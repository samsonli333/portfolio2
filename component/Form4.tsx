import React from 'react'




type Prop = {
    spanName: string,
    inputBackground: string,
    width: string
    value: string | number | undefined
    errMsgOn?: boolean | undefined
    name: string
    errMsgContent: string | JSX.Element
    errFunc?: (email: string) => "" | JSX.Element
    swFunc?: boolean
    id?: string
    htmlFor?: string
    placeholder?: string
    onChange?: (x: any) => void
    swType?: boolean
    disabled?: boolean
    specialClass?: string
    maxLength?: number | undefined
    enable?: boolean
    switchValue?: boolean
}

interface State {
    value: any
}

class Form3 extends React.Component<Prop, State>{
    public state: State
    constructor(props: Prop) {
        super(props)
        this.state = {
            value: this.props.value
        }
    }

    static defaultProps = {
        swType: true
    }

    public render(): React.ReactNode {
        return (
            <div className={`${this.props.width} form`}>
                <label htmlFor={this.props.htmlFor || "inputfield"} className={`${(this.props.errMsgOn && ((!this.state.value || !this.props.value))) && "b-red border-1"} position-relative border border-secondary border-opacity-25 rounded-2 w-100`}>
                    <input id={this.props.id || "inputfield"} name={this.props.name} onChange={(e => this.props.onChange?.(e.target.value)) || ((e) => this.setState({ value: e.target.value }))} value={this.props.enable ? undefined : (this.props.switchValue ? this.props.value : this.state.value)} className={`${this.props.inputBackground} border-0 rounded-2 pt-4 pb-2 px-3 w-100`} type="text" placeholder={this.props.placeholder || "你的姓名"} disabled={this.props.disabled} maxLength={this.props.maxLength} />
                    <span style={{ color: "#ADB5BD" }} className={`${this.props.specialClass && this.props.specialClass} position-absolute top-0 start-0 form-text`}>{this.props.spanName}</span>
                </label>
                {this.props.swType ? (this.props.errMsgOn ? (this.props.swFunc ? this.props.errFunc?.(this.state.value) : checkVal(this.state.value, this.props.value, this.props.errMsgContent)) : "") : (this.props.errMsgOn && checkVal2(this.props.errMsgContent))}
            </div>
        )
    }
}


function checkVal(email: string | undefined, propsValue: string | number | undefined, msgContent: string | JSX.Element) {
    return ((!email) && (!propsValue)) ? <div className="text-danger fs-8 error-text">{msgContent}</div> : ""
}

function checkVal2(msgContent: string | JSX.Element) {
    return <div className="text-danger fs-8 error-text">{msgContent || ""}</div>
}


export default Form3;