import { WithRouterProps } from 'next/dist/client/with-router';
import React, { Dispatch } from 'react'
import { connect ,ConnectedProps} from 'react-redux';
import { Cata, CombineRootState, planServiceList } from '../redux/store';
import withRouter from 'next/dist/client/with-router';
import SelectItem from './SelectItem';
import { getClassId } from '../type/type';





interface State {
    title: { title1: string, title2: string[], disabled: boolean, onChange: (x: React.ChangeEvent<HTMLSelectElement>) => void }[],
    value: { value1: string, value2: string, value3: string, value4: string },
    load: boolean,
    timer: number,
    class: getClassId | undefined
}


type Props = CombineRootState["searchByCata"] & CombineRootState["miscell"] & WithRouterProps & ConnectedProps<typeof connector>

class SearchByCata extends React.Component<Props, State>{
    public state: State
    constructor(props: Props) {
        super(props)
        const Arr = ["九龍", "香港", "新界"]
        this.state = {
            title: [{ title1: "區域", title2: ["請選擇區域", ...Arr], disabled: false, onChange: this.onChange2 },
            { title1: "分區", title2: ["請選擇分區"], disabled: true, onChange: this.onChange3 },
            { title1: "屋苑/街道名稱", title2: ["請選擇屋苑/街道名稱"], disabled: true, onChange: this.onChange4 },
            { title1: "座數/期數/座名/街號", title2: ["請選擇座數/期數/座名/街號"], disabled: true, onChange: this.onChange5 }],
            value: { value1: "", value2: "", value3: "", value4: "" },
            load: false,
            timer: 0,
            class: undefined
        }
    }

    getClassId = async (x: string) => {
        const Body = {
            eleID: x,
            page: "sub",
            lang: "C"
        }
        const res = await fetch(`${process.env.NEXT_PUBLIC_API18}`, { method: "POST", headers: { "Content-Type": "application/json" }, body: JSON.stringify(Body) })
        const result: getClassId = await res.json();
        if (result.code === 0) this.setState({ class: result })
    }

    componentDidMount = () => {
        const Arr = [["serAdr", ""], ["planSid", ""]]
        for (let x of Arr) {
            // store.dispatch({ type: x[0], [`${x[0]}`]: x[1] })
            this.props.Arr({ type: x[0], [`${x[0]}`]: x[1] })
        }
        this.getClassId("NSSbtn3")
    }

    componentWillUnmount = () => clearInterval(this.state.timer)

    close = () => {
        const arr: { a: string, b: boolean }[] = [{ a: "com1", b: true }, { a: "com2", b: false }]
        for (let i of arr) {
            // store.dispatch({ type: i["a"], [i["a"]]: i["b"] })
            this.props.obj({ type: i["a"], [i["a"]]: i["b"] })
        }
    }

    componentDidUpdate = (preStatus: CombineRootState["searchByCata"] & CombineRootState["miscell"]) => {

        const change2 = () => {
            const newArr: string[] = []
            let isDisabled: boolean = true
            this.props.getAdrResult?.resultList?.forEach((item, i) => {
                newArr.push(item.district);
                if (item.processNext === "Y" && isDisabled == true) {
                    isDisabled = false;
                    return
                }
            })
            this.setState((state: State) => ({
                ...state, title: [...state.title.slice(0, 1),
                {
                    ...state.title[1],
                    title2: [...["請選擇分區"], ...newArr], disabled: isDisabled
                },
                ...state.title.slice(2, 4)]
            }))
            setTimeout(() => console.log(this.state.title), 0)
        }

        const change3 = () => {
            const newArr: string[] = []
            let isDisabled: boolean = true
            this.props.getAdrResult?.resultList?.forEach((item, i) => {
                newArr.push(item.estate);
                if (item.processNext === "Y" && isDisabled == true) {
                    isDisabled = false;
                    return
                }
            })

            this.setState((state: State) => ({
                ...state, title: [...state.title.slice(0, 2),
                {
                    ...state.title[2],
                    title2: [...["請選擇屋苑/街道名稱"], ...newArr], disabled: isDisabled
                },
                ...state.title.slice(3, 4)]
            }))
            setTimeout(() => console.log(this.state.title), 0);
        }

        const change4 = () => {
            const newArr: string[] = []
            let isDisabled: boolean = true
            this.props.getAdrResult?.resultList?.forEach((item, i) => {
                newArr.push(item.building);
                if (item.building && isDisabled == true) {
                    isDisabled = false;
                    return
                }
            })

            this.setState((state: State) => ({
                ...state, title: [...state.title.slice(0, 3),
                {
                    ...state.title[3],
                    title2: [...["請選擇座數/期數/座名/街號"], ...newArr], disabled: isDisabled
                },
                ...state.title.slice(4, 4)]
            }))
            setTimeout(() => console.log(this.state.title), 0);
        }

        const change5 = () => {
            const newArr: string[] = []
            let isDisabled: boolean = true
            this.props.getAdrResult?.resultList?.forEach((item, i) => {
                newArr.push(item.building);
                if (item.building && isDisabled == true) {
                    isDisabled = false;
                    return
                }
            })

            this.setState((state: State) => ({
                ...state, title: [...state.title],
            }))
            setTimeout(() => console.log(this.state.title), 0);
        }

        const confirmFinalSite = () => {
            this.props.getAdrResult?.resultList?.filter((item, _) => {
                if (item.building) {
                    return item.building === this.props.finalSite
                } else if (item.estate) {
                    return item.estate === this.props.finalSite
                }
            }).forEach((item2, _) => {
                console.log("finalResult", item2)
                if (item2.sid && item2.processNext === "N") {
                    const tempAdr = transferAddr(item2.area) + " " + item2.district + " " + item2.estate + " " + transferAddr(item2.building)
                    // store.dispatch({ type: "finalSite", finalSite: tempAdr })
                    this.props.finalSite({ type: "finalSite", finalSite: tempAdr })
                    // store.dispatch({ type: "planSid", planSid: item2.sid })
                    this.props.planSid({ type: "planSid", planSid: item2.sid })
                }
            })

        }

        if (preStatus.getAdrResult?.resultList !== this.props.getAdrResult?.resultList && preStatus.change2 !== this.props.change2) {
            change2();
            return
        }
        if (preStatus.getAdrResult?.resultList !== this.props.getAdrResult?.resultList && preStatus.change3 !== this.props.change3) {
            change3();
            return
        }
        if (preStatus.getAdrResult?.resultList !== this.props.getAdrResult?.resultList && preStatus.change4 !== this.props.change4) {
            change4();
            return
        }
        if (preStatus.getAdrResult?.resultList !== this.props.getAdrResult?.resultList && preStatus.change5 !== this.props.change5) {
            change5();
            return
        }
        if (preStatus.setPlanInfo !== this.props.setPlanInfo) {
            confirmFinalSite();
            setTimeout(() => console.log("sid", this.props.planSid, " finalSite", this.props.finalSite), 0);
            return
        }
    }



    goToSignup = (add: string | undefined) => {
        const Arr = [["com2", false], ["serAdr", add]]
        for (const x of Arr) {
            this.props.Arr({ type: x[0], [`${x[0]}`]: x[1] })
        }
        setTimeout(() => this.props.router.push('/page2'), 100)
    }


    inquiry = async () => {
        this.setState({ load: true })
        const Func = async () => {
            if (this.props.planSid) {
                const Body = {
                    sid: this.props.planSid,
                    lang: "C"
                }
                const res = await fetch(`${process.env.NEXT_PUBLIC_API3}`, { method: "POST", headers: { "Content-type": "application/json" }, body: JSON.stringify(Body) })
                const result: planServiceList = await res.json()
                console.log(result)
                if (result.code == 0 || 22101) {
                    // store.dispatch({ type: "planService", planService: result });
                    this.props.planService({ type: "planService", planService: result });
                    this.setState({ load: false })
                    this.goToSignup(this.props.finalSite)
                }
            }
        }
        this.setState({ timer: window.setInterval(Func, 5000) })
        setTimeout(() => { clearInterval(this.state.timer); this.setState({ load: false }) }, 10000)
    }



    public render() {
        return (
            <div className="vw-100 vh-100 d-flex justify-content-center align-items-center bg-dark bg-opacity-50 position-fixed start-0 top-0 z px-2 overflow-auto">
                <div className="outermost m-auto position-relative border border-secobdary rounded-3 py-1">
                    <i onClick={this.close} className="fa-solid fa-xmark position-absolute cross-icon fs-5"></i>
                    <div className="text-center my-3">以地址分類搜尋</div>
                    <div className="breakline"></div>
                    <section className="px-4 position-relative">
                        {this.state.load && <Spinner />}
                        <div className={`${this.state.load && "invisible"} title text-center my-4`}>請輸入地址</div>
                        <div>
                            {this.state.title.map((item, i) => {
                                return <div className="my-4"><SelectItem key={i} title1={item.title1} title2={item.title2}
                                    disabled={item.disabled} onChange={item.onChange} name={''} />
                                </div>
                            })}
                        </div>
                    </section>
                    <section className="px-4 pb-3 mt-5">
                        <div id={this.state.class?.result.htmlID} onClick={this.inquiry} className={`${this.state.class?.result.cssClass} col-12 color text-center bottom-btn py-2 rounded-pill`}>查詢</div>
                    </section>
                </div>
            </div>
        )
    }

    onChange2 = async (x: React.ChangeEvent<HTMLSelectElement>) => {
        this.setState((state: State) => ({ ...state, value: { ...state.value, value1: x.target.value } }));
        const callApi = async () => {
            const Body = {
                level: 2,
                keyword_1: this.state.value.value1,
                keyword_2: "",
                keyword_3: "",
                keyword_4: "",
                lang: "C"
            }
            console.log(Body)
            const res = await fetch(`${process.env.NEXT_PUBLIC_API2}`, { method: "POST", headers: { "content-type": "application/json" }, body: JSON.stringify(Body) })
            const result: Cata = await res.json();
            console.log(result)
            if (Body.level === 4 && result.code === 0) {
                const Arr = [["updateCata", "getAdrResult", result], ["change2", "change2", !this.props.change2], ["setPlanInfo", "setPlanInfo", !this.props.setPlanInfo], ["finalSite", "finalSite", this.state.value.value2]]
                for (let x of Arr) {
                    // store.dispatch({ type: x[0], [`${x[1]}`]: x[2] });
                    this.props.Arr({ type: x[0], [`${x[1]}`]: x[2] });
                }
                console.log("case 1")
            } else if (result.code === 0) {
                const Arr = [["updateCata", "getAdrResult", result], ["change2", "change2", !this.props.change2]];
                for (let x of Arr) {
                    // store.dispatch({ type: x[0], [`${x[1]}`]: x[2] })
                    this.props.Arr({ type: x[0], [`${x[1]}`]: x[2] })
                }
                console.log("case 2")
            } else if (result.code === 21201) {

                const Arr = [["setPlanInfo", "setPlanInfo", !this.props.setPlanInfo], ["finalSite", "finalSite", this.state.value.value4]]
                for (let x of Arr) {
                    // store.dispatch({ type: x[0], [`${x[1]}`]: x[2] });
                    this.props.Arr({ type: x[0], [`${x[1]}`]: x[2] });
                    console.log("case 3")
                }
            }
        }
        setTimeout(callApi, 0);
        if (!this.state.title[1].disabled) this.setState((state: State) => ({
            ...state, title: [...state.title.slice(0, 1),
            {
                ...state.title[1],
                title2: [...["請選擇分區"]], disabled: true
            }, {
                ...state.title[2],
                title2: [...["請選擇屋苑/街道名稱"]], disabled: true
            }, {
                ...state.title[3],
                title2: [...["請選擇座數/期數/座名/街號"]], disabled: true
            }]
        }))
    }
    onChange3 = async (x: React.ChangeEvent<HTMLSelectElement>) => {
        this.setState((state: State) => ({ ...state, value: { ...state.value, value2: x.target.value } }));
        const callApi = async () => {
            const Body = {
                level: 3,
                keyword_1: this.state.value.value1,
                keyword_2: this.state.value.value2,
                keyword_3: "",
                keyword_4: "",
                lang: "C"
            }
            console.log(Body)
            const res = await fetch(`${process.env.NEXT_PUBLIC_API2}`, { method: "POST", headers: { "content-type": "application/json" }, body: JSON.stringify(Body) })
            const result: Cata = await res.json();
            console.log(result)
            if (Body.level === 4 && result.code === 0) {
                const Arr = [["updateCata", "getAdrResult", result], ["change3", "change3", !this.props.change3], ["setPlanInfo", "setPlanInfo", !this.props.setPlanInfo], ["finalSite", "finalSite", this.state.value.value2]]
                for (let x of Arr) {
                    // store.dispatch({ type: x[0], [`${x[1]}`]: x[2] });
                    this.props.Arr({ type: x[0], [`${x[1]}`]: x[2] });
                }
                console.log("case 1")
            } else if (result.code === 0) {
                const Arr = [["updateCata", "getAdrResult", result], ["change3", "change3", !this.props.change3]];
                for (let x of Arr) {
                    // store.dispatch({ type: x[0], [`${x[1]}`]: x[2] })
                    this.props.Arr({ type: x[0], [`${x[1]}`]: x[2] })
                }
                console.log("case 2")
            } else if (result.code === 21201) {
                const Arr = [["setPlanInfo", "setPlanInfo", !this.props.setPlanInfo], ["finalSite", "finalSite", this.state.value.value4]]
                for (let x of Arr) {
                    // store.dispatch({ type: x[0], [`${x[1]}`]: x[2] });
                    this.props.Arr({ type: x[0], [`${x[1]}`]: x[2] });
                    console.log("case 3")
                }
            }
        }
        setTimeout(callApi, 0);
        if (!this.state.title[1].disabled) this.setState((state: State) => ({
            ...state, title: [...state.title.slice(0, 2),
            {
                ...state.title[2],
                title2: [...["請選擇屋苑/街道名稱"]], disabled: true
            }, {
                ...state.title[3],
                title2: [...["請選擇座數/期數/座名/街號"]], disabled: true
            }]
        }))
    }
    onChange4 = async (x: React.ChangeEvent<HTMLSelectElement>) => {
        this.setState((state: State) => ({ ...state, value: { ...state.value, value3: x.target.value } }));
        const callApi = async () => {
            const Body = {
                level: 4,
                keyword_1: this.state.value.value1,
                keyword_2: this.state.value.value2,
                keyword_3: this.state.value.value3,
                keyword_4: "",
                lang: "C"
            }
            console.log(Body)
            const res = await fetch(`${process.env.NEXT_PUBLIC_API2}`, { method: "POST", headers: { "content-type": "application/json" }, body: JSON.stringify(Body) })
            const result: Cata = await res.json();
            console.log(result)
            if (Body.level === 4 && result.code === 0) {
                const Arr = [["updateCata", "getAdrResult", result], ["change4", "change4", !this.props.change4], ["setPlanInfo", "setPlanInfo", !this.props.setPlanInfo], ["finalSite", "finalSite", this.state.value.value3]]
                for (let x of Arr) {
                    // store.dispatch({ type: x[0], [`${x[1]}`]: x[2] });
                    this.props.Arr({ type: x[0], [`${x[1]}`]: x[2] });
                }
                console.log("case 1")
            } else if (result.code === 0) {
                const Arr = [["updateCata", "getAdrResult", result], ["change4", "change4", !this.props.change4]];
                for (let x of Arr) {
                    // store.dispatch({ type: x[0], [`${x[1]}`]: x[2] })
                    this.props.Arr({ type: x[0], [`${x[1]}`]: x[2] })
                }
                console.log("case 2")
            } else if (result.code === 21201) {
                Body.level = 3
                const res = await fetch(`${process.env.NEXT_PUBLIC_API2}`, { method: "POST", headers: { "content-type": "application/json" }, body: JSON.stringify(Body) })
                const result: Cata = await res.json();
                const Arr = [["updateCata", "getAdrResult", result], ["setPlanInfo", "setPlanInfo", !this.props.setPlanInfo], ["finalSite", "finalSite", this.state.value.value3]]
                for (let x of Arr) {
                    // store.dispatch({ type: x[0], [`${x[1]}`]: x[2] });
                    this.props.Arr({ type: x[0], [`${x[1]}`]: x[2] });
                }
                console.log("case 3")
            }
        }
        setTimeout(callApi, 0);
        if (!this.state.title[1].disabled) this.setState((state: State) => ({
            ...state, title: [...state.title.slice(0, 3),
            {
                ...state.title[3],
                title2: [...["請選擇座數/期數/座名/街號"]], disabled: true
            }]
        }))
    }
    onChange5 = async (x: React.ChangeEvent<HTMLSelectElement>) => {
        this.setState((state: State) => ({ ...state, value: { ...state.value, value4: x.target.value } }));
        const callApi = async () => {
            const Body = {
                level: 4,
                keyword_1: this.state.value.value1,
                keyword_2: this.state.value.value2,
                keyword_3: this.state.value.value3,
                keyword_4: this.state.value.value4,
                lang: "C"
            }
            console.log(Body)
            const res = await fetch(`${process.env.NEXT_PUBLIC_API2}`, { method: "POST", headers: { "content-type": "application/json" }, body: JSON.stringify(Body) })
            const result: Cata = await res.json();
            console.log(result)
            if (Body.level === 4 && result.code === 0) {
                const Arr = [["change5", "change5", !this.props.change5], ["setPlanInfo", "setPlanInfo", !this.props.setPlanInfo], ["finalSite", "finalSite", this.state.value.value4]]
                for (let x of Arr) {
                    // store.dispatch({ type: x[0], [`${x[1]}`]: x[2] });
                    this.props.Arr({ type: x[0], [`${x[1]}`]: x[2] });
                }
                console.log("case 1")
            } else if (result.code === 0) {
                const Arr = [["updateCata", "getAdrResult", result], ["change5", "change5", !this.props.change5]];
                for (let x of Arr) {
                    // store.dispatch({ type: x[0], [`${x[1]}`]: x[2] })
                    this.props.Arr({ type: x[0], [`${x[1]}`]: x[2] })
                }
                console.log("case 2")
            } else if (result.code === 21201) {
                Body.level = 3
                const res = await fetch(`${process.env.NEXT_PUBLIC_API2}`, { method: "POST", headers: { "content-type": "application/json" }, body: JSON.stringify(Body) })
                const result: Cata = await res.json();
                const Arr = [["updateCata", "getAdrResult", result], ["setPlanInfo", "setPlanInfo", !this.props.setPlanInfo], ["finalSite", "finalSite", (this.state.value.value4 || this.state.value.value3)]]
                for (let x of Arr) {
                    // store.dispatch({ type: x[0], [`${x[1]}`]: x[2] });
                    this.props.Arr({ type: x[0], [`${x[1]}`]: x[2] });
                }
                console.log("case 3")
            }
        }
        setTimeout(callApi, 0);
    }
}


function transferAddr(Addr: string) {
    const y = "chinese"
    switch (Addr) {
        case "HK":
        case "香港":
            return (y === "chinese") ? "香港" : "HK"
        case "KLN":
        case "九龍":
            return (y === "chinese") ? "九龍" : "KLN"
        case "新界":
        case "NT":
            return (y === "chinese") ? "新界" : "NT"
        case null:
            return (y === "chinese") ? "" : ""
        default:
            return Addr
    }
}


const MaptoState = (state: CombineRootState) => {
    return {
        ...state.searchByCata,
        ...state.miscell
    }
}

const MaptoDispatch = (dispatch:Dispatch<any>) => ({
    Arr:(x:any) => dispatch({...x}),
    obj:(x:any) => dispatch({...x}),
    finalSite:(x:{ type:string, finalSite:string }) => ({...x}),
    planSid:(x:{ type: string, planSid: string}) => dispatch({...x}),
    planService:(x:{ type: string, planService: planServiceList }) => dispatch({...x})
})

const connector = connect(MaptoState,MaptoDispatch)
export default withRouter(connector(SearchByCata));



const Spinner = () => {
    return (
        <div id="load">
            <div>G</div>
            <div>N</div>
            <div>I</div>
            <div>D</div>
            <div>A</div>
            <div>O</div>
            <div>L</div>
        </div>
    )
}