import dayjs from 'dayjs'
import React from 'react'
import { textMarshal } from 'text-marshal'
import PageCardHTML from './PageCardHTML'
import TermHTML from './TermHTML'
//@ts-ignore
import { getCreditCardNameByNumber } from 'creditcard.js'
import { CombineRootState, getProcess, planServiceResultList } from '../redux/store'
import { body } from '../function/MetaData'
import { getPlanPaymentInfo } from '../type/type'




type CardInfoPage5 = { Title: string, Item: string, Total: string, Detail: { Content: string, Amount: string, underLine: boolean }[] }

type info = {
    info: (getProcess["result"]) | undefined
}

type TncContent = {
    tncID: number
    content: string
    is_Compulsory: boolean
    is_Checked: boolean
}

export type Tnc = {
    code: number,
    msg: string,
    result: {
        tnc: string
        tncDetails: TncContent[]
    }
}

interface State {
    planInfo: getPlanPaymentInfo
    result: {
        tnc: string
        tncDetails: TncContent[]
    }
    termIndex: boolean[]
    term: { tncID: number }[]
    term2: boolean
    errMsgOn: boolean
    loader: boolean
    tncErrMsgOn: boolean[]
}

type Props = {
    miscell: CombineRootState["miscell"]
    planService: CombineRootState["planService"]
    cardInfo: CombineRootState["cardInfo"]
    processInfo: CombineRootState["processInfo"]
    result: {
        tnc: string
        tncDetails: TncContent[]
    }
    payment: getPlanPaymentInfo
    imgUrl: string
}


class ViewHTML2 extends React.Component<Props, State>{
    public term: { tncID: number }[]
    constructor(props: Props) {
        super(props)
        this.term = []
        this.state = {
            planInfo: {
                code: 0, msg: "", resultList: [{
                    groupNum: 0,
                    groupName: "",
                    itemNum: 0,
                    itemName: "",
                    priceStr: ""
                }]
            },
            result: {
                tnc: "",
                tncDetails: []
            },
            termIndex: [],
            term: [],
            term2: false,
            errMsgOn: false,
            loader: false,
            tncErrMsgOn: [],
        }
    }


    getPlanService = () => {
        const filterPlan: any = this.props.planService.planService?.resultList.filter((item, _) => {
            if (item.planCode === this.props.miscell.selectedPlanCode) {
                return item
            }
        })
        return filterPlan[0]
    }


    public render(): React.ReactNode {
        return (
            <>
                <table style={{ backgroundColor: "#ffff", fontFamily: "\\\"Noto Sans HK\\\",  sans-serif;font-family: \\\"Noto Sans TC\\\", sans-serif", fontSize: "16px", lineHeight: "14px", minWidth: "550px", maxWidth: "880px", width: "auto", margin: "auto", padding: "0px 40px 5px 40px", textAlign: "left" }}><tr><td>

                    <table style={{ padding: "10px 0px 10px 0px", width: "100%" }}>
                        <td style={{ width: "70px" }}>
                            <img style={{ maxHeight: "30px", width: "auto" }} alt="#" src='https://renewal.i-cable.com/img/i-cable_80x.png' />
                        </td>
                        <td align='left'>
                            <img style={{ maxHeight: "40px", width: "auto" }} alt="#" src='https://subscription.cabletv.com.hk//img/cabletv.jpg' />
                        </td>
                        <td align='right'>
                            <h3 style={{ fontSize: "18px", fontWeight: "lighter", textDecoration: "none!important" }}>網上登記</h3>
                        </td>
                    </table>
                    <h2 style={{ fontSize: "18px", fontWeight: "lighter", textAlign: "center" }}>確認資料</h2>
                    <h2 style={{ fontSize: "18px", fontWeight: "lighter", textAlign: "center" }}>{`你的網上登記編號：${this.props.miscell.processId}`}</h2>
                    <PageRowOrdered info={this.getPlanService()} imgUrl={this.props.imgUrl} />
                    <br />
                    <br />
                    <PageRowBasic info={this.props.processInfo.body} />
                    <br />
                    <br />
                    <PageRowInstall info={this.props.processInfo.body} />
                    <br />
                    <br />
                    <PageRowContact info={this.props.processInfo.body} />
                    <div className="html2-df_page-break page-break"></div>
                    <br />
                    <br />
                    <PageRowMonth code={0} msg={''} resultList={this.props.payment.resultList} />
                    <br />
                    <br />
                    <PageRowPayment body={this.props.cardInfo.body} />
                    <br />
                    <br />
                    <table style={{ width: "100%", lineHeight: "normal", padding: "40px 0px 40px 0px" }}>
                        {this.props.result.tncDetails.filter((item, _) => item.is_Compulsory === true).map((item, i) => {
                            return <>
                                <tr style={{ verticalAlign: "top", height: "70px" }}>
                                    <td>
                                        <img src="https://subscription.cabletv.com.hk/img/checked.png" />
                                    </td>
                                    <td style={{ paddingLeft: "16px", lineHeight: "20px", fontSize: "16px", fontWeight: "lighter" }}><TermHTML key={i} termContent={item.content} name={`term${i + 1}`} errMsgOn={this.state.tncErrMsgOn[i]} errMsgContent={'請勾選該欄位'} /></td>
                                </tr>
                            </>
                        })}
                        <tr style={{ verticalAlign: "top", height: "70px" }}>
                            <td>
                                <img src="https://subscription.cabletv.com.hk/img/checked.png" />
                            </td>
                            <td style={{ paddingLeft: "16px", lineHeight: "20px", fontSize: "16px", fontWeight: "lighter" }}>本人已詳閱並同意 服務計劃條款及細則 及 一般條款及細則 ，本人同意此項申請被接納後受該等條款及細則所約束。</td>
                        </tr>
                    </table>
                    <br />
                    <br />
                    <table style={{ width: "100%", textAlign: "center", borderCollapse: "collapse" }}>
                        <tr style={{ borderTop: "1px solid #DEE2E6" }}>
                            <td >
                                <p style={{ fontSize: "14px", fontWeight: "lighter" }}>© 2022 Hong Kong Cable Television Limited. All Rights Reserved.</p>
                            </td>
                        </tr>
                    </table>
                </td></tr></table>
            </>
        )
    }
}



const PageRowOrdered = (prop: { info: planServiceResultList, imgUrl: string }) => {
    return (
        <>
            <PageCardHTML outline={false} caption={false} planSerResult={prop.info} imgUrl={prop.imgUrl} />
        </>
    )
}

const PageRowBasic = (prop: info) => {
    const data = textMarshal({
        input: prop.info?.identityNo,
        template: "xxxx****",
        disallowCharacter: [/[a-z]/],
    })
    return (
        <>
            <table style={body.style6}>
                <tr>
                    <td colSpan={5} style={body.style7}>
                        <h2 style={body.style8} >基本資料</h2>
                    </td>
                </tr>
                <tr>
                    <td colSpan={5} style={{ paddingBottom: "10px" }}>&nbsp;</td>
                </tr>
                <tr >
                    <td><h4 style={body.style27}>性別</h4></td>
                    <td><h4 style={body.style27}>姓氏</h4></td>
                    <td><h4 style={body.style27}>名字</h4></td>
                    <td><h4 style={body.style27}>出生日期 (YYYY/MM/DD)</h4></td>
                    <td><h4 style={body.style27}>護照號碼</h4></td>
                </tr>
                <tr>
                    <td><h3 style={body.style28}>{prop.info?.gender === "M" ? "男" : "女"}</h3></td>
                    <td><h3 style={body.style28}>{prop.info?.surname}</h3></td>
                    <td><h3 style={body.style28}>{prop.info?.givenname}</h3></td>
                    <td><h3 style={body.style28}>{`${prop.info?.dateOfBirthYrs}/${prop.info?.dateOfBirthMth}/${prop.info?.dateOfBirthDay}`}</h3></td>
                    <td><h3 style={body.style28}>{data.marshaltext}</h3></td>
                </tr>
            </table>
        </>
    )
}

const PageRowInstall = (prop: info) => {
    return (
        <>
            <table style={body.style6}>
                <tr>
                    <td colSpan={3} style={body.style7}                                      >
                        <h2 style={body.style8} >安裝詳情</h2>
                    </td>
                </tr>
                <tr>
                    <td colSpan={3} style={{ paddingBottom: "10px" }}>&nbsp;</td>
                </tr>
                <tr >
                    <td><h4 style={body.style27}>預約安裝日期</h4></td>
                    <td><h4 style={body.style27}>預約安裝時間</h4></td>
                    <td><h4 style={body.style27}>地址</h4></td>
                </tr>
                <tr>
                    <td><h3 style={body.style28}>{prop.info?.appoDate}</h3></td>
                    <td><h3 style={body.style28}>{prop.info?.appoTimeStr}</h3></td>
                    <td><h3 style={{ fontSize: "16px", fontWeight: "normal", wordWrap: "break-word" }}>{prop.info?.addrDiff === "Y"?prop.info.commsAddress:`${prop.info?.commsAddress} ${prop.info?.instFloor && prop.info?.instFloor + "樓"} ${prop.info?.instRoom && prop.info?.instRoom + "室"}`}</h3></td>
                </tr>
            </table>
        </>
    )
}


const PageRowContact = (prop: info) => {
    return (
        <>
            <table style={body.style6}>
                <tr>
                    <td colSpan={3} style={body.style7}>
                        <h2 style={body.style8} >聯絡資料</h2>
                    </td>
                </tr>
                <tr>
                    <td colSpan={3} style={body.style9}>&nbsp;</td>
                </tr>
                <tr >
                    <td><h4 style={body.style27}>通訊語言選擇</h4></td>
                    <td><h4 style={body.style27}>手提電話</h4></td>
                    <td><h4 style={body.style27}>住宅電話</h4></td>
                </tr>
                <tr>
                    <td><h3 style={body.style28}>{prop.info?.commsLang === "C" ? "英文" : "中文"}</h3></td>
                    <td><h3 style={body.style28}>{prop.info?.contactMobile}</h3></td>
                    <td><h3 style={body.style28}>{prop.info?.contactHome}</h3></td>

                </tr>
                <tr >
                    <td><h4 style={body.style27}>電郵地址</h4></td>
                    <td><h4 style={body.style27}>通訊地址</h4></td>
                </tr>
                <tr>
                    <td><h3 style={body.style28}>{prop.info?.email}</h3></td>
                    <td><h3 style={{ fontSize: "16px", fontWeight: "normal", wordWrap: "break-word" }}>{prop.info?.addrDiff === "Y"?prop.info.commsAddress:`${prop.info?.commsAddress} ${prop.info?.instFloor && prop.info?.instFloor + "樓"} ${prop.info?.instRoom && prop.info?.instRoom + "室"}`}</h3></td>
                </tr>
            </table>
        </>
    )
}


const PageRowMonth = (prop: getPlanPaymentInfo) => {
    // const [open, setOpen] = useState(false)
    // const [open2, setOpen2] = useState(false)
    const Arr: CardInfoPage5[] = []
    return (
        <>
            <table style={body.style6}>
                <tr>
                    <td style={body.style7}>
                        <h2 style={body.style8}>收費詳情</h2>
                    </td>
                </tr>
                <tr>
                    <td style={body.style23}>&nbsp;</td>
                </tr>
            </table>
            {prop.resultList.reduce((prevItem, currentItem) => {
                console.log("currentItem", currentItem)
                console.log("prevItem", prevItem)
                const func = () => {
                    let y = true
                    for (let x of prevItem) {
                        if (x.Title === currentItem.groupName && currentItem.itemNum === 99) {
                            x.Total = currentItem.priceStr
                            x.Item = currentItem.itemName
                            return
                        } else if (x.Title === currentItem.groupName) {
                            x.Detail = [...x.Detail, { Content: currentItem.itemName, Amount: currentItem.priceStr, underLine: false }]
                            y = false
                            return
                        }
                    }
                    return y
                }
                if (func()) prevItem = [...prevItem, { Title: currentItem.groupName, Item: "", Total: "", Detail: [{ Content: currentItem.itemName, Amount: currentItem.priceStr, underLine: false }] }]
                console.log("finalPrevItem", prevItem)
                return prevItem
            }, Arr).map((item, i) => {
                return (
                    <>
                        <table cellPadding={0} style={body.style10}>
                            <tr>
                                <td colSpan={2} style={body.style11}>&nbsp;</td>
                            </tr>
                            <tr>
                                <td style={body.style12}><h3 style={{ fontSize: "16px", fontWeight: "lighter" }}>{item.Item}</h3></td>
                                <td style={body.style20}><h3 style={{ fontSize: "18px", fontWeight: "normal", color: "red", textAlign: "right" }}>{item.Total}</h3></td>
                            </tr>
                            <tr style={body.style24}>
                                <td style={body.style12}><h4 style={{ fontSize: "14px", fontWeight: "lighter" }}>項目</h4></td>
                                <td style={body.style20}><h4 style={{ fontSize: "14px", fontWeight: "lighter", textAlign: "right" }}>金額 (HK$)</h4></td>
                            </tr>
                            {item.Detail.map(item => {
                                return (
                                    <tr style={body.style24}>
                                        <td style={body.style12}><h4 style={{ fontSize: "14px", fontWeight: "lighter" }}>{item.Content}</h4></td>
                                        <td style={body.style20}><h4 style={{ fontSize: "14px", fontWeight: "lighter", textAlign: "right" }}>{item.Amount}</h4></td>
                                    </tr>
                                )
                            })}
                            <tr>
                                <td colSpan={2} style={body.style37}>&nbsp;</td>
                            </tr>
                        </table>
                        <br />
                    </>
                )
            })}
        </>
    )
}


const PageRowPayment = (prop: CombineRootState["cardInfo"]) => {
    const data = textMarshal({
        input: prop.body.card_no,
        template: "xxxx************",
        disallowCharacter: [/[a-z]/],
    })
    const MM = dayjs(prop.body.expiry_date).format('MM')
    const YY = dayjs(prop.body.expiry_date).format('YY')
    return (
        <>


            <table style={body.style6}>
                <tr>
                    <td colSpan={4} style={{ paddingLeft: "16px", borderLeft: "2px solid red" }}>
                        <h2 style={{ fontSize: "20px", fontWeight: "normal" }}>付款資料</h2>
                    </td>
                </tr>
                <tr>
                    <td colSpan={4} style={body.style37}>&nbsp;</td>
                </tr>
                <tr >
                    <td><h4 style={body.style27}>信用卡號碼</h4></td>
                    <td><h4 style={body.style27}>信用卡種類</h4></td>
                    <td><h4 style={body.style27}>持卡人姓名</h4></td>
                    <td><h4 style={body.style27}>有效期至 </h4></td>
                </tr>
                <tr>
                    <td><h3 style={body.style47}>{data.marshaltext}</h3></td>
                    <td><h3 style={body.style47}>{getCreditCardNameByNumber(prop.body.card_no).toUpperCase()}</h3></td>
                    <td><h3 style={body.style47}>{prop.body.card_holder}</h3></td>
                    <td><h3 style={body.style47}>{`${MM}/${YY}`}</h3></td>
                </tr>

            </table>
        </>
    )
}

export default ViewHTML2;