import React, { Dispatch } from 'react'
import { connect ,ConnectedProps} from 'react-redux'



type Props = {
    termContent: string | JSX.Element
    name: string
    errMsgOn: boolean
    errMsgContent: string
    onChange?: (x:any) => void
    value?:boolean
    checked?:boolean
} & ConnectedProps<typeof connector>

interface State {
    value: boolean
}

class Term2 extends React.Component<Props, State>{
    public state: State
    constructor(props: Props) {
        super(props)
        this.state = {
            value: false
        }
    }

    goToTAndC = () => {
        const Arr = [["com1", false], ["com2", false], ["com3", false], ["com4", false], ["com5", false], ["com6", false], ["com7", false], ["com8", true], ["com9", false],["com10",true],["com11",false]]
        for (let x of Arr) {
            // store.dispatch({ type: x[0], [`${x[0]}`]: x[1] })
            this.props.Arr({ type: x[0], [`${x[0]}`]: x[1] })
        }
        document.body.style.overflow = "hidden"
    }

    public render(): React.ReactNode {
        return (
            <div className="d-flex checkbox ">
                <div className="me-3"><input name={this.props.name} value={this.props.value?.toString()} onChange={this.props.onChange || (() => this.setState(state => ({ value: !state.value })))} className="position-relative" type="checkbox" checked={this.props.checked} /></div>
                <div>
                    <div className="text">{this.props.termContent || <>本人已詳閱並同意 <span onClick={this.goToTAndC} className="fw-bold text-decoration-underline cursor">服務計劃條款及細則</span> 及 <span onClick={this.goToTAndC} className="fw-bold text-decoration-underline cursor">一般條款及細則</span> ，本人同意此項申請被接納後受該等條款及細則所約束。</>}</div>
                    {(this.props.errMsgOn && (!this.props.value && !this.state.value)) ? <div className=" mt-3 border-0 border-start border-3 b-red text-danger fs-8 error-text error-bg p-3">{this.props.errMsgContent || "請輸入你的地址關鍵詞 (如大廈屋苑名稱)"}</div> : ""}
                </div>
            </div>
        )
    }
}

const MaptoDispatch = (dispatch:Dispatch<any>) => ({
    Arr:(x:any) => dispatch({...x})
})

const connector = connect(null,MaptoDispatch)
export default connector(Term2);