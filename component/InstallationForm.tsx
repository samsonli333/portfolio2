import React from 'react'
import { connect } from 'react-redux';
import { CombineRootState } from '../redux/store';
import { Appointment, errMsgOn, onChange, personalData, value } from '../type/type';
import Form2 from './Form2';
import FormCalendar from './FormCalendar';
import SelectItem2 from './SelectItem2';






type Prop = {
    value: value 
    time: string[]
    Appointment: Appointment[],
    onChange?: onChange
    selectChange: (x: React.ChangeEvent<HTMLSelectElement>) => void
    disable: boolean
    nameGroup: {
        name1: string
        name2: string
    }
    errMsgOn: errMsgOn
    selectedValue: string | undefined
    innerRef?: React.ForwardedRef<any>
    personalData:personalData | undefined
}


class InstallationForm extends React.Component<Prop & CombineRootState["switchCom"]>{
    constructor(props: Prop & CombineRootState["switchCom"]) {
        super(props)
    }



    public render(): React.ReactNode {
        return (
            <div ref={this.props.innerRef} className="installation-form">
                <div className="border-0 border-start b-red border-3 ps-2 mb-2 fs-5 fw-semibold">安裝詳情</div>
                <FormCalendar errMsgOn={this.props.errMsgOn.errMsgOn8} title={'您的預約安裝日期'} title2={this.props.value.value1} onChange={this.props.onChange?.onChange1} Appointment={this.props.Appointment} />
                <div className="install-text">預約日期必需為30天內</div>
                <SelectItem2 errMsgOn={this.props.errMsgOn.errMsgOn9} onChange={this.props.selectChange} value={this.props.selectedValue} title1={'預約安裝時間'} title2={this.props.time} disabled={this.props.disable} name={''} />
                <div className="text-start fw-bold"><span className="fs-8 fw-normal">地址</span><br />{this.props.serAdr || this.props.personalData?.CommAddress}</div>
                <div className="d-flex">
                    <Form2 spanName={'樓層'} onChange={this.props.onChange?.onChange2} errMsgOn={this.props.errMsgOn.errMsgOn12} inputBackground={'bg-transparent'} width={'w-50'} value={this.props.value.value2 } name={this.props.nameGroup.name1} errMsgContent={''} />
                    <div className="me-2"></div>
                    <Form2 spanName={'室'} errMsgOn={this.props.errMsgOn.errMsgOn13} onChange={this.props.onChange?.onChange3} inputBackground={'bg-transparent'} width={'w-50'} value={this.props.value.value3 } name={this.props.nameGroup.name2} errMsgContent={''} />
                </div>
            </div>
        )
    }
}

const MaptoState = (state: CombineRootState) => {
    return state.switchCom
}

const newInstallationForm = React.forwardRef((props: Prop & CombineRootState["switchCom"], ref: React.ForwardedRef<any>) => <InstallationForm {...props} innerRef={ref} />);

export default connect(MaptoState, null, null, { forwardRef: true })(newInstallationForm);