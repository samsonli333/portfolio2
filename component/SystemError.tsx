import React, { Dispatch } from 'react'
import { WithRouterProps } from 'next/dist/client/with-router'
import { withRouter } from 'next/router'
import { connect,ConnectedProps } from 'react-redux'

type Props = WithRouterProps & ConnectedProps<typeof connector>
class SystemError extends React.Component<Props>{
    constructor(props: Props) {
        super(props)
    }


    goToPage1 = () => {
        // store.dispatch({ type: "com12", com12: false })
        this.props.swCom({ type: "com12", com12: false })
        this.props.router.push('/')
    }

    public render(): React.ReactNode {
        return (
            <div>
                <div className="position-fixed px-3 w-100 h-100 bg-dark bg-opacity-50 d-flex justify-content-center align-items-center inquiry-outermost">
                    <div className="inquiry-width rounded-3 bg-white py-3 text-center">
                        <div className="fs-6 pt-1 pb-3 border-0 border-2 border-bottom b-red">多謝查詢</div>
                        <div className="px-4">
                            <div className="py-3 pb-5">系統訊息錯誤</div>
                            <div onClick={this.goToPage1} className="rounded-pill db-red text-white py-2 mb-3 cursor">返回主頁</div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}


const MaptoDispatch = (dispatch:Dispatch<any>) => ({
    swCom:(x:{ type: "com12", com12: false }) => dispatch({...x})
})

const connector = connect(null,MaptoDispatch)
export default withRouter(connector(SystemError));