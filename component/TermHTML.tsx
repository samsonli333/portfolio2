import React from 'react'




type Props = {
    termContent: string | JSX.Element
    name: string
    errMsgOn: boolean
    errMsgContent: string
    onChange?: (x:any) => void
    value?:boolean
} 

interface State {
    value: boolean
}

class TermHTML extends React.Component<Props, State>{
    public state: State
    constructor(props: Props) {
        super(props)
        this.state = {
            value: false
        }
    }

    // goToTAndC = () => {
    //     const Arr = [["com1", false], ["com2", false], ["com3", false], ["com4", false], ["com5", false], ["com6", false], ["com7", false], ["com8", false], ["com9", false],["com10",true],["com11",false]]
    //     for (let x of Arr) {
    //         store.dispatch({ type: x[0], [`${x[0]}`]: x[1] })
    //     }
    // }

    public render(): React.ReactNode {
        return (
            <div  className="d-flex checkbox ">
                {/* <div style={{marginRight: "1rem!important"}} className="me-3"><input style={{appearance:"none"}} name={this.props.name} value={this.props.value?.toString()} onChange={this.props.onChange || (() => this.setState(state => ({ value: !state.value })))} className="position-relative" type="checkbox" /></div> */}
                <div>
                    <div className="text">{this.props.termContent || <>本人已詳閱並同意 <span style={{fontWeight: "700!important"}} className="fw-bold text-decoration-underline cursor">服務計劃條款及細則</span> 及 <span className="fw-bold text-decoration-underline cursor">一般條款及細則</span> ，本人同意此項申請被接納後受該等條款及細則所約束。</>}</div>
                    {(this.props.errMsgOn && !this.state.value) ? <div className=" mt-3 border-0 border-start border-3 b-red text-danger fs-8 error-text error-bg p-3">{this.props.errMsgContent || "請輸入你的地址關鍵詞 (如大廈屋苑名稱)"}</div> : ""}
                </div>
            </div>
        )
    }
}





export default TermHTML;