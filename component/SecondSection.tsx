import React, { Dispatch, FormEvent } from 'react'
import { connect ,ConnectedProps } from 'react-redux';
import { checkEmail, checkTel } from '../function/generalFunc';
import { CombineRootState } from '../redux/store';
import { success } from '../type/type';
import Spinner from './Spinner';
import SubmitForm from './SubmitForm';
import Term from './Term';





interface State {
    errMsg: {
        errMsg1: { errMsgOn: boolean, errMsgContent: string, errFunc: (email: string) => "" | JSX.Element, swFunc: boolean },
        errMsg2: { errMsgOn: boolean, errMsgContent: string, errFunc: (email: string) => "" | JSX.Element, swFunc: boolean },
        errMsg3: { errMsgOn: boolean, errMsgContent: string, errFunc: (tel: string) => "" | JSX.Element, swFunc: boolean },
        errMsg4: { errMsgOn: boolean, errMsgContent: string, errFunc: (email: string) => "" | JSX.Element, swFunc: boolean },
        errMsg5: { errMsgOn: boolean, errMsgContent: string, errFunc: (email: string) => "" | JSX.Element, swFunc: boolean },
        errMsg6: { errMsgOn: boolean, errMsgContent: string, errFunc: (email: string) => "" | JSX.Element, swFunc: boolean }
    },
    name: { name1: string, name2: string, name3: string, name4: string, name5: string, name6: string },
    spin: boolean
}

type Props = CombineRootState["miscell"] & CombineRootState["switchCom"] & ConnectedProps<typeof connector>

class SecondSection extends React.Component<Props, State> {
    constructor(props: Props) {
        super(props)
        this.state = {
            errMsg: {
                errMsg1: { errMsgOn: false, errMsgContent: "", errFunc: () => <></>, swFunc: false },
                errMsg2: { errMsgOn: false, errMsgContent: "", errFunc: () => <></>, swFunc: false },
                errMsg3: { errMsgOn: false, errMsgContent: "", errFunc: () => <></>, swFunc: false },
                errMsg4: { errMsgOn: false, errMsgContent: "", errFunc: () => <></>, swFunc: false },
                errMsg5: { errMsgOn: false, errMsgContent: "", errFunc: () => <></>, swFunc: false },
                errMsg6: { errMsgOn: false, errMsgContent: "", errFunc: () => <></>, swFunc: false }
            },
            name: { name1: "name1", name2: "name2", name3: "name3", name4: "name4", name5: "name5", name6: "name6" },
            spin: false
        }
    }

    componentDidMount = () => {
     console.log("Second-com5",this.props.com5)
    }
        
    

    onSubmit = async (e: FormEvent<HTMLFormElement>) => {
        e.preventDefault();
        this.setState((state: State) => ({
            ...state,
            errMsg: {
                ...state.errMsg,
                errMsg1: {
                    ...state.errMsg.errMsg1,
                    errMsgOn: true
                },
                errMsg2: {
                    ...state.errMsg.errMsg2,
                    errMsgOn: true
                },
                errMsg3: {
                    ...state.errMsg.errMsg2,
                    errMsgOn: true,
                    errFunc: checkForm2
                },
                errMsg4: {
                    ...state.errMsg.errMsg2,
                    errMsgOn: true,
                    errFunc: checkForm
                },
                errMsg5: {
                    ...state.errMsg.errMsg2,
                    errMsgOn: true
                },
                errMsg6: {
                    ...state.errMsg.errMsg2,
                    errMsgOn: true
                }
            }
        }))

        const submitForm: any = document.querySelector('#submit-form');
        (submitForm.name1.value && submitForm.name2.value && submitForm.name3.value &&
            submitForm.name4.value && (submitForm.name5.value === '') && (submitForm.name6.value === ''))
            && this.setState((state: State) => ({
                ...state,
                errMsg: {
                    ...state.errMsg,
                    errMsg4: {
                        ...state.errMsg.errMsg4,
                        swFunc: true
                    },
                    errMsg3: {
                        ...state.errMsg.errMsg3,
                        swFunc: true
                    }
                }
            }));
        if ((!checkEmail(submitForm.name4.value)) && (!checkTel(submitForm.name3.value)) && submitForm.name5.checked  && submitForm.name6.checked) {
            this.setState({spin:true});
            const Body = {
                fullname: submitForm.name1.value,
                contactNo: submitForm.name2.value + submitForm.name3.value,
                email: submitForm.name4.value,
                address: this.props.serAdr,
                sid: this.props.planSid,
                noCoverage: "N",
                lang: "C"
            }
            const res = await fetch(`${process.env.NEXT_PUBLIC_API7}`, { method: "POST", headers: { "Content-Type": "application/json" }, body: JSON.stringify(Body) })
            const result: success = await res.json();
            if (result.code === 0 && result.msg === "success") {
                setTimeout(() => this.setState({ spin: false }),1000);
                setTimeout(() => this.submit(),1500);
            }
        }
    }

    submit() {
        console.log("Second-Submit-com5",this.props.com5)
        // store.dispatch({type:"com5",com5:true})
        this.props.swCom({type:"com5",com5:true})
    }

    public render() {
        return (
            <div className="section-inquiry border-top py-3">
                <div className="text-center">
                    <span className="dt-red fs-5 fw-bold">多謝查詢</span>
                    <br />請提供以下資料，我們將會盡快與您聯絡跟進有關的服服事宜。
                </div>
                <form id="submit-form" onSubmit={this.onSubmit}>
                    <SubmitForm errMsg={this.state.errMsg} name={this.state.name} />
                    <Term termContent={<span>本人已閱讀及明白<span style={{textDecoration:"underLine"}}>個人資料私隱政策</span>之內容，尤其是本人知悉可行駛選擇權拒絕以上本人之個人資料所上述之直接促銷用途。</span>} name={this.state.name.name5} errMsgOn={this.state.errMsg.errMsg5.errMsgOn} errMsgContent={'必須同意個人資料私隱政策'} />
                    <div className="my-1"></div>
                    <Term termContent={"如閣下在下方方格表明不希望收到上述宣傳及推廣資料，則閣下將不會收到相關資料。本人拒絕接收上述宣傳及推廣資料。"} name={this.state.name.name6} errMsgOn={this.state.errMsg.errMsg6.errMsgOn} errMsgContent={'須同意個人資料私隱政策'} />
                    <button className={`${this.state.spin ? "text-light" : ""} rounded-pill mt-5 p-3 w-100 bg-white text-center submit-btn cursor position-relative`}>{this.state.spin && <Spinner />}提交</button>
                </form>
            </div>
        )
    }
}


export function checkForm(email: string) {
    return (checkEmail(email) ? <div className="text-danger fs-8 error-text">電郵地址無效</div> : "")
}


export function checkForm2(tel: string) {
    return (checkTel(tel) ? <div className="text-danger fs-8 error-text">電話號碼無效</div> : "")
}


const MaptoState = (state: CombineRootState) => {
    return {
        ...state.miscell,
        ...state.switchCom
    }
}

const MaptoDispatch = (dispatch:Dispatch<any>) => ({
    swCom:(x:{type:string,com5:boolean}) => dispatch({...x})
})

const connector = connect(MaptoState,MaptoDispatch)
export default connector(SecondSection);