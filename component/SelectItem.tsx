import React from 'react'
import { connect } from 'react-redux'
import { CombineRootState } from '../redux/store'



type Prop = {
    title1: string,
    title2: string[]
    disabled?: boolean
    onChange?:(x: React.ChangeEvent<HTMLSelectElement>) => void
    name:string
    errMsgOn?:boolean
    value?:string
}


interface State{
    value:string
}

class SelectItem extends React.Component<Prop & CombineRootState["searchByCata"],State> {
    constructor(props: Prop & CombineRootState["searchByCata"]) {
        super(props)
        this.state = {
            value:""
        }
    }

    public render(): React.ReactNode {
        return (
            <>
                <div className="form-floating">
                    <select onChange={this.props.onChange || (e => this.setState({value:e.target.value}))} name={this.props.name} className={`form-select text-dark ${(this.props.errMsgOn && !this.props.value && !this.state.value) && "b-red"}`}  defaultValue={this.props.value || 0} id="floatingSelect" aria-label="Floating label select example" disabled={this.props.disabled}>
                        {this.props.title2.map((item, i) => {
                            return <option key={i} value={item} >{item}</option>
                        })}
                    </select>
                    <label className="text-secondary text-opacity-50 special-color" htmlFor="floatingSelect">{this.props.title1}</label>
                </div>
            </>
        )
    }
}

const MaptoState = (state:CombineRootState) => {
    return state.searchByCata
}
export default connect(MaptoState)(SelectItem);