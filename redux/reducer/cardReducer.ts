import { cardInfo, cardInitialState } from "../store";

export default function cardReducer (state:cardInfo = cardInitialState,action:any){
    switch(action.type){
        case"body":
        return {
            ...state,
            body:action.body
        }
        default:
            return state
    }
    
}