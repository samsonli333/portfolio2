import { cominitialState, switchCom } from "../store"




export function comReducer(state: switchCom = cominitialState, action: any): switchCom {
    switch (action.type) {
        case "com1":
            return {
                ...state,
                com1: action.com1
            }
        case "com2":
            return {
                ...state,
                com2: action.com2
            }
        case "com3":
            return {
                ...state,
                com3: action.com3
            }
        case "com4":
            return {
                ...state,
                com4: action.com4
            }
        case "com5":
            return {
                ...state,
                com5: action.com5
            }
        case "com6":
            return {
                ...state,
                com6: action.com6
            }
        case "com7":
            return {
                ...state,
                com7: action.com7
            }
        case "com8":
            return {
                ...state,
                com8: action.com8
            }
        case "com9":
            return {
                ...state,
                com9: action.com9
            }
        case "com10":
            return {
                ...state,
                com10: action.com10
            }
        case "com11":
            return {
                ...state,
                com11: action.com11
            }
        case "serAdr":
            return {
                ...state,
                serAdr: action.serAdr
            }
        case "com12":
            return {
                ...state,
                com12: action.com12
            }
        case "comMount":
            return {
                ...state,
                comMount: action.comMount
            }
        default:
            return state
    }
}