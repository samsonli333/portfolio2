import { planSerInitialState, planService } from "../store";

export function planSerReducer(state:planService = planSerInitialState,action:any){
    switch(action.type){
        case "planService":
            return {
                ...state,
                planService:action.planService
            }
        default:
            return state
    }
}