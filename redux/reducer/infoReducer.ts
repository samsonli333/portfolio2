import { processInfo, processInfoInitialState } from "../store";

export default function infoReducer(state:processInfo = processInfoInitialState,action:any){
    switch(action.type){
        case "getProcess":
            return {
                ...state,
                body:action.body
            }
        default:
            return state
    }
}