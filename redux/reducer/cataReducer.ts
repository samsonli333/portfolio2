import { catainitialState, searchByCata } from "../store";


export function cataReducer(state: searchByCata = catainitialState, action: any): searchByCata {
    switch (action.type) {
        case "updateCata":
            return {
                ...state,
                getAdrResult: action.getAdrResult
            }
        default:
            return state
    }
    
}