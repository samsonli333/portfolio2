import { CombineRootState, miscellinitalState } from "../store";

export function miscellReducer(state: CombineRootState["miscell"] = miscellinitalState, action: any) {
    switch (action.type) {
        case "change2":
            return {
                ...state,
                change2: action.change2
            }
        case "change3":
            return {
                ...state,
                change3: action.change3
            }
        case "change4":
            return {
                ...state,
                change4: action.change4
            }
        case "setPlanInfo":
            return {
                ...state,
                setPlanInfo: action.setPlanInfo
            }
        case "planSid":
            return {
                ...state,
                planSid: action.planSid
            }
        case "finalSite":
            return {
                ...state,
                finalSite: action.finalSite
            }
        case "selectedPlanCode":
            return {
                ...state,
                selectedPlanCode: action.selectedPlanCode
            }
        case "processId":
            return {
                ...state,
                processId: action.processId
            }
        case "installDate":
            return {
                ...state,
                installDate: action.installDate
            }
        case "keyword":
            return {
                ...state,
                keyword: action.keyword
            }
        case "personalData":
            return {
                ...state,
                personalData: action.personalData
            }
        case "visaData":
            return {
                ...state,
                visaData: action.visaData
            }
        default:
            return state
    }
}