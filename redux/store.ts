import { legacy_createStore as createStore, Action, combineReducers, compose, applyMiddleware, Store } from 'redux'
import logger from 'redux-logger'
import { personalData, visaData } from '../type/type'
import cardReducer from './reducer/cardReducer'
import { cataReducer } from './reducer/cataReducer'
import { comReducer } from './reducer/comReducer'
import infoReducer from './reducer/infoReducer'
import { miscellReducer } from './reducer/miscellReducer'
import { planSerReducer } from './reducer/planSerReducer'
import { createWrapper, Context, HYDRATE } from "next-redux-wrapper";


declare module "redux"{
      export interface Store<S = any, A extends Action = AnyAction>{
        __persistor:any
      }
}




export type switchCom = {
    com1: boolean,
    com2: boolean,
    com3: boolean,
    com4: boolean,
    com5: boolean,
    com6: boolean,
    com7: boolean,
    com8: boolean,
    com9: boolean,
    com10: boolean,
    com11: boolean
    serAdr: string | ""
    com12: boolean | undefined
    comMount: boolean | undefined
}


export const cominitialState: switchCom = {
    com1: true,
    com2: false,
    com3: false,
    com4: false,
    com5: false,
    com6: false,
    com7: false,
    com8: false,
    com9: false,
    com10: false,
    com11: false,
    com12: false,
    serAdr: "",
    comMount: false
}

/////////////////////////////////////////////////////////////////////////////////


type cataResult = {
    sid: string
    processNext: string
    area: string
    district: string
    estate: string
    building: string
}

export type Cata = {
    code: number
    msg: string | ""
    resultList: cataResult[] | undefined
}

export type searchByCata = {
    getAdrResult: (Cata) | undefined
}

export const catainitialState: searchByCata = {
    getAdrResult: undefined
}

/////////////////////////////////////////////////////////////////////////////////


export type miscell = {
    change2?: boolean
    change3?: boolean
    change4?: boolean
    change5?: boolean
    setPlanInfo: boolean
    planSid?: string
    finalSite?: string
    selectedPlanCode: string | undefined
    processId: string
    installDate: string
    keyword: string
    personalData: personalData | undefined
    visaData: visaData | undefined
}

export const miscellinitalState: miscell = {
    change2: false,
    change3: false,
    change4: false,
    change5: false,
    setPlanInfo: false,
    planSid: "",
    finalSite: "",
    selectedPlanCode: "",
    processId: "",
    installDate: "",
    keyword: "",
    personalData: undefined,
    visaData: undefined,
}

////////////////////////////////////////////////////////////////////////////////////////////////////////

export type premiumList = {
    premiumCode: string
    premiumName: string
    is_free: boolean
    free_detail: string | null
    value: string | null
    premiumImage: string
    premiumImageType: string
    grouping: string
}


export type planServiceResultList = {
    planType: string
    is_new: boolean
    is_hot: boolean
    planCode: string
    planImage: string
    planImageType: string
    planCategory: string
    planName: string
    priceAdv: number
    monthlyFee: number
    contractPeriod: string
    introTxt: string | null
    introTxtRed: string | null
    planContent: string | null
    planRemark: string | null
    free: string | null
    freeDetail: string | null
    addonList: string | null
    premiumList: premiumList[]
    genreList: string | null
    converterRental: string | null
}

export type planServiceList = {
    code: number | ""
    msg: string
    resultList: planServiceResultList[]
}


export type planService = {
    planService?: (planServiceList)
}


export const planSerInitialState: planService = {
    planService: undefined
}


///////////////////////////////////////////////////////////////////////////////////////////////////////////////
export type cardInfoContent = {
    expiry_date: string
    user: string,
    card_no: string,
    card_holder: string,
    channel: string
}


export type cardInfo = {
    body: cardInfoContent
}


export const cardInitialState: cardInfo = {
    body: {
        expiry_date: "",
        user: "",
        card_no: "",
        card_holder: "",
        channel: ""
    }
}

//////////////////////////////////////////////////////////////////////////////////////

export type getProcess = {
    code: 0
    msg: string
    result: {
        gender: string
        surname: string
        givenname: string
        identityNo: string
        identityType: string
        dateOfBirthYrs: string
        dateOfBirthMth: string
        dateOfBirthDay: string
        appoDate: string
        appoTime: string
        appoTimeStr: string
        appoCal: string
        fullAddress: string
        instFloor: string
        instRoom: string
        commsLang: string
        contactMobile: string
        contactHome: string
        email: string
        commsAddress: string
        addrDiff: string
    }
}


export type processInfo = {
    body: getProcess["result"] | undefined
}

export const processInfoInitialState: processInfo = {
    body: undefined
}

////////////////////////////////////////////////////////////////////////////////////////

export interface CombineRootState {
    switchCom: switchCom,
    searchByCata: searchByCata,
    miscell: miscell,
    planService: planService,
    cardInfo: cardInfo,
    processInfo: processInfo
}


const composeEnhancers = compose

const RootReducer = combineReducers<CombineRootState>({
    switchCom: comReducer,
    searchByCata: cataReducer,
    miscell: miscellReducer,
    planService: planSerReducer,
    cardInfo: cardReducer,
    processInfo: infoReducer
})


const reducer = (state: CombineRootState, action: any) => {
    if (action.type === HYDRATE) {
        return {
            ...state,
            ...action
        }
    } else {
        return RootReducer(state, action)
    }
}




//const makeStore = (context:Context) => createStore<ReturnType<typeof reducer>, Action<any>, {}, {}>(reducer,composeEnhancers(applyMiddleware(logger)))
const makeConfiguredStore = (reducer:(state: CombineRootState, action: any) => any) => createStore<ReturnType<typeof reducer>, Action<any>, {}, {}>(reducer,composeEnhancers(applyMiddleware(logger)))

const makeStore = () => {
    const isServer = typeof window === 'undefined';
  
    if (isServer) {
      return makeConfiguredStore(RootReducer);
    } else {
      // we need it only on client side
      const {persistStore, persistReducer} = require('redux-persist');
      const storage = require('redux-persist/lib/storage').default;
  
      const persistConfig = {
        key: 'nextjs',
        storage,
      };

      const persistedReducer = persistReducer(persistConfig, RootReducer);
      let store = makeConfiguredStore(persistedReducer);
  
       store.__persistor = persistStore(store);

      return store 
    }
  };

export const wrapper = createWrapper<Store<CombineRootState>>(makeStore, { debug: true });



