
export default function metaData(Content: string) {


    return `<!DOCTYPE html>
      <html lang="en">
      <head>
          <meta charset="UTF-8">
          <meta http-equiv="X-UA-Compatible" content="IE=edge">
          <meta name="viewport" content="width=device-width, initial-scale=1.0">
          <title>Document</title>
          <link rel='preconnect' href='https://fonts.googleapis.com'> 
          <link rel='preconnect' href='https://fonts.gstatic.com' crossorigin> 
          <link href='https://fonts.googleapis.com/css2?family=Noto+Sans+HK:wght@100;400;500&family=Noto+Sans+TC:wght@400;500&display=swap' rel='stylesheet'>
          <link href="/catv/fontawesome/css/all.css" rel="stylesheet">
      </head>
      <body>
      ${Content}
      </body>
  </html>
  `
  }
  
  
  
  
  export const body = {
    style6: { width: "100%" },
    style7: { paddingLeft: "16px", borderLeft: "2px solid red" },
    style8: { fontSize: "20px", fontWeight: "normal" },
    style9: { paddingBottom: "10px" },
    style10: { width: "100%", border: "1px solid #e3e3e3", boxShadow: "5px 5px 10px 5px #e3e3e3", backgroundColor: "white", borderRadius: "15px 15px 15px 15px" },
    style11: { paddingTop: "16px" },
    style12: { paddingLeft: "16px" },
    style20: { paddingRight: "16px" },
    style23: { paddingBottom: "8px" },
    style24: { borderBottom: "1px solid #DEE2E6" },
    style27: { fontSize: "14px", fontWeight: "normal", color: "#404040" },
    style28: { fontSize: "16px", fontWeight: "normal" },
    style37: { paddingBottom: "16px" },
    style47: { fontSize: "16px", fontWeight: "normal" },
  }