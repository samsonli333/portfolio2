import { WithRouterProps } from "next/dist/client/with-router";
import  { getProcess } from "../redux/store";
import { getBanner, getClassId, IdAndClass, bannerProp, tvChannelList, getImage, getPlanPaymentInfo, getAppointment, Tnc } from "../type/type";

export function checkTel(tel: string) {
    let x: boolean | undefined = true;
    if (tel.length === 8) {
        const regEx = new RegExp('[2679][0-9]{7}')
        regEx.test(tel) && (x = false)
    }
    return x
}


export function checkMobile(tel: string) {
    let x: boolean | undefined = true;

    if (tel && tel.length === 8) {
        const regEx = new RegExp('[5679][0-9]{7}')
        regEx.test(tel) && (x = false)
    }
    return x
}


export function checkEmail(email: string) {
    let x: string | boolean = true;
    let w: number = 0
    if (email) {
        for (let word of email) {
            (word === "@") && w++
        }

        if (w < 2) {
            const regEx = new RegExp('[a-z0-9]+@[a-z]+[.][a-z]{2,3}');
            regEx.test(email) && (x = false)
        }
    }
    return x;
}

///////////////////////////////////////////// getServerSideProps for index ////////////////////////////////////////////////////////
export async function func1() {
    let IdAndClass: IdAndClass = { class: { class1: undefined, class2: undefined } }
    const getClassID = async (x: string, n: number) => {
        const Body = {
            eleID: x,
            page: "sub",
            lang: "C"
        }
        const res = await fetch(`${process.env.NEXT_PUBLIC_API18}`, { method: "POST", headers: { "Content-Type": "application/json" }, body: JSON.stringify(Body) })
        const result: getClassId = await res.json();
        if (result.code === 0) {
            IdAndClass = { ...IdAndClass, class: { ...IdAndClass.class, [`class${n}`]: result } }
        }
    }
    await getClassID("NSSbtn1", 1);
    await getClassID("NSSbtn2", 2);
    return IdAndClass
}


export async function func2() {
    let bannerProp: bannerProp = {
        banner: []
    }

    const Body = {
        page: "sub",
        lang: "C",
        isMobile: false
    }
    const res = await fetch(`${process.env.NEXT_PUBLIC_API5}`, {
        method: "POST",
        headers: { "Content-type": "application/json" }, body: JSON.stringify(Body)
    });
    const result: getBanner = await res.json();
    if (result.code === 0) {
        bannerProp = {
            banner: result.resultList,
        }
    }
    return bannerProp
}


export async function func3() {
    const Body = {
        page: "sub",
        section: "faq",
        isMobile: false,
        lang: "C"
    }
    const res = await fetch(`${process.env.NEXT_PUBLIC_API17}`, { method: "POST", headers: { "Content-type": "application/json" }, body: JSON.stringify(Body) })
    const result: getImage = await res.json();
    if (result.code === 0) return result
}


/////////////////////////////////////////////// getServerSideProps for page2 ////////////////////////////////////////////////////////

export async function getTvChannel(item: string | undefined) {
    const Body = {
        planCode: item,
        lang: "C"
    }
    const res = await fetch(`${process.env.NEXT_PUBLIC_API4}`, { method: "POST", headers: { "Content-type": "application/json" }, body: JSON.stringify(Body) });
    const result: tvChannelList = await res.json()
    console.log("result", result.resultList)
    if (result.code === 0) return result.resultList
}


////////////////////////////////////////////// getServerSideProps for page3 ///////////////////////////////////////////////////////////

export async function getInstallDate(planSid: string, selectedPlanCode: string) {
    const Body = {
        sid: planSid,
        planCode: selectedPlanCode,
        lang: "C"
    }

    const res = await fetch(`${process.env.NEXT_PUBLIC_API14}`, { method: "POST", headers: { "Content-type": "application/json" }, body: JSON.stringify(Body) })
    const result: getAppointment = await res.json()
    if (result.code === 0) return result.resultList
}



////////////////////////////////////////////// getServerSideProps  for page4 ///////////////////////////////////////////////////////////

export async function getClassId(x: string) {
    const Body = {
        eleID: x,
        page: "sub",
        lang: "C"
    }
    const res = await fetch(`${process.env.NEXT_PUBLIC_API18}`, { method: "POST", headers: { "Content-Type": "application/json" }, body: JSON.stringify(Body) })
    const result: getClassId = await res.json();
    if (result) return result
}


export async function getPayment(selectedPlanCode: string) {
    const Body = {
        planCode: selectedPlanCode,
        lang: "C"
    }
    const res = await fetch(`${process.env.NEXT_PUBLIC_API6}`, { method: "POST", headers: { "Content-type": "application/json" }, body: JSON.stringify(Body) })
    const result: getPlanPaymentInfo = await res.json();
    if (result.code === 0) return result.resultList

}


//////////////////////////////////////////// getServerSideProp  for  page6 /////////////////////////////////////////////////////////////



export async function getProcessSub(processId:string){
    const Body = {
        processID:processId,
        lang: "C"
    }
    const reply = await fetch(`${process.env.NEXT_PUBLIC_API12}`, { method: "POST", headers: { "Content-type": "application/json" }, body: JSON.stringify(Body) });
    const result: getProcess = await reply.json();
    if (result.code === 0) return result.result
}


export async function getClassIdPage6(x: string) {
    const Body = {
        eleID: x,
        page: "sub",
        lang: "C"
    }
    const res = await fetch(`${process.env.NEXT_PUBLIC_API18}`, { method: "POST", headers: { "Content-Type": "application/json" }, body: JSON.stringify(Body) })
    const result: getClassId = await res.json();
    if (result) return result
}


export async function getPaymentPage6(selectedPlanCode:string) {
    const Body = {
        planCode: selectedPlanCode,
        lang: "C"
    }
    const res = await fetch(`${process.env.NEXT_PUBLIC_API6}`, { method: "POST", headers: { "Content-type": "application/json" }, body: JSON.stringify(Body) })
    const result: getPlanPaymentInfo = await res.json();
    if (result.code === 0) return result
}


export async function getTNC(selectedPlanCode:string){
    const Body = {
        planCode:selectedPlanCode,
        lang: "C"
    }
    const res = await fetch(`${process.env.NEXT_PUBLIC_API15}`, { method: "POST", headers: { "Content-type": "application/json" }, body: JSON.stringify(Body) });
    const result: Tnc = await res.json();
    if(result.code === 0) return result.result
}


export async function tAndC(selectedPlanCode:string){
const Body = {
    planCode: selectedPlanCode,
    lang: "C"
}
const reply = await fetch(`${process.env.NEXT_PUBLIC_API15}`, { method: "POST", headers: { "Content-type": "application/json" }, body: JSON.stringify(Body) });
const result: Tnc = await reply.json();
if (result.code === 0) return result.result.tnc
}


