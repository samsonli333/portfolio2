import React, { FormEvent ,Dispatch} from 'react'
import { connect ,ConnectedProps} from 'react-redux';
import { textMarshal } from 'text-marshal'
import dayjs from 'dayjs';
//@ts-ignore
import { isValid, getCreditCardNameByNumber } from 'creditcard.js'
import { cardInfoContent, CombineRootState } from '../redux/store';
import { withRouter } from 'next/router'
import { page4ErrMsgContent, page4ErrMsgOn, PlanPayment, SubP2, Token, visaData } from '../type/type';
import Stepper from '../component/Stepper';
import NavBar from '../component/NavBar';
import FirstSection from '../component/FirstSection';
import FeeInfo from '../component/FeeInfo';
import PaymentInfo from '../component/PaymentInfo';
import Footer from '../component/Footer';
import { WithRouterProps } from 'next/dist/client/with-router';
import { getClassId, getPayment } from '../function/generalFunc';
import { InferGetServerSidePropsType } from 'next';




export const getServerSideProps = async (x: any) => {
    const { query } = x
    return {
        props: {
            class: await getClassId("NSSbtn12"),
            payment: await getPayment(query.selectedPlanCode) || null
        }
    }
}


type Props = CombineRootState["miscell"] & WithRouterProps & InferGetServerSidePropsType<typeof getServerSideProps> & ConnectedProps<typeof connector>

interface State {
    classVal: boolean
    planInfo: PlanPayment[] | null
    value: { value1: string | undefined, value2: string | undefined }
    onChange: { onChange1: (x: string) => void, onChange2: (x: string) => void }
    name: { name1: string, name2: string, name3: string }
    errMsgOn: page4ErrMsgOn
    errMsgContent: page4ErrMsgContent
    cardType: string
}

class Page4Index extends React.Component<Props, State>{
    public state: State
    constructor(props: Props) {
        super(props)
        this.state = {
            classVal: false,
            planInfo: this.props.payment || null,
            value: { value1: this.props.visaData?.cardNo, value2: this.props.visaData?.expiryDate },
            onChange: { onChange1: this.converFormat, onChange2: this.converFormat2 },
            name: { name1: "name1", name2: "name2", name3: "name3" },
            errMsgOn: { errMsgOn1: false, errMsgOn2: false, errMsgOn3: false },
            errMsgContent: { errMsgContent1: "", errMsgContent2: "", errMsgContent3: "" },
            cardType: ""
        }
    }

    backToPage3 = () => {
        this.props.router.back();
    }

    componentDidMount = () => {
        this.changeSize();
        setTimeout(() => console.log(this.state.planInfo), 100)
    }


    converFormat = (x: string) => {
        let data = textMarshal({
            input: x,
            template: "xxxx xxxx xxxx xxxx",
            disallowCharacters: [/[a-z,=/\\]/]
        })
        if (!isNaN(parseInt(x)) && (x.length !== 0)) {
            this.setState((state: State) => ({ ...state, value: { ...state.value, value1: data.marshaltext }, errMsgOn: { ...state.errMsgOn, errMsgOn1: false }, errMsgContent: { ...state.errMsgContent, errMsgContent1: "" } }))
        } else if (x.length === 0) {
            this.setState((state: State) => ({ ...state, value: { ...state.value, value1: data.marshaltext }, errMsgOn: { ...state.errMsgOn, errMsgOn1: false } }))
        } else {
            this.setState((state: State) => ({ ...state, errMsgOn: { ...state.errMsgOn, errMsgOn1: true }, errMsgContent: { ...state.errMsgContent, errMsgContent1: "咭號碼不正確" } }))
        }

        switch (getCreditCardNameByNumber(data.marshaltext)) {
            case "Visa":
                this.setState({ cardType: "visa-logo" })
                break;
            case "Mastercard":
                this.setState({ cardType: "master-logo" })
                break;
            default:
                this.setState({ cardType: "" })
        }
    }


    converFormat2 = (x: string) => {
        let data: any
        x[0] === "1" ? (
            x[0] === "1" && x[1] === "/" ?
                data = textMarshal({
                    input: x,
                    template: "0xxx",
                    disallowCharacters: [/[a-z]/],
                })
                :
                data = textMarshal({
                    input: x,
                    template: "xx/xx",
                    disallowCharacters: [/[a-z]/],
                })) :
            data = textMarshal({
                input: x,
                template: "0x/xx",
                disallowCharacters: [/[a-z]/],
            })
        this.setState((state: State) => ({ value: { ...state.value, value2: data.marshaltext } }))
    }


    Submit = async (e: FormEvent<HTMLFormElement>) => {
        e.preventDefault();
        const visaForm: any = document.querySelector('#visaForm');
        const isCardNumber = isValid(visaForm.name1.value)
        if (visaForm.name3.value && visaForm.name1.value && visaForm.name2.value) {
            const Body: cardInfoContent = {
                expiry_date: getDate(visaForm.name3.value),
                user: "sys",
                card_no: cardNumber(visaForm.name1.value),
                card_holder: visaForm.name2.value,
                channel: "catv"
            }
            //store.dispatch({ type: "body", body: Body })
            this.props.body({ type: "body", body: Body })
            const res = await fetch(`${process.env.NEXT_PUBLIC_API19}`, { method: "POST", headers: { "Content-type": "application/json" }, body: JSON.stringify(Body) })
            const result2: Token = await res.json();
            if (!isCardNumber) {
                this.setState((state: State) => ({ ...state, errMsgOn: { ...state.errMsgOn, errMsgOn1: true, errMsgon3: false }, errMsgContent: { ...state.errMsgContent, errMsgContent1: "咭號碼不正確", errMsgContent3: "" } }))
            } else if (result2.code === 0) {
                const tokenId = result2.result.token_id
                const coBranded = result2.result.co_branded
                const Body = {
                    processID: this.props.processId,
                    token_id: tokenId,
                    cardHolderName: visaForm.name2.value,
                    cardNumber: cardNumber(visaForm.name1.value),
                    co_branded: coBranded,
                    cardExpiry: getDate(visaForm.name3.value),
                    lang: "C"
                }
                const res = await fetch(`${process.env.NEXT_PUBLIC_API11}`, { method: "POST", headers: { "Content-type": "application/json" }, body: JSON.stringify(Body) })
                const result: SubP2 = await res.json();
                (result.code === 0 && result.result.status === "P2") && this.goToPage5();
            } else if (result2.code === 1003) {
                this.setState((state: State) => ({ ...state, errMsgOn: { ...state.errMsgOn, errMsgOn1: true, errMsgon3: false }, errMsgContent: { ...state.errMsgContent, errMsgContent1: "咭號碼不正確", errMsgContent3: "" } }))
            } else if (result2.code === 1007) {
                this.setState((state: State) => ({ ...state, errMsgOn: { ...state.errMsgOn, errMsgOn1: false, errMsgOn3: true }, errMsgContent: { ...state.errMsgContent, errMsgContent1: "", errMsgContent3: "到期日無效" } }))
            }
        } else {
            this.setState((state: State) => ({ ...state, errMsgOn: { ...state.errMsgOn, errMsgOn1: true, errMsgOn2: true, errMsgOn3: true } }))
        }
        this.saveData();
    }

    goToPage5 = () => {
        this.props.router.push(`/page5?processId=${this.props.processId}&selectedPlanCode=${this.props.selectedPlanCode}`)
    }

    public render(): React.ReactNode {
        console.log("processID ", this.props.processId)
        console.log("selectedPlanCode ", this.props.selectedPlanCode)
        return (
            <div>
               
                    <NavBar><Stepper className1={"step-active step-start step-end-active"} className2={"step-active2 step-active"} className3={"step-active2"} step={'1'} /></NavBar>
                    <FirstSection title="付款詳情" step={'第三步'} content={<></>} backToPage={this.backToPage3}>
                    </FirstSection>
                    <div className={`container${this.state.classVal ? "-fluid" : ""}`}>
                        <div className="row justify-content-center">
                            <div className="col-lg-7"><FeeInfo PlanInfo={this.state.planInfo} /></div>
                            <div className="col-lg-5"><form id="visaForm" onSubmit={this.Submit}><PaymentInfo class={this.props.class} errMsgContent={this.state.errMsgContent} cardType={this.state.cardType} visaData={this.props.visaData} name={this.state.name} value={this.state.value} onChange={this.state.onChange} errMsgOn={this.state.errMsgOn} /></form></div>
                        </div>
                    </div>
                    <Footer />
            
            </div>
        )
    }


    changeSize = () => {
        (window.innerWidth < 991) ? this.setState({ classVal: true }) : this.setState({ classVal: false })
        window.addEventListener('resize', () => (
            (window.innerWidth < 991) ? this.setState({ classVal: true }) : this.setState({ classVal: false })
        ))
    };

    saveData = () => {
        const visaForm: any = document.querySelector('#visaForm')
        const Visa: visaData = {
            cardNo: visaForm.name1.value,
            cardHolder: visaForm.name2.value,
            expiryDate: visaForm.name3.value
        }
        // store.dispatch({ type: "visaData", visaData: Visa })
        this.props.saveVisaData({ type: "visaData", visaData: Visa })
    }
}

function cardNumber(x: string) {
    const Arr = x.split("");
    let Arr2 = ""
    for (let i of Arr) {
        if (i !== " ") (Arr2 = Arr2 + i)
    }
    return Arr2
}

function getDate(x: string) {
    const Arr = x.split("/");
    const month = parseInt(Arr[0], 10) - 1
    const expiry1 = dayjs().year(parseInt("20" + Arr[1])).format("YYYY")
    const expiry2 = dayjs().month(month).format("MM")
    const expiry3 = dayjs(dayjs().month(month).format("YYYY-MM-DD")).daysInMonth()
    const expiry = expiry1 + "-" + expiry2 + "-" + expiry3
    return expiry
}

const MaptoState = (state: CombineRootState) => {
    return state.miscell
}

const MaptoDispatch = (dispatch:Dispatch<any>) => ({
    body:(x:{ type:string, body: cardInfoContent }) => dispatch({...x}),
    saveVisaData:(x:{ type: string, visaData: visaData }) => dispatch({...x})
})


const connector = connect(MaptoState,MaptoDispatch)
export default withRouter(connector(Page4Index));