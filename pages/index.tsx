//import { Inter } from '@next/font/google'
import React from 'react'
import { connect } from 'react-redux';
import { CombineRootState } from '../redux/store';
import Question from '../component/Question';
import NavBar from '../component/NavBar';
import MiddleSection from '../component/MiddleSection';
import Bottom from '../component/Bottom';
import Footer from '../component/Footer';
import SearchByCata from '../component/SearchByCata';
import TagManager from 'react-gtm-module';
import CarousellCus from '../component/CarousellCus';
import { InferGetServerSidePropsType } from 'next';
import { func1, func2, func3, } from '../function/generalFunc';



//const inter = Inter({ subsets: ['latin'] })



export const getServerSideProps = async () => {

  return {
    props: { ...await func1(), ...await func2(), ...{ question: await func3() } }
  }

}


type Props = CombineRootState["switchCom"] & InferGetServerSidePropsType<typeof getServerSideProps>

class Home extends React.Component<Props> {

  constructor(props: Props) {
    super(props)
  }


  componentDidMount = () => {
    TagManager.initialize({
      gtmId: 'GTM-KM3ZCSJ'
    })
    // setReduxMount();
  }

  public render(): React.ReactNode {
    return (
      <>
        {this.props.com6 && <Question image={this.props.question} />}
        {this.props.com1 &&
          <div>
            <NavBar />
            <section className="section1">
              <div className="hero-section mt-1">
                <CarousellCus {...this.props} />
              </div>
              <MiddleSection {...this.props} />
              <Bottom />
            </section>
            <Footer />
          </div>}
        {this.props.com2 && <SearchByCata />}
      </>
    )
  }
}

const MaptoState = (state: CombineRootState) => {
  return state.switchCom
}



export default connect(MaptoState)(Home);
