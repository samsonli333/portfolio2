import '../styles/custom.scss'
import type { AppProps } from 'next/app'
//import store from '../redux/store'
import { Provider } from 'react-redux'
import Layout from '../component/Layout'
import { wrapper } from '../redux/store'







const App = ({ Component, ...rest }: AppProps) => {
  const {store,props} = wrapper.useWrappedStore(rest)

    return (
      <>
        <Provider store={store}>
    
          <Layout>
            <Component {...props.pageProps} />
          </Layout>
        
        </Provider>
      </>
    )
  
}

export default App;

