import { Html, Head, Main, NextScript } from 'next/document'




export default function Document() {
  return (
    <Html lang="en">
      <Head><link href="/catv/fontawesome/css/all.css" rel="stylesheet" /></Head>
      <body>
        <Main />
        <NextScript />
      </body>
    </Html>
  )
}
