// Next.js NEXT_PUBLIC__API route support: https://nextjs.org/docs/NEXT_PUBLIC__API-routes/introduction
import { NextApiRequest, NextApiResponse } from "next"
import jsonfile from 'jsonfile'
import dayjs from "dayjs"
import { sendMail } from "../../../function/mailService"



const username = process.env.API_USERNAME
const password = process.env.API_PASSWORD
const header = {
    "Authorization": "Basic " + Buffer.from(`${username}:${password}`, 'binary').toString('base64'),
    "Content-Type": "application/json"
}



export default function handler(
    req: NextApiRequest,
    res: NextApiResponse
) {
    if (req.method === "GET") {
        switch (req.url) {
            case '/api/catv/index':
                res.send(`<h1 style="text-align:center">Hello World</div>`)
                return
        }
    } else if (req.method === "POST") {
        switch (req.url) {
            case '/api/catv/addressByKeyword':
                addressByKeyword(req, res);
                break;
            case '/api/catv/addressByAddress':
                addressByAddress(req, res);
                break;
            case '/api/catv/getPlanService':
                getPlanService(req, res);
                break;
            case '/api/catv/getPlanInfo':
                getPlanInfo(req, res);
                break;
            case '/api/catv/getTvChannel':
                getTvChannel(req, res);
                break;
            case '/api/catv/getBanner':
                getBanner(req, res);
                break;
            case '/api/catv/contactEnquiry':
                contactEnquiry(req, res);
                break;
            case '/api/catv/getImage':
                getImage(req, res);
                break;
            case '/api/catv/getAppointment':
                getAppointment(req, res);
                break;
            case '/api/catv/processNewSub':
                processNewSub(req, res);
                break;
            case '/api/catv/processSubP1':
                processSubP1(req, res);
                break;
            case '/api/catv/getPlanPaymentInfo':
                getPlanPaymentInfo(req, res);
                break;
            case '/api/catv/GenToken':
                GenToken(req, res);
                break;
            case '/api/catv/processSubP2':
                processSubP2(req, res);
                break;
            case '/api/catv/getProcessSub':
                getProcessSub(req, res);
                break;
            case '/api/catv/getTNC':
                getTNC(req, res);
                break;
            case '/api/catv/planSubmit':
                planSubmit(req, res);
                break;
            case '/api/catv/getClassID':
                getClassID(req, res);
                break;
            case '/api/catv/sendEmail':
                sendEmail(req, res);
                break;
        }

    } else {
        res.status(404).end("not found")
    }
}




async function addressByKeyword(req: NextApiRequest,
    res: NextApiResponse) {
    type Address = {
        keyword: string
    }
    const address: Address = { keyword: req.body.keyword }
    console.log("body.address", address)
    // try {
    //     const reply = await fetch(`${process.env.API1}`, {
    //         method: "POST", headers: header,
    //         body: JSON.stringify(address)
    //     })
    //     const result = await reply.json()
    //     console.log(result)
    //     res.status(200).json(result);
    // } catch (error) {
    //     console.log("\"AddressByKeyword\" ", error)
    // }
    const resultList = [
        {
            sid: "1",
            full_address: "lok fu esate lok man",
            weighting: 0
        },
        {
            sid: "2",
            full_address: "Wong Tai Sin esate Wong man House",
            weighting: 0
        },
    ]
    const newResultList = resultList.filter((item) => item.full_address.replace(/ /g,'').toUpperCase().includes(address.keyword.replace(/ /g,'').toUpperCase()))
    if (req.body.keyword) {
        res.status(200).json({
            code: 0,
            msg: "success",
            resultList: newResultList.length > 0 ? newResultList : [{
                sid: "1",
                full_address: "找不到相關資料",
                weighting: 0
            }]
        })
    } else if (req.body.keyword.length > 0 && req.body.keyword.length < 3) {
        res.status(200).json({
            code: 21101,
            msg: "success",
            resultList: []
        })
    } else if (!req.body.keyword.length) {
        res.status(200).json({
            code: 21102,
            msg: "success",
            resultList: []
        })
    }
}


async function addressByAddress(req: NextApiRequest, res: NextApiResponse) {
    const body = {
        level: req.body.level,
        keyword_1: req.body.keyword_1 || "",
        keyword_2: req.body.keyword_2 || "",
        keyword_3: req.body.keyword_3 || "",
        lang: "C"
    }
    console.log("address", body)
    // try {
    //     const reply = await fetch(`${process.env.API2}`, {
    //         method: "POST", headers: header,
    //         body: JSON.stringify(body)
    //     })
    //     const result = await reply.json()
    //     console.log(result)
    //     if (result) {
    //         res.status(200).json(result)
    //     } else {
    //         res.status(404).json({ status: "error" })
    //     }
    // } catch (err) {
    //     console.log("\"addressByAddress\" ", err)
    // }
    const resultList = [
        {
            sid: "1",
            processNext: "Y",
            area: "九龍",
            district: "lok fu",
            estate: "lok fu estate",
            building: "lok man house",
        },
        {
            sid: "2",
            processNext: "Y",
            area: "九龍",
            district: "Wong Tai Sin",
            estate: "Wong Tai Sin esate",
            building: "Wong man House",
        }
    ]
    let newResultList: Array<any> = []
    if (body.level === 2) {
        newResultList = resultList.filter((item) => item.area.replace(/ /g,'').toUpperCase() === body.keyword_1.replace(/ /g,'').toUpperCase())
    } else if (body.level === 3) {
        newResultList = resultList.filter((item) => item.district.replace(/ /g,'').toUpperCase() === body.keyword_2.replace(/ /g,'').toUpperCase())
    } else if (body.level === 4) {
        newResultList = resultList.filter((item) => {
            if (item.estate.replace(/ /g,'').toUpperCase() === body.keyword_3.replace(/ /g,'').toUpperCase()) {
                item.processNext = "N";
                return true
            }
        }
        )
    }
    res.status(200).json({
        code: 0,
        msg: "successs",
        resultList: newResultList
    })
}


async function getPlanService(req: NextApiRequest, res: NextApiResponse) {
    // const Body = {
    //     sid: req.body.sid,
    //     lang: req.body.lang
    // }
    // try {
    //     const reply = await fetch(`${process.env.API3}`, { method: "POST", headers: header, body: JSON.stringify(Body) })
    //     const result = await reply.json();
    //     if (result) {
    //         res.status(200).json(result)
    //     } else {
    //         res.status(404).json({ status: "error" })
    //     }
    // } catch (err) {
    //     console.log("\"getPlanService error\"", err)
    // }
    res.status(200).json({
        code: 0,
        msg: "success",
        resultList: [
            {
                planType: "tyuii",
                is_new: false,
                is_hot: false,
                planCode: "00001",
                planImage: "/catv/photo/banner0.png",
                planImageType: "png",
                planCategory: "寬頻",
                planName: "消費券限時優惠：200M寬頻",
                priceAdv: 10000,
                monthlyFee: 49,
                contractPeriod: 36,
                introTxt: "200M寬頻\n首月月費以信用卡支付。\n第2-36個月月費以AlipayHK/WeChat Pay HK消費券一次性支付。",
                introTxtRed: "首月月費($49)以信用卡支付。 第2-36個月月費(1,715)以消費券一次性支付。公私樓價劃一！",
                planContent: "有線200M寬頻\n服務速度: 共享下載200Mbps / 上載10Mbps\n技術：DOCSIS 3.0\n(Data Over Cable Service Interface Specification)\n- 為美洲寬頻最高的標準\n- 擁高速網際網絡存取能力\n- 光纖同軸網絡",
                planRemark: "備註：(上網服務) 實際速度會因使用者的設備、傳送技術、個別網絡及軟件之使用、網絡設定、覆蓋範圍、使用量、及其他外在因素而有所影響。",
                free: "free",
                freeDetail: "All free",
                addonList: "No",
                premiumList: [
                    {
                        premiumCode: "00001",
                        premiumName: "0001",
                        is_free: true,
                        free_detail: "all free",
                        value: "100",
                        premiumImage: "/catv/photo/banner0.png",
                        premiumImageType: "png",
                        grouping: "yes"
                    }
                ],
                genreList: "good good",
                converterRental: "good good",
            },
            {
                planType: "tyuii",
                is_new: false,
                is_hot: false,
                planCode: "00002",
                planImage: "/catv/photo/banner1.png",
                planImageType: "png",
                planCategory: "寬頻",
                planName: "電影迷必選：200M寬頻+hmvod",
                priceAdv: 10000,
                monthlyFee: 78,
                contractPeriod: 36,
                introTxt: "200M寬頻\n首月月費以信用卡支付。\n第2-36個月月費以AlipayHK/WeChat Pay HK消費券一次性支付。",
                introTxtRed: "",
                planContent: "有線200M寬頻\n服務速度: 共享下載200Mbps / 上載10Mbps\n技術：DOCSIS 3.0\n(Data Over Cable Service Interface Specification)\n- 為美洲寬頻最高的標準\n- 擁高速網際網絡存取能力\n- 光纖同軸網絡",
                planRemark: "備註：(上網服務) 實際速度會因使用者的設備、傳送技術、個別網絡及軟件之使用、網絡設定、覆蓋範圍、使用量、及其他外在因素而有所影響。",
                free: "free",
                freeDetail: "All free",
                addonList: "No",
                premiumList: [
                    {
                        premiumCode: "00001",
                        premiumName: "0001",
                        is_free: true,
                        free_detail: "all free",
                        value: "100",
                        premiumImage: "/catv/photo/banner1.png",
                        premiumImageType: "png",
                        grouping: "yes"
                    }
                ],
                genreList: "good good",
                converterRental: "good good",
            },
            {
                planType: "tyuii",
                is_new: false,
                is_hot: false,
                planCode: "00003",
                planImage: "/catv/photo/banner2.png",
                planImageType: "png",
                planCategory: "寬頻",
                planName: "200M家居寬頻",
                priceAdv: 10000,
                monthlyFee: 88,
                contractPeriod: 36,
                introTxt: "200M寬頻\n首月月費以信用卡支付。\n第2-36個月月費以AlipayHK/WeChat Pay HK消費券一次性支付。",
                introTxtRed: "成功申請可享CABLE Shop電子現金券$500",
                planContent: "有線200M寬頻\n服務速度: 共享下載200Mbps / 上載10Mbps\n技術：DOCSIS 3.0\n(Data Over Cable Service Interface Specification)\n- 為美洲寬頻最高的標準\n- 擁高速網際網絡存取能力\n- 光纖同軸網絡",
                planRemark: "備註：(上網服務) 實際速度會因使用者的設備、傳送技術、個別網絡及軟件之使用、網絡設定、覆蓋範圍、使用量、及其他外在因素而有所影響。",
                free: "free",
                freeDetail: "All free",
                addonList: "No",
                premiumList: [
                    {
                        premiumCode: "00001",
                        premiumName: "0001",
                        is_free: true,
                        free_detail: "all free",
                        value: "100",
                        premiumImage: "/catv/photo/banner2.png",
                        premiumImageType: "png",
                        grouping: "yes"
                    }
                ],
                genreList: "good good",
                converterRental: "good good",
            },
            {
                planType: "tyuii",
                is_new: false,
                is_hot: false,
                planCode: "00004",
                planImage: "/catv/photo/banner3.png",
                planImageType: "png",
                planCategory: "寬頻",
                planName: "200M家居寬頻 + TP-Link AX20路由器",
                priceAdv: 10000,
                monthlyFee: 78,
                contractPeriod: 36,
                introTxt: "200M寬頻\n首月月費以信用卡支付。\n第2-36個月月費以AlipayHK/WeChat Pay HK消費券一次性支付。",
                introTxtRed: "",
                planContent: "有線200M寬頻\n服務速度: 共享下載200Mbps / 上載10Mbps\n技術：DOCSIS 3.0\n(Data Over Cable Service Interface Specification)\n- 為美洲寬頻最高的標準\n- 擁高速網際網絡存取能力\n- 光纖同軸網絡",
                planRemark: "備註：(上網服務) 實際速度會因使用者的設備、傳送技術、個別網絡及軟件之使用、網絡設定、覆蓋範圍、使用量、及其他外在因素而有所影響。",
                free: "free",
                freeDetail: "All free",
                addonList: "No",
                premiumList: [
                    {
                        premiumCode: "00001",
                        premiumName: "0001",
                        is_free: true,
                        free_detail: "all free",
                        value: "100",
                        premiumImage: "/catv/photo/banner3.png",
                        premiumImageType: "png",
                        grouping: "yes"
                    }
                ],
                genreList: "good good",
                converterRental: "good good",
            },
            {
                planType: "tyuii",
                is_new: false,
                is_hot: false,
                planCode: "00005",
                planImage: "/catv/photo/banner4.png",
                planImageType: "png",
                planCategory: "寬頻",
                planName: "200M家居寬頻 + + TP-Link M4 Mesh",
                priceAdv: 10000,
                monthlyFee: 78,
                contractPeriod: 36,
                introTxt: "200M寬頻\n首月月費以信用卡支付。\n第2-36個月月費以AlipayHK/WeChat Pay HK消費券一次性支付。",
                introTxtRed: "",
                planContent: "有線200M寬頻\n服務速度: 共享下載200Mbps / 上載10Mbps\n技術：DOCSIS 3.0\n(Data Over Cable Service Interface Specification)\n- 為美洲寬頻最高的標準\n- 擁高速網際網絡存取能力\n- 光纖同軸網絡",
                planRemark: "備註：(上網服務) 實際速度會因使用者的設備、傳送技術、個別網絡及軟件之使用、網絡設定、覆蓋範圍、使用量、及其他外在因素而有所影響。",
                free: "free",
                freeDetail: "All free",
                addonList: "No",
                premiumList: [
                    {
                        premiumCode: "00001",
                        premiumName: "0001",
                        is_free: true,
                        free_detail: "all free",
                        value: "100",
                        premiumImage: "/catv/photo/banner4.png",
                        premiumImageType: "png",
                        grouping: "yes"
                    }
                ],
                genreList: "good good",
                converterRental: "good good",
            },
            {
                planType: "tyuii",
                is_new: false,
                is_hot: false,
                planCode: "00006",
                planImage: "/catv/photo/banner5.png",
                planImageType: "png",
                planCategory: "寬頻",
                planName: "200M家居寬頻",
                priceAdv: 10000,
                monthlyFee: 148,
                contractPeriod: 12,
                introTxt: "200M寬頻\n首月月費以信用卡支付。\n第2-36個月月費以AlipayHK/WeChat Pay HK消費券一次性支付。",
                introTxtRed: "",
                planContent: "有線200M寬頻\n服務速度: 共享下載200Mbps / 上載10Mbps\n技術：DOCSIS 3.0\n(Data Over Cable Service Interface Specification)\n- 為美洲寬頻最高的標準\n- 擁高速網際網絡存取能力\n- 光纖同軸網絡",
                planRemark: "備註：(上網服務) 實際速度會因使用者的設備、傳送技術、個別網絡及軟件之使用、網絡設定、覆蓋範圍、使用量、及其他外在因素而有所影響。",
                free: "free",
                freeDetail: "All free",
                addonList: "No",
                premiumList: [
                    {
                        premiumCode: "00001",
                        premiumName: "0001",
                        is_free: true,
                        free_detail: "all free",
                        value: "100",
                        premiumImage: "/catv/photo/banner5.png",
                        premiumImageType: "png",
                        grouping: "yes"
                    }
                ],
                genreList: "good good",
                converterRental: "good good",
            }
        ]
    })
}


async function getPlanInfo(req: NextApiRequest, res: NextApiResponse) {
    const Body = {
        planCode: req.body.planCode,
        lang: req.body.lang
    }
    try {
        const reply = await fetch(`${process.env.API8}`, { method: "POST", headers: header, body: JSON.stringify(Body) })
        const result = await reply.json();
        if (result) {
            res.status(200).json(result)
        } else {
            res.status(404).json({ status: "error" })
        }
    } catch (err) {
        console.log("\"getPlanInfo\" ", err)
    }
}


async function getTvChannel(req: NextApiRequest, res: NextApiResponse) {
    // const Body = {
    //     planCode: req.body.planCode,
    //     lang: req.body.lang
    // }
    // try {
    //     const reply = await fetch(`${process.env.API4}`, { method: "POST", headers: header, body: JSON.stringify(Body) });
    //     const result = await reply.json()
    //     if (result) {
    //         res.status(200).json(result)
    //     } else {
    //         res.status(404).json({ status: "error" })
    //     }
    // } catch (err) {
    //     console.log("\"getTvChannel\"", err)
    // }

    res.status(200).json({
        code: 0,
        msg: "success",
        resultList: [
            {
                genrecode: "098",
                genreName: "boradband",
                channelCode: "90",
                channelName: "product",
                channelDesc: "ertyu",
                channelImage: "/",
                channelImageType: "png"
            }
        ]
    })
}

async function getBanner(req: NextApiRequest, res: NextApiResponse) {
    // const Body = {
    //     page: req.body.page,
    //     lang: req.body.lang,
    //     isMobile: req.body.isMobile
    // }
    // try {
    //     const reply = await fetch(`${process.env.API15}`, { method: "POST", headers: header, body: JSON.stringify(Body) });
    //     const result = await reply.json()
    //     if (result) {
    //         res.status(200).json(result)
    //     } else {
    //         res.status(404).json({ status: "error" })
    //     }
    // } catch (err) {
    //     console.log("\"getBanner\"", err)
    // }
    res.status(200).json({
        code: 0,
        msg: "success",
        resultList: [{
            bannerImage: "/catv/photo/1.png",
            bannerImageType: "png",
            bannerName: "1"
        },
        {
            bannerImage: "/catv/photo/2.png",
            bannerImageType: "png",
            bannerName: "2"
        },
        {
            bannerImage: "/catv/photo/3.png",
            bannerImageType: "png",
            bannerName: "3"
        }
        ]
    })
}


async function getImage(req: NextApiRequest, res: NextApiResponse) {
    // const Body = {
    //     page: req.body.page,
    //     section: req.body.section,
    //     isMobile: false,
    //     lang: "C"
    // }
    // try {
    //     const reply = await fetch(`${process.env.API17}`, { method: "POST", headers: header, body: JSON.stringify(Body) })
    //     const result = await reply.json();
    //     if (result) {
    //         res.status(200).json(result)
    //     } else {
    //         res.status(404).json({ status: "error" })
    //     }
    // } catch (err) {
    //     console.log("\"getImage\"", err)
    // }
    res.status(200).json({
        code: 0,
        msg: "success",
        result: "/catv/photo/question.jpeg"
    })
}


async function getAppointment(req: NextApiRequest, res: NextApiResponse) {
    // const Body = {
    //     sid: req.body.sid,
    //     planCode: req.body.planCode,
    //     lang: req.body.lang
    // }
    // try {
    //     const reply = await fetch(`${process.env.API14}`, { method: "POST", headers: header, body: JSON.stringify(Body) })
    //     const result = await reply.json();
    //     if (result) {
    //         res.status(200).json(result)
    //     } else {
    //         res.status(404).json({ status: "error" })
    //     }
    // } catch (err) {
    //     console.log("\"getAppointment\"", err)
    // }
    res.status(200).json({
        code: 0,
        msg: "success",
        resultList: [
            {
                calId: "1",
                dateStr: "2023-10-08",
                timeStr: "08:00",
                timeId: "1"
            },
            {
                calId: "2",
                dateStr: "2023-10-08",
                timeStr: "09:00",
                timeId: "2"
            },
            {
                calId: "3",
                dateStr: "2023-10-09",
                timeStr: "10:00",
                timeId: "3"
            },
        ]
    })
}


async function processNewSub(req: NextApiRequest, res: NextApiResponse) {
    // const Body = {
    //     processID: req.body.processID,
    //     planCode: req.body.planCode,
    //     sid: req.body.sid,
    //     lang: req.body.sid
    // }
    // try {
    //     const reply = await fetch(`${process.env.API9}`, { method: "POST", headers: header, body: JSON.stringify(Body) })
    //     const result = await reply.json();
    //     if (result.code === 0) {
    //         res.status(200).json(result)
    //     } else {
    //         res.status(404).end("")
    //     }
    // } catch (err) {
    //     console.log("\"processNewSub\"", err)
    // }
    res.status(200).json({
        code: 0,
        msg: "success",
        result: { processID: "P1", status: "P1" }
    })
}


async function processSubP1(req: NextApiRequest, res: NextApiResponse) {
    // const Body = { ...req.body }
    // try {
    //     const reply = await fetch(`${process.env.API10}`, { method: "POST", headers: header, body: JSON.stringify(Body) })
    //     const result = await reply.json();
    //     if (result) {
    //         res.status(200).json(result)
    //     } else {
    //         res.status(404).json({ status: "error" })
    //     }
    // } catch (err) {
    //     console.log("\"processSubP1\"", err)
    // }
    let code: number | string = 0
    const statusErr: { statusName: string, statusCode: number | string }[] = [
        { statusName: "processID", statusCode: "" },
        { statusName: "gender", statusCode: 23202 },
        { statusName: "surname", statusCode: 23203 },
        { statusName: "givenname", statusCode: 23204 },
        { statusName: "identityNo", statusCode: 23205 },
        { statusName: "identityType", statusCode: "" },
        { statusName: "dateOfBirthYrs", statusCode: 23207 },
        { statusName: "dateOfBirthMth", statusCode: 23207 },
        { statusName: "dateOfBirthDay", statusCode: 23207 },
        { statusName: "appoDate", statusCode: 23208 },
        { statusName: "appoTime", statusCode: 23209 },
        { statusName: "appoCal", statusCode: "" },
        { statusName: "fullAddress", statusCode: "" },
        { statusName: "instFloor", statusCode: 23212 },
        { statusName: "instRoom", statusCode: 23213 },
        { statusName: "commsLang", statusCode: 23219 },
        { statusName: "contactMobile", statusCode: 23210 },
        { statusName: "contactHome", statusCode: "" },
        { statusName: "email", statusCode: 23211 },
        { statusName: "commsAddress", statusCode: 23214 },
        { statusName: "lang", statusCode: 23219 },
        { statusName: "addrDiff", statusCode: "" }
    ]
    let body = { ...req.body }

    for (let key in body) {
        if ((key !== "contactHome") && (body[key] === "")) {
            statusErr.forEach((item) => {
                if (item.statusName === key)
                    code = item.statusCode
            })
            break;
        }
    }

    if (code === 0) {
        if (body.appoTime === "1") {
            body = { ...body, appoTimeStr: "08:00" }
        } else if (body.appoTime === "2") {
            body = { ...body, appoTimeStr: "09:00" }
        } else if (body.appoTime == "3") {
            body = { ...body, appoTimeStr: "10:00" }
        }
        await jsonfile.writeFile(`${__dirname}/body.json`, body, { spaces: 2 });
    }
    res.status(200).json({
        code: code,
        msg: "success",
        result: { processID: "P1", status: "P1" }
    })

}


async function getPlanPaymentInfo(req: NextApiRequest, res: NextApiResponse) {
    // const Body = {
    //     planCode: req.body.planCode,
    //     lang: req.body.lang
    // }
    // try {
    //     const reply = await fetch(`${process.env.API6}`, { method: "POST", headers: header, body: JSON.stringify(Body) })
    //     const result = await reply.json();
    //     if (result) {
    //         res.status(200).json(result);
    //     } else {
    //         res.status(404).json({ status: "error" })
    //     }
    // } catch (err) {
    //     console.log("\"getPlanPaymentinfo\"", err)
    // }
    res.status(200).json({
        code: 0,
        msg: "success",
        resultList: [
            {
                groupNum: 0,
                groupName: "boardband",
                itemNum: 1,
                itemName: "fee",
                priceStr: "1000"
            },
            {
                groupNum: 0,
                groupName: "boardband",
                itemNum: 2,
                itemName: "fee",
                priceStr: "1000"
            },
            {
                groupNum: 0,
                groupName: "boardband",
                itemNum: 99,
                itemName: "fee",
                priceStr: "1000"
            },
            {
                groupNum: 1,
                groupName: "boardband2",
                itemNum: 1,
                itemName: "fee3",
                priceStr: "1000"
            },
            {
                groupNum: 1,
                groupName: "boardband2",
                itemNum: 99,
                itemName: "fee3",
                priceStr: "2000"
            },
        ]
    })
}

async function GenToken(req: NextApiRequest, res: NextApiResponse) {
    const Body = { ...req.body }
    // try {
    //     const reply = await fetch(`${process.env.API19}`, { method: "POST", headers: header, body: JSON.stringify(Body) })
    //     const result = await reply.json();
    //     if (result) {
    //         res.status(200).json(result)
    //     } else {
    //         res.status(404).json({ status: "error" })
    //     }
    // } catch (err) {
    //     console.log("\"GenToken\"", err)
    // }
    let code: number = 0
    const nowDate = dayjs().format('YYYY/MM/DD')
    const Diff = dayjs(nowDate).diff(Body.expiry_date, 'month')
    Diff > 0 ? [code = 1007] : ""
    Body.card_holder === "" ? [code = 1003] : ""
    res.status(200).json({
        code: code,
        msg: "success",
        result: { token_id: "xxxxx", co_branded: "catv" }
    })
}


async function processSubP2(req: NextApiRequest, res: NextApiResponse) {
    //const Body = { ...req.body }
    // try {
    //     const reply = await fetch(`${process.env.API11}`, { method: "POST", headers: header, body: JSON.stringify(Body) })
    //     const result = await reply.json();
    //     if (result) {
    //         res.status(200).json(result)
    //     } else {
    //         res.status(404).json({ status: "error" })
    //     }
    // } catch (err) {
    //     console.log("\"processSubP2\"", err)
    // }
    res.status(200).json({
        code: 0,
        msg: "success",
        result: { status: "P2" }
    })
}

async function getProcessSub(req: NextApiRequest, res: NextApiResponse) {
    // const Body = {
    //     processID: req.body.processID,
    //     lang: req.body.lang
    // }
    // try {
    //     const reply = await fetch(`${process.env.API12}`, { method: "POST", headers: header, body: JSON.stringify(Body) });
    //     const result = await reply.json();
    //     if (result) {
    //         res.status(200).json(result)
    //     } else {
    //         res.status(404).json({ status: "error" })
    //     }
    // } catch (err) {
    //     console.log("\"getProcessSub\"", err)
    // }
    let body = await jsonfile.readFile(`${__dirname}/body.json`)
    const { lang, processID, ...rest } = body
    body = { ...rest }
    console.log("body", body)
    res.status(200).json({
        code: 0,
        msg: "success",
        result: body
        // {
        //     gender: "M",
        //     surname: "Li",
        //     givenname: "Lap Shing",
        //     identityNo: "12345678",
        //     identityType: "ID",
        //     dateOfBirthYrs: "3",
        //     dateOfBirthMth: "3",
        //     dateOfBirthDay: "3",
        //     appoDate: "2023-10-08",
        //     appoTime: "09:00",
        //     appoTimeStr: "09:00",
        //     appoCal: "",
        //     fullAddress: "lok fu estate",
        //     instFloor: "5",
        //     instRoom: "500",
        //     commsLang: "E",
        //     contactMobile: "62715408",
        //     contactHome: "",
        //     email: "samson@samson.com",
        //     commsAddress: "lok fu",
        //     addrDiff: "Y"
        // }
    })
}


async function getTNC(req: NextApiRequest, res: NextApiResponse) {
    // const Body = {
    //     planCode: req.body.planCode,
    //     lang: req.body.lang
    // }
    // try {
    //     const reply = await fetch(`${process.env.API5}`, { method: "POST", headers: header, body: JSON.stringify(Body) });
    //     const result = await reply.json();
    //     if (result) {
    //         res.status(200).json(result);
    //     } else {
    //         res.status(404).json({ status: "error" })
    //     }
    // } catch (err) {
    //     console.log("\"hetTNC\"", err)
    // }
    res.status(200).json({
        code: 0,
        msg: "success",
        result: {
            tnc: "1",
            tncDetails: [
                {
                    tncID: "01",
                    content: "Terms 1 2 3 4 5",
                    is_Compulsory: true,
                    is_Checked: true
                }
            ]
        }

    })
}


async function planSubmit(req: NextApiRequest, res: NextApiResponse) {
    // console.log(req.body)
    // const Body = {
    //     processID: req.body.processID,
    //     tnc: req.body.tnc,
    //     lang: req.body.lang
    // }
    // try {
    //     const reply = await fetch(`${process.env.API13}`, { method: "POST", headers: header, body: JSON.stringify(Body) });
    //     const result = await reply.json();
    //     if (result) {
    //         res.status(200).json(result)
    //     } else {
    //         res.status(404).json({ status: "error" })
    //     }
    // } catch (err) {
    //     console.log("\"planSubmit\"", err)
    // }
    res.status(200).json({
        code: 0,
        msg: "success",
        result: { status: "C" }
    })
}


async function getClassID(req: NextApiRequest, res: NextApiResponse) {
    console.log(req.body.Body)
    // const Body = {
    //     eleID: req.body.eleID,
    //     page: req.body.page,
    //     lang: req.body.lang
    // }
    // try {
    //     const reply = await fetch(`${process.env.API18}`, { method: "POST", headers: header, body: JSON.stringify(Body) })
    //     const result = await reply.json()
    //     if (result) {
    //         res.status(200).json(result)
    //     } else {
    //         res.status(404).json({ status: "error" })
    //     }
    // } catch (err) {
    //     console.log("\"getClassID\"", err)
    // }
    res.status(200).json({
        code: 0,
        msg: "success",
        result: {
            cssClass: "001",
            htmlID: "001"
        }
    })
}


async function sendEmail(req: NextApiRequest, res: NextApiResponse) {
    console.log(req.body)
    // const username = process.env.MAIL_USERNAME
    // const password = process.env.MAIL_PASSWORD
    // const header = {
    //     "Authorization": "Basic " + Buffer.from(`${username}:${password}`, 'binary').toString('base64'),
    //     "Content-Type": "application/json"
    // }
    // const Body = {
    //     acctNum: process.env.ACCTNUM,
    //     recipientEmail: req.body.recipientEmail,
    //     senderEmail: process.env.SENDEREMAIL,
    //     emailSubject: process.env.EMAILSUBJECT,
    //     emailContent: req.body.mail
    // }
    // try {
    //     const reply = await fetch(`${process.env.API20}`, { method: "POST", headers: header, body: JSON.stringify(Body) })
    //     const result = await reply.json();
    //     if (result) {
    //         res.status(200).json(result.result)
    //     } else {
    //         res.status(404).json({ status: "error" })
    //     }
    // } catch (err) {
    //     console.log("\"sendEmail\"", err)
    // }
    await sendMail("Confirmation Notification",
    req.body.recipientEmail,
    req.body.mail)
   res.status(200).json({
      code:0,
      msg:"success",
      result:"completed"
   })
}


async function contactEnquiry(req: NextApiRequest, res: NextApiResponse) {
    // const Body = { ...req.body }
    // try {
    //     const reply = await fetch(`${process.env.API7}`, { method: "POST", headers: header, body: JSON.stringify(Body) })
    //     const result = await reply.json();
    //     if (result) {
    //         res.status(200).json(result)
    //     } else {
    //         res.status(404).json({ status: "error" })
    //     }
    // } catch (err) {
    //     console.log("\"contactEnquiry\"", err)
    // }
    res.status(200).json({
        code: 0,
        msg: "success",
        result: "success"
    })
}


