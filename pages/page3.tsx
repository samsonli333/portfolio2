import React, { ChangeEvent, FormEvent, ReactNode ,Dispatch} from 'react'
import { connect ,ConnectedProps} from 'react-redux';
//@ts-ignore
import validid from 'validid/esm/index.mjs'
import ContactForm from '../component/ContactForm';
import FirstSection from '../component/FirstSection';
import Footer from '../component/Footer';
import InstallationForm from '../component/InstallationForm';
import NavBar from '../component/NavBar';
import Stepper from '../component/Stepper';
import SubInfoForm from '../component/SubInfoForm';
import { CombineRootState } from '../redux/store';
import { Appointment, errMsgOn, onChange, personalData, processNewSub, processSubP1 } from '../type/type';
import { withRouter } from 'next/router'
import { WithRouterProps } from 'next/dist/client/with-router';
import { getClassId, getInstallDate } from '../function/generalFunc';
import { InferGetServerSidePropsType } from 'next';





export const getServerSideProps = async (params: any) => {
    const { query } = params
    return {
        props: {
            installDate: await getInstallDate(query.planSid, query.selectedPlanCode) || null,
            class: await getClassId("NSSbtn11") || null
        }
    }
}




type entireProp = CombineRootState["switchCom"] & CombineRootState["miscell"] 
type excludedProp = {
    switchCom: CombineRootState["switchCom"]
    miscell: {
        planSid: CombineRootState["miscell"]["planSid"], selectedPlanCode: CombineRootState["miscell"]["selectedPlanCode"],
        fianlSite: CombineRootState["miscell"]["finalSite"], personalData: CombineRootState["miscell"]["personalData"],
    } 
} 

interface State {
    value: { value1: string | undefined, value2: string | undefined, value3: string | undefined, value4: string | undefined }
    selectedValue: string | undefined
    installDate: Appointment[]
    time: { title2: string[], disable: boolean }
    errMsgOn: errMsgOn
    errIdContent: string,
    onChange: onChange,
    PbookOn: boolean,
    swAddr: boolean | undefined
}

type Props = Exclude<excludedProp, entireProp> & WithRouterProps & InferGetServerSidePropsType<typeof getServerSideProps> & ConnectedProps<typeof connector>

class Page3Index extends React.Component<Props, State> {
    public InstallationRef: React.RefObject<HTMLInputElement>
    public ContactFormRef: React.RefObject<HTMLInputElement>
    public SubInfoFormRef: React.RefObject<HTMLInputElement>
    constructor(props: Props) {
        super(props)
        this.InstallationRef = React.createRef();
        this.SubInfoFormRef = React.createRef();
        this.ContactFormRef = React.createRef();
        this.state = {
            value: { value1: (this.props.miscell.personalData?.AppoDate), value2: this.props.miscell.personalData?.isntFloor, value3: this.props.miscell.personalData?.isntRoom, value4: this.props.miscell.personalData?.diffAddrTxt || "" },
            selectedValue: this.props.miscell.personalData?.AppoTime,
            installDate: this.props.installDate || [],
            time: { title2: ["請選擇安裝時間"], disable: true },
            errMsgOn: {
                errMsgOn1: false, errMsgOn2: false, errMsgOn3: false, errMsgOn4: false,
                errMsgOn5: false, errMsgOn6: false, errMsgOn7: false, errMsgOn8: false,
                errMsgOn9: false, errMsgOn10: false, errMsgOn11: false, errMsgOn12: false,
                errMsgOn13: false, errMsgOn14: false, errMsgOn19: false
            },
            errIdContent: "",
            onChange: { onChange1: this.setValue1, onChange2: this.setValue2, onChange3: this.setValue3, onChange4: this.setValue4, onChange5: this.onChangeTerm },
            PbookOn: false || (this.props.miscell.personalData?.passBookOn === "true"),
            swAddr: false || (this.props.miscell.personalData?.AddrDiff === "true")
        }
    }

    onChange = () => {
        this.setState((state: State) => ({ ...state, PbookOn: !state.PbookOn }));
    }


    onChangeTerm = () => this.setState((state: State) => ({ ...state, swAddr: !state.swAddr }))

    componentDidMount = async () => {
        console.log("selectedplancode ", this.props.miscell.selectedPlanCode);
        console.log("planSid ", this.props.miscell.planSid)
        console.log("personalData ", this.props.miscell.personalData)
        this.findTimeSlot2();
    }


    setValue1 = (x: any) => this.setState((state: State) => ({ ...state, value: { ...state.value, value1: x } }))
    setValue2 = (x: any) => this.setState((state: State) => ({ ...state, value: { ...state.value, value2: x } }))
    setValue3 = (x: any) => this.setState((state: State) => ({ ...state, value: { ...state.value, value3: x } }))
    setValue4 = (x: any) => this.setState((state: State) => ({ ...state, value: { ...state.value, value4: x } }))


    setSelectedValue = (x: ChangeEvent<HTMLSelectElement>) => this.setState({ selectedValue: x.target.value })



    componentDidUpdate = (prevProp: Exclude<excludedProp, entireProp>, prevState: State) => {
        if (prevState.value.value1 !== this.state.value.value1) {
            this.findTimeSlot2();
        }
    }


    goToPage4 = () => {
        this.saveData();
        this.props.router.push(`/page4?selectedPlanCode=${this.props.miscell.selectedPlanCode}`)
    }

    backToPage2 = () => {
        this.props.router.back();
    }

    findTimeSlot = () => {
        const Arr = new Set<string>()
        this.state.installDate.filter((item) => {
            return item.dateStr === this.state.value.value1
        }).forEach((item) => {
            Arr.add(item.timeStr)
        })
        return Array.from(Arr)
    }


    findTimeSlot2 = () => {
        const Timeslot = this.findTimeSlot();
        let currentDisable: boolean = true;
        (Timeslot.length > 0) && (currentDisable = false); (this.setState((state: State) => ({
            ...state,
            time: {
                ...state.time,
                title2: [...["請選擇安裝時間"], ...Timeslot],
                disable: currentDisable
            }
        })))
    }

    Submit = async (e: FormEvent<HTMLElement>) => {
        e.preventDefault()
        const Body = {
            processID: null,
            planCode: this.props.miscell.selectedPlanCode,
            sid: this.props.miscell.planSid,
            lang: "C"
        }
        let res, result: processNewSub
        try {
            res = await fetch(`${process.env.NEXT_PUBLIC_API9}`, { method: "POST", headers: { "Content-type": "application/json" }, body: JSON.stringify(Body) })
            if (!res.ok) {
                throw new Error(res.status.toString())
            } else {
                result = await res.json()
                if (this.getValue2(getValue, result)) {
                    const res2 = await fetch(`${process.env.NEXT_PUBLIC_API10}`, { method: "POST", headers: { "Content-type": "application/json" }, body: JSON.stringify(this.getValue2(getValue, result)) })
                    const result2: processSubP1 = await res2.json();
                    console.log("processSubP1",result2);
                    const signupForm: any = document.querySelector('#signup')
                    const checkInValid = () => {
                        this.setState((state: State) => ({ ...state, errMsgOn: { ...state.errMsgOn, errMsgOn5: false } }))
                        switch (result2.code) {
                            case 23202:
                                this.setState((state: State) => ({ ...state, errMsgOn: { ...state.errMsgOn, errMsgOn2: true } }))
                                this.SubInfoFormRef.current?.scrollIntoView({ behavior: "smooth", block: "center", inline: "center" })
                                break;
                            case 23203:
                                this.setState((state: State) => ({ ...state, errMsgOn: { ...state.errMsgOn, errMsgOn3: true } }))
                                this.SubInfoFormRef.current?.scrollIntoView({ behavior: "smooth", block: "center", inline: "center" })
                                break;
                            case 23204:
                                this.setState((state: State) => ({ ...state, errMsgOn: { ...state.errMsgOn, errMsgOn4: true } }))
                                this.SubInfoFormRef.current?.scrollIntoView({ behavior: "smooth", block: "center", inline: "center" })
                                break;
                            case 23205:
                                this.setState((state: State) => ({ ...state, errMsgOn: { ...state.errMsgOn, errMsgOn5: true } }))
                                this.SubInfoFormRef.current?.scrollIntoView({ behavior: "smooth", block: "center", inline: "center" })
                                break;
                            case 23206:
                                this.setState((state: State) => ({ ...state, errMsgOn: { ...state.errMsgOn, errMsgOn6: true } }))
                                this.SubInfoFormRef.current?.scrollIntoView({ behavior: "smooth", block: "center", inline: "center" })
                                break;
                            case 23207:
                                this.setState((state: State) => ({ ...state, errMsgOn: { ...state.errMsgOn, errMsgOn7: true } }))
                                this.SubInfoFormRef.current?.scrollIntoView({ behavior: "smooth", block: "center", inline: "center" })
                                break;
                            case 23208:
                            case 23218:
                                this.setState((state: State) => ({ ...state, errMsgOn: { ...state.errMsgOn, errMsgOn8: true } }))
                                this.InstallationRef.current?.scrollIntoView({ behavior: "smooth", block: "center", inline: "center" })
                                break;
                            case 23209:
                                this.setState((state: State) => ({ ...state, errMsgOn: { ...state.errMsgOn, errMsgOn9: true, errMsgOn8: true } }))
                                this.ContactFormRef.current?.scrollIntoView({ behavior: "smooth", block: "center", inline: "center" })
                                break;
                            case 23210:
                                this.setState((state: State) => ({ ...state, errMsgOn: { ...state.errMsgOn, errMsgOn10: true } }))
                                this.ContactFormRef.current?.scrollIntoView({ behavior: "smooth", block: "center", inline: "center" })
                                break;
                            case 23211:
                                this.setState((state: State) => ({ ...state, errMsgOn: { ...state.errMsgOn, errMsgOn11: true } }))
                                this.ContactFormRef.current?.scrollIntoView({ behavior: "smooth", block: "center", inline: "center" })
                                break;
                            case 23212:
                                this.setState((state: State) => ({ ...state, errMsgOn: { ...state.errMsgOn, errMsgOn12: true } }))
                                this.InstallationRef.current?.scrollIntoView({ behavior: "smooth", block: "center", inline: "center" })
                                break;
                            case 23213:
                                this.setState((state: State) => ({ ...state, errMsgOn: { ...state.errMsgOn, errMsgOn13: true } }))
                                this.InstallationRef.current?.scrollIntoView({ behavior: "smooth", block: "center", inline: "center" })
                                break;
                            case 23214:
                            case 23215:
                                this.setState((state: State) => ({ ...state, errMsgOn: { ...state.errMsgOn, errMsgOn14: true } }))
                                this.ContactFormRef.current?.scrollIntoView({ behavior: "smooth", block: "center", inline: "center" })
                                break;
                            case 23219:
                                this.setState((state: State) => ({ ...state, errMsgOn: { ...state.errMsgOn, errMsgOn19: true } }))
                                this.ContactFormRef.current?.scrollIntoView({ behavior: "smooth", block: "center", inline: "center" })
                                break;
                        }
                    }
                    const showIdInvalid = () => {
                        this.setState((state: State) => ({ ...state, errMsgOn: { ...state.errMsgOn, errMsgOn5: true } }))
                        this.SubInfoFormRef.current?.scrollIntoView({ behavior: "smooth", block: "center", inline: "center" })
                    }
                    const customCheckIsValid = () => {
                        if (this.state.swAddr) {
                            if (this.state.value.value4) {
                                validid.hkid(signupForm.hkid.value + signupForm.bracket.value) ? this.goToPage4() : showIdInvalid()
                            } else {
                                this.setState((state: State) => ({ ...state, errMsgOn: { ...state.errMsgOn, errMsgOn14: true } }));
                                this.ContactFormRef.current?.scrollIntoView({ behavior: "smooth", block: "center", inline: "center" })
                            }
                        } else {
                            validid.hkid(signupForm.hkid.value + signupForm.bracket.value) ? this.goToPage4() : showIdInvalid()
                        }
                    }
                    ((result2.code === 0) && (result2.result.status === "P1")) ?
                        this.state.PbookOn ?
                            this.goToPage4()
                            :
                            customCheckIsValid()
                        :
                        checkInValid();
                }
            }
        } catch (e) {
            e && 
            // store.dispatch({ type: "com12", com12: true })
            this.props.swCom({ type: "com12", com12: true })
        }
    }

    public render(): ReactNode {
        return (
            <>
                    <NavBar><Stepper className1={"step-active step-start"} className2={"step-active2"} className3={null} step={'2'} /></NavBar>
                    <FirstSection title="請輸入基本資料" content={<div className="text-center text-dark mb-4">請以英文填寫以下資料</div>} step={'第二步'} backToPage={this.backToPage2}>
                        <div className="px-3">
                            <form id="signup" onSubmit={this.Submit}>
                                <SubInfoForm ref={this.SubInfoFormRef} PbookOn={this.state.PbookOn} PbookonChange={this.onChange} errMsgOn={this.state.errMsgOn} personalData={this.props.miscell.personalData} errIdContent={this.state.errIdContent} nameGroup={{
                                    name1: 'sex',
                                    name2: 'surname',
                                    name3: 'firstname',
                                    name4: 'hkid',
                                    name5: 'bracket',
                                    name6: 'passbookon',
                                    name7: 'passbook',
                                    name8: 'brith'
                                }} tranFunc={tranFunc2} />
                                <div className="mt-5"></div>
                                <InstallationForm ref={this.InstallationRef} errMsgOn={this.state.errMsgOn} value={this.state.value} personalData={this.props.miscell.personalData} selectedValue={this.state.selectedValue} onChange={this.state.onChange} time={this.state.time.title2} Appointment={this.state.installDate} disable={this.state.time.disable} selectChange={this.setSelectedValue} nameGroup={{
                                    name1: 'floor',
                                    name2: 'room'
                                }} />
                                <div className="mt-5"></div>
                                <ContactForm ref={this.ContactFormRef} swAddr={this.state.swAddr} class={this.props.class} onChangeTerm={this.state.onChange.onChange5} onChange={this.state.onChange.onChange4} value={this.state.value} personalData={this.props.miscell.personalData} errMsgOn={this.state.errMsgOn} nameGroup={{
                                    name1: 'lang',
                                    name2: 'countrycode',
                                    name3: 'mobile',
                                    name4: 'countrycode',
                                    name5: 'tel',
                                    name6: 'email',
                                    name7: 'addr',
                                    name8: 'diffaddr'
                                }} tranFunc={tranFunc} />
                            </form>
                        </div>
                        <div className="py-5"></div>
                    </FirstSection>
                    <Footer />
            </>
        )
    }

    getValue2 = (getValue: (result: processNewSub) => string | undefined, result: processNewSub) => {
        const signupForm: any = document.querySelector('#signup')
        let timeId, calId, newProcessId: string | undefined, Body, step, step2;
        (this.state.value.value1 && this.state.selectedValue) ?
            step = [timeId = findId(this.state.installDate, this.state.value.value1, this.state.selectedValue)?.timeId,
            calId = findId(this.state.installDate, this.state.value.value1, this.state.selectedValue)?.calId,
            newProcessId = undefined,
            (newProcessId = getValue(result)) && (
                Body = {
                    processID: newProcessId || "",
                    gender: signupForm.sex.value || "",
                    surname: signupForm.surname.value || "",
                    givenname: signupForm.firstname.value || "",
                    identityNo: signupForm.passbookon.value === "true" ? (signupForm.passbook.value) : (signupForm.hkid.value + (signupForm.bracket.value && "(" + signupForm.bracket.value + ")")),
                    identityType: tranFunc3(signupForm.passbookon.value) || "",
                    dateOfBirthYrs: signupForm.brith2.value || "",
                    dateOfBirthMth: addZero(signupForm.brith1.value) || "",
                    dateOfBirthDay: addZero(signupForm.brith0.value) || "",
                    appoDate: this.state.value.value1 || "",
                    appoTime: timeId || "",
                    appoCal: calId || "",
                    fullAddress: this.props.switchCom.serAdr || signupForm.addr.value || "",
                    instFloor: signupForm.floor.value || "",
                    instRoom: signupForm.room.value || "",
                    commsLang: signupForm.lang.value || "",
                    contactMobile: signupForm.mobile.value || "",
                    contactHome: signupForm.tel.value || "",
                    email: signupForm.email.value || "",
                    commsAddress: signupForm.diffaddr.value === "true" ? signupForm.addr.value : this.props.switchCom.serAdr,
                    lang: "C",
                    addrDiff: tranFunc4(signupForm.diffaddr.value) || ""
                }),
            // store.dispatch({ type: "processId", processId: newProcessId }),
            this.props.processId({ type: "processId", processId: newProcessId }),
            console.log(Body)] : step2 = [this.setState((state: State) => ({ ...state, errMsgOn: { ...state.errMsgOn, errMsgOn8: true, errMsgOn9: true } })),
            this.InstallationRef.current?.scrollIntoView({ behavior: "smooth", block: "center", inline: "center" })]
        return Body
    }


    saveData = () => {
        const signupForm: any = document.querySelector('#signup')
        const PersonalData: personalData = {
            Gender: signupForm.sex.value,
            Surname: signupForm.surname.value,
            Givenname: signupForm.firstname.value,
            IdentityNo: signupForm.hkid.value,
            IdentityNo2: signupForm.bracket.value,
            passBookOn: signupForm.passbookon.value,
            passBookNo: signupForm.passbookon.value === "true" ? signupForm.passbook.value : "",
            dateOfBirthYrs: signupForm.brith2.value,
            dateOfBirthMth: signupForm.brith1.value,
            dateOfBirthDay: signupForm.brith0.value,
            AppoDate: this.state.value.value1,
            AppoTime: this.state.selectedValue,
            AppoCal: "",
            fullAddress: signupForm.addr.value,
            isntFloor: signupForm.floor.value,
            isntRoom: signupForm.room.value,
            CommsLang: signupForm.lang.value,
            ContactMobile: signupForm.mobile.value,
            ContactHome: signupForm.tel.value,
            email: signupForm.email.value,
            CommAddress: this.props.switchCom.serAdr,
            Lang: "C",
            AddrDiff: signupForm.diffaddr.value,
            diffAddrTxt: signupForm.addr.value
        }
        // store.dispatch({ type: "personalData", personalData: PersonalData })
        this.props.personalData({ type: "personalData", personalData: PersonalData })
    }

}



function getValue(result: processNewSub) {
    let newProcess = ""
    if (result.code === 0) {
        newProcess = result.result.processID
        return newProcess
    }
}

function tranFunc(item: string) {
    switch (item) {
        case "中文":
            return "C"
        case "英文":
            return "E"
        default:
            return ""
    }
}


function tranFunc2(item: string) {
    switch (item) {
        case "男":
            return "M"
        case "女":
            return "F"
        default:
            return ""
    }
}

function tranFunc3(item: string) {
    switch (item) {
        case "true":
            return "P"
        case "false":
            return "H"
        default:
            return ""
    }
}

function tranFunc4(item: string) {
    switch (item) {
        case "true":
            return "Y"
        case "false":
            return "N"
        default:
            return ""
    }
}


function findId(installDate: Appointment[], date: string, time: string) {
    const x = installDate.filter((item) => {
        return (item.dateStr === date) && (item.timeStr === time)
    })
    if (x) {
        return x[0]
    } else {
        return null
    }
}

function addZero(x: string) {
    if (isNaN(parseInt(x))) {
        return ""
    } else {
        return ("0" + x).substring(("0" + x).length - 2)
    }
}


const MaptoState = (state: Exclude<excludedProp, entireProp>) => {
    return state
}

const MaptoDispatch = (dispatch:Dispatch<any>) => ({
      swCom: (x:{ type:string,com12:boolean}) => dispatch({...x}),
      processId:(x:{ type:string, processId:string | undefined}) => dispatch({...x}),
      personalData:(x:{type:string, personalData: personalData})=> dispatch({...x}) 
})



const connector = connect(MaptoState,MaptoDispatch)
export default withRouter(connector(Page3Index));