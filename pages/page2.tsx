import { InferGetServerSidePropsType } from 'next';
import React , {Dispatch} from 'react'
import { connect ,ConnectedProps} from 'react-redux';
import Card from '../component/Card';
import FirstSection from '../component/FirstSection';
import Footer from '../component/Footer';
import Inquiry from '../component/Inquiry';
import NavBar from '../component/NavBar';
import SecondSection from '../component/SecondSection';
import Stepper from '../component/Stepper';
import { getTvChannel } from '../function/generalFunc';
import { CombineRootState } from '../redux/store';




export const getServerSideProps = async (x: any) => {
    const { query } = x
    return {
        props: {
            smImgList: await getTvChannel(query.planSid) || null
        }
    }
}

type Props = CombineRootState["switchCom"] & CombineRootState["planService"] & CombineRootState["miscell"] & InferGetServerSidePropsType<typeof getServerSideProps> & ConnectedProps<typeof connector>

class Page2Index extends React.Component<Props> {
    constructor(props: Props) {
        super(props)
    }


    componentDidMount = () => {
        this.props.selectedPlanCode({ type: "selectedPlanCode", selectedPlanCode: "" });
    }


    public render(): React.ReactNode {
        return (
            <>
                {this.props.com5 && <Inquiry />}
                <NavBar>{this.props.planService ? <Stepper className1={null} className2={null} className3={null} step={'3'} /> : <></>}</NavBar>
                <section className="section1 page2index">
                    <div className="px-3">
                        {this.props.planService ? (this.props.planService.code === 22101) ?
                            <FirstSection title={"你已選擇的地址為："} content={<div className="text-center text2">
                                <span className="text-dark fw-bold text2">{this.props.serAdr}</span>
                            </div>} step={''} backToPage={null}>
                            </FirstSection>
                            :
                            <FirstSection title={""} content={<div className="text-center text2">
                                <span className="text1">你已選擇的地址為：</span><br />
                                <span className="text-dark fw-bold text2">{this.props.serAdr}</span>
                            </div>} step={'第一步'} backToPage={null}>
                            </FirstSection>
                            :
                            <FirstSection title={"你已選擇的地址為："} content={<span className="text-dark fw-bold text2">{this.props.keyword}</span>} step={''} backToPage={null}>
                            </FirstSection>}
                    </div>
                    <div className="p-3">
                        <div className="my-4"></div>
                        {this.props.planService && this.props.planService?.resultList?.map((item, i) => {
                            console.log("page2index", i)
                            return <Card key={i} planSerResult={item} planSid={this.props.planSid} caption={(i < 1) ? true : false} outline={(i < 1) ? true : false} planCode={item.planCode} smImgList={this.props.smImgList} />
                        })}
                        <div className="my-4"></div>
                        <SecondSection />
                    </div>
                </section>
                <Footer />
            </>
        )
    }
}

const MaptoState = (state: CombineRootState) => {
    return {
        ...state.switchCom,
        ...state.planService,
        ...state.miscell
    }
}

const MaptoDispatch = (dispatch:Dispatch<any>) => ({
    selectedPlanCode:(x:{type: string, selectedPlanCode:string }) => dispatch({...x})
})


const connector = connect(MaptoState,MaptoDispatch)
export default connector(Page2Index);