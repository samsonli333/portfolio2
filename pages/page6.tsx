import React, { Dispatch } from 'react'
import { connect ,ConnectedProps } from 'react-redux';
import { Congrat } from '../public/photo/photo';
import{ CombineRootState, getProcess } from '../redux/store';
import { withRouter } from 'next/router'
import Image from 'next/image'
import NavBar from '../component/NavBar';
import Footer from '../component/Footer';
import { WithRouterProps } from 'next/dist/client/with-router';
import ViewPDF from '../component/ViewPDF';
import CusToast from '../component/CusToast';





interface State {
    info: getProcess | ""
}

type Props = CombineRootState["miscell"] & CombineRootState["processInfo"] & CombineRootState["switchCom"] & WithRouterProps & ConnectedProps<typeof connector>

class Page6 extends React.Component<Props>{
    public state: State
    constructor(props: Props) {
        super(props)
        this.state = {
            info: ""
        }
    }

    componentDidmount = () => {
        // store.dispatch({ type: "finalSite", finalSite: "" })
        this.props.clearfinalSite({ type: "finalSite", finalSite: "" })
    }


    goToPage1 = () => {
        this.props.router.push('/')
    }

    goToViewPDF = () => {
        // store.dispatch({ type: "com11", com11: true })
        this.props.swCom({ type: "com11", com11: true})
    }


    public render(): React.ReactNode {
        return (
            <div>
                <CusToast/>
                {!this.props.com11 ?
                    <>
                            <NavBar />
                            <section className="text-center page6-outer mx-auto mt-5 px-3">
                                <div className="text-center"><span className="dt-red fs-3 fw-bold">恭喜!</span><br />您的登記已完成</div>
                                <Image className="congrat-img" src={Congrat} alt="" />
                                <div className="fs-5 fw-bold mb-4"><span className="text-secondary fs-7 fw-light">您的登記編號是</span><br />{this.props.processId}</div>
                                <div className="fs-5 fw-bold mb-4"><span className="text-secondary text-center fs-7 fw-light">安裝地址</span><br />{this.props.body?.addrDiff === "Y" ? this.props.body.commsAddress : this.props.finalSite}</div>
                                <div className="fs-5 fw-bold mb-4"><span className="text-secondary fs-7 fw-light">已預約的安裝日期及時間</span><br />{formatDate(this.props.installDate.substring(0, 11)) + this.props.installDate.substring(10)}</div>
                                <div>我們稍後會有專人與您聯絡以核實登記。<br />如有需要，請儲存登記紀錄。</div>
                                <div onClick={this.goToViewPDF} className="text-white text-center db-red rounded-pill p-2 px-5 mt-4 cursor"><span className="page6_reg_desktop">儲存登記紀錄</span><span className="page6_reg_mobile">儲存登記紀錄/發送電郵通知</span></div>
                                <div onClick={this.goToPage1} className="text-center border b-red dt-red rounded-pill p-2 mt-4 cursor">返回主頁</div>
                            </section>
                            <Footer />
                    </>
                    :
                    <ViewPDF />}
            </div>
        )
    }
}

const MaptoState = (state: CombineRootState) => {
    return {
        ...state.miscell,
        ...state.processInfo,
        ...state.switchCom
    }
}


function formatDate(x: string | undefined) {
    const Arr = x?.split("-")
    if (Arr) {
        return Arr.join('/')
    }
}

const MaptoDispatch = (dispatch:Dispatch<any>) => ({
    clearfinalSite:(x:{ type:string, finalSite:string }) => dispatch({...x}),
    swCom:(x:{ type:string, com11:boolean }) => dispatch({...x})
})

const connector = connect(MaptoState,MaptoDispatch)
export default withRouter(connector(Page6));