import dayjs from 'dayjs'
import React, { Dispatch, FormEvent, useState } from 'react'
import { Collapse } from 'react-bootstrap'
import { connect ,ConnectedProps} from 'react-redux'
import { textMarshal } from 'text-marshal'
//@ts-ignore
import { getCreditCardNameByNumber } from 'creditcard.js'
import { CombineRootState, getProcess, planServiceResultList, processInfo } from '../redux/store'
import Stepper from '../component/Stepper'
import FirstSection from '../component/FirstSection'
import NavBar from '../component/NavBar'
import Term from '../component/Term'
import Term2 from '../component/Term2'
import Footer from '../component/Footer'
import PageCard from '../component/PageCard'
import { withRouter } from 'next/router'
import { WithRouterProps } from 'next/dist/client/with-router'
import TAndC from '../component/TAndC'
import { CardInfoPage5, getClassId, getPlanPaymentInfo, info, planSubmit, Tnc, TncContent } from '../type/type'
import { getClassIdPage6, getPaymentPage6, getProcessSub, getTNC, tAndC } from '../function/generalFunc'
import { InferGetServerSidePropsType } from 'next'




export const getServerSideProps = async (x: any) => {
    const { query } = x
    return {
        props: {
            getProcessSub: await getProcessSub(query.processId) || null,
            getPaymentPage6: await getPaymentPage6(query.selectedPlanCode) || null,
            getTNC: await getTNC(query.selectedPlanCode) || null,
            class: await getClassIdPage6("NSSbtn13"),
            tAndC: await tAndC(query.selectedPlanCode) || "",
        }
    }
}

interface State {
    info: (getProcess["result"]) | undefined
    planInfo: getPlanPaymentInfo
    result: {
        tnc: string
        tncDetails: TncContent[]
    }
    termIndex: boolean[]
    term: { tncID: number }[]
    term2: boolean
    errMsgOn: boolean
    loader: boolean
    tncErrMsgOn: boolean[]
    class: getClassId | undefined
}

type Props = CombineRootState["miscell"] & CombineRootState["planService"] & CombineRootState["cardInfo"] &
    CombineRootState["switchCom"] & WithRouterProps & InferGetServerSidePropsType<typeof getServerSideProps> & ConnectedProps<typeof connector>

class Page5Index extends React.Component<Props, State>{
    public term: { tncID: number }[]
    constructor(props: Props) {
        super(props)
        this.term = []
        this.state = {
            info: this.props.getProcessSub || undefined,
            planInfo: this.props.getPaymentPage6 || {
                code: 0, msg: "", resultList: [{
                    groupNum: 0,
                    groupName: "",
                    itemNum: 0,
                    itemName: "",
                    priceStr: ""
                }]
            },
            result: this.props.getTNC || {
                tnc: "",
                tncDetails: []
            },
            termIndex: [],
            term: [],
            term2: false,
            errMsgOn: false,
            loader: false,
            tncErrMsgOn: [],
            class: this.props.class || undefined
        }
    }


    backToPage3 = () => {
        this.props.router.back();
    }

    goToPage6 = () => {
        this.props.router.push('/page6')
    }


    componentDidMount = async () => {
        //store.dispatch({ type: "getProcess", body: this.state.info })
        this.props.getProcess({ type: "getProcess", body: this.state.info })
        await this.getPlanService();
        this.setState({ termIndex: Array<boolean>(this.state.result.tncDetails.length).fill(false) })
        const Arr = [["installDate", this.state.info?.appoDate + " " + this.state.info?.appoTimeStr], ["finalSite", this.state.info?.fullAddress + " " + this.state.info?.instFloor + " " + this.state.info?.instRoom]]
        for (let x of Arr) {
            // store.dispatch({ type: x[0], [`${x[0]}`]: x[1] })
            this.props.Arr({ type: x[0], [`${x[0]}`]: x[1] })
        }
    }


    getPlanService = () => {
        const filterPlan: any = this.props.planService?.resultList.filter((item, _) => {
            if (item.planCode === this.props.selectedPlanCode) {
                return item
            }
        })
        return filterPlan?.[0]
    }


    onChangeTnc = async (position: number, x: number) => {
        const newTermIndex = this.state.termIndex.map((item, i) => {
            return i === position ? !item : item
        })
        newTermIndex[position] ? this.term.push({ tncID: x }) : this.term.filter((item, i) => item.tncID === x && this.term.splice(i, 1));
        this.setState((state: State) => ({ ...state, term: this.term, termIndex: newTermIndex }))
    }

    confirmTnc = (x: any) => this.setState((state: State) => ({ term2: !state.term2 }))

    Submit = async (e: FormEvent<HTMLFormElement>) => {
        e.preventDefault();
        const tempArr: boolean[] = []
        const secondStep = async () => {
            this.setState({ loader: true })
            const Body = {
                processID: this.props.processId,
                tnc: this.term,
                lang: "C"
            }
            console.log(Body)
            if (this.state.term2) {
                const res = await fetch(`${process.env.NEXT_PUBLIC_API13}`, { method: "POST", headers: { "Content-type": "application/json" }, body: JSON.stringify(Body) });
                const result: planSubmit = await res.json();
                console.log(result);
                (result.code === 0 && result.result.status === "C") && this.setState({ loader: false }); this.goToPage6();
            } else { this.setState({ loader: false }) }
        }
        const firstStep = () => {
            let temp: boolean = true
            !this.state.term2 ? this.setState({ errMsgOn: true }) : this.setState({ errMsgOn: false })
            if (this.state.result.tncDetails.length === this.state.termIndex.length) {
                this.setState({ tncErrMsgOn: Array<boolean>(this.state.result.tncDetails.length).fill(false) })
                const Length = this.state.result.tncDetails.length;
                for (let i = 0; i < Length; i++) {
                    (this.state.result.tncDetails[i].is_Compulsory && !this.state.termIndex[i]) ? (tempArr[i] = true) : (tempArr[i] = false);
                }
                tempArr.forEach((item) => item && (temp = false))
            }
            return temp
        }
        firstStep() ? secondStep() : this.setState({ tncErrMsgOn: tempArr });
        // store.dispatch({ type: "personalData", personalData: "" })
        this.props.personalData({ type: "personalData", personalData: "" })
        // store.dispatch({ type: "visaData", visaData: "" })
        this.props.visaData({ type: "visaData", visaData: "" })
    }


    public render(): React.ReactNode {
        return (
            <div>
                    <NavBar><Stepper className1={"step-active step-start step-end-active"} className2={"step-active2 step-active step2-start"} className3={"step-active"} step={'1'} content={"最後一步"} /></NavBar>
                    <FirstSection title="確認資料" step={'第四步'} content={<></>} backToPage={this.backToPage3}>
                        <div className="page5-width mx-auto px-3">
                            <PageRowOrdered info={this.getPlanService()} />
                            <div className="mt-5"></div>
                            <PageRowBasic info={this.state.info} />
                            <div className='mt-5'></div>
                            <PageRowInstall info={this.state.info} />
                            <div className="mt-5"></div>
                            <PageRowContact info={this.state.info} />
                            <div className="mt-5"></div>
                            <PageRowMonth code={0} msg={''} resultList={this.state.planInfo.resultList} />
                            <div className="mt-5"></div>
                            <PageRowPayment body={this.props.body} />
                            <form id="term" onSubmit={this.Submit}>
                                <section className="terms mt-5">
                                    {this.state.result.tncDetails.map((item, i) => {
                                        return <>
                                            <Term key={i} termContent={item.content} onChange={() => this.onChangeTnc(i, item.tncID)} name={`term${i + 1}`} errMsgOn={(!this.state.termIndex[i] && this.state.tncErrMsgOn[i]) ? true : false} errMsgContent={'請勾選該欄位'} />
                                            <div className="mt-2"></div>
                                        </>
                                    })}
                                    <Term2 termContent={""} errMsgOn={this.state.errMsgOn} name={""} value={this.state.term2} onChange={this.confirmTnc} errMsgContent={'未能成功提交申請，請稍後再次嘗試。'} />
                                </section>
                                <button id={this.state.class?.result.htmlID} className={`${this.state.class?.result.cssClass} db-red w-100 border-0 rounded-pill text-center text-white p-2 my-5 mt-4 cursor`}>確定申請</button>
                            </form>
                        </div>
                    </FirstSection>
                    <Footer />
                    {this.state.loader && <Spinner />}
                    {this.props.com10 && <TAndC info={this.props.tAndC} />}
            </div>
        )
    }
}

const MaptoState = (state: CombineRootState) => {
    return {
        ...state.miscell,
        ...state.planService,
        ...state.cardInfo,
        ...state.switchCom,
    }
}

const MaptoDispatch = (dispatch:Dispatch<any>) => ({
    getProcess:(x:{ type:string, body:getProcess["result"] | undefined}) => dispatch({...x}),
    personalData:(x:{ type:string, personalData:string }) => dispatch({...x}),
    visaData:(x:{ type: string, visaData:string }) => dispatch({...x}),
    Arr:(x:any) => dispatch({...x})
})

const PageRowOrdered = (prop: { info: planServiceResultList }) => {
    return (
        <>
            <div className="fs-5 border-0 b-red border-start border-4 ps-2 mb-2 fw-bold">已選擇的服務</div>
            <PageCard outline={false} caption={false} planSerResult={prop.info} />
        </>
    )
}

const PageRowBasic = (prop: info) => {
    return (
        <>
            <div className="fs-5 border-0 b-red border-start border-4 ps-2 mb-2 fw-bold gx-5">基本資料</div>
            <div className="row justify-content-start ps-2 border-0 border-start border-4 border-white gy-3">
                <div className="col-auto me-4"><span className="text-secondary text-opacity-50">性別</span><br />{prop.info?.gender == 'M' ? "男" : "女"}</div>
                <div className="col-auto me-4"><span className="text-secondary text-opacity-50">姓氏</span><br />{prop.info?.surname}</div>
                <div className="col-auto me-4"><span className="text-secondary text-opacity-50">名字</span><br />{prop.info?.givenname}</div>
                <div className="col-auto me-4"><span className="text-secondary text-opacity-50">出生日期 (YYYY/MM/DD)</span><br />{`${prop.info?.dateOfBirthYrs}/${prop.info?.dateOfBirthMth}/${prop.info?.dateOfBirthDay}`}</div>
                <div className="col-auto me-4"><span className="text-secondary text-opacity-50">{prop.info?.identityType === "H" ? "身份證號碼" : "護照號碼"}</span><br />{prop.info?.identityNo}</div>
            </div>
        </>
    )
}

const PageRowInstall = (prop: info) => {
    return (
        <>
            <div className="fs-5 border-0 b-red border-start border-4 ps-2 mb-2 fw-bold">安裝詳情</div>
            <div className="row justify-content-start ps-2 border-0 border-start border-4 border-white gy-3">
                <div className="col-auto me-4"><span className="text-secondary text-opacity-50">預約安裝日期</span><br />{prop.info?.appoDate}</div>
                <div className="col-auto me-4"><span className="text-secondary text-opacity-50">預約安裝時間</span><br />{prop.info?.appoTimeStr}</div>
                <div className="col-auto me-4"><span className="text-secondary text-opacity-50">地址</span><br />{prop.info?.addrDiff === "Y" ? prop.info.commsAddress : `${prop.info?.commsAddress} ${prop.info?.instFloor && prop.info?.instFloor + "樓"} ${prop.info?.instRoom && prop.info?.instRoom + "室"}`}</div>
            </div>
        </>
    )
}


const PageRowContact = (prop: info) => {
    return (
        <>
            <div className="fs-5 border-0 b-red border-start border-4 ps-2 mb-2 fw-bold">聯絡資料</div>
            <div className="row justify-content-start ps-2 border-0 border-start border-4 border-white gy-3">
                <div className="col-auto me-4"><span className="text-secondary text-opacity-50">通訊語言選擇</span><br />{prop.info?.commsLang == "C" ? "中文" : "英文"}</div>
                <div className="col-auto me-4"><span className="text-secondary text-opacity-50">手提電話</span><br />{prop.info?.contactMobile}</div>
                <div className="col-auto me-4"><span className="text-secondary text-opacity-50">住宅電話</span><br />{prop.info?.contactHome ? prop.info?.contactHome : "--"}</div>
                <div className="col-auto me-4"><span className="text-secondary text-opacity-50">電郵地址</span><br />{prop.info?.email}</div>
                <div className="col-auto me-4"><span className="text-secondary text-opacity-50">通訊地址</span><br />{prop.info?.addrDiff === "Y" ? prop.info.commsAddress : `${prop.info?.commsAddress} ${prop.info?.instFloor && prop.info?.instFloor + "樓"} ${prop.info?.instRoom && prop.info?.instRoom + "室"}`}</div>
            </div>
        </>
    )
}


const PageRowMonth = (prop: getPlanPaymentInfo) => {
    const Arr: CardInfoPage5[] = []
    return (
        <>
            <div className="fs-5 border-0 b-red border-start border-4 ps-2 mb-3 fw-bold">月費詳情</div>
            <>
                <div>
                    {prop.resultList.reduce((prevItem, currentItem) => {
                        console.log("currentItem", currentItem)
                        console.log("prevItem", prevItem)
                        const func = () => {
                            let y = true
                            for (let x of prevItem) {
                                if (x.Title === currentItem.groupName && currentItem.itemNum === 99) {
                                    x.Total = currentItem.priceStr
                                    x.Item = currentItem.itemName
                                    return
                                } else if (x.Title === currentItem.groupName) {
                                    x.Detail = [...x.Detail, { Content: currentItem.itemName, Amount: currentItem.priceStr, underLine: false }]
                                    y = false
                                    return
                                }
                            }
                            return y
                        }
                        if (func()) prevItem = [...prevItem, { Title: currentItem.groupName, Item: "", Total: "", Detail: [{ Content: currentItem.itemName, Amount: currentItem.priceStr, underLine: false }] }]
                        console.log("finalPrevItem", prevItem)
                        return prevItem
                    }, Arr).map((item, i) => {
                        return <div key={i} className="mb-3"><FeeCard Title={item.Title} Item={item.Item} Total={item.Total} Detail={item.Detail} /></div>
                    })}
                </div>
            </>

        </>
    )
}


const PageRowPayment = (prop: CombineRootState["cardInfo"]) => {
    const data = textMarshal({
        input: prop.body.card_no,
        template: "xxxx************",
        disallowCharacter: [/[a-z]/],
    })
    const MM = dayjs(prop.body.expiry_date).format('MM')
    const YY = dayjs(prop.body.expiry_date).format('YY')
    return (
        <>
            <div className="fs-5 border-0 b-red border-start border-4 ps-2 mb-2 fw-bold">付款資料</div>
            <div className="row justify-content-start ps-2 border-0 border-start border-4 border-white gy-3">
                <div className="col-auto me-4"><span className="text-secondary text-opacity-50">信用卡號碼</span><br />{data.marshaltext}</div>
                <div className="col-auto me-4"><span className="text-secondary text-opacity-50">信用卡種類</span><br />{getCreditCardNameByNumber(prop.body.card_no).toUpperCase()}</div>
                <div className="col-auto me-4"><span className="text-secondary text-opacity-50">持卡人姓名</span><br />{prop.body.card_holder}</div>
                <div className="col-auto me-4"><span className="text-secondary text-opacity-50">有效期至 </span><br />{`${MM}/${YY}`}</div>
            </div>
        </>
    )
}


const FeeCard = (props: CardInfoPage5) => {
    const [open, setOpen] = useState(false)
    return (
        <div className="rounded-4 shadow p-3">
            <div onClick={() => setOpen(state => !state)} className="d-flex justify-content-between align-items-center cursor">
                <div className="fs-6 fw-bold mb-2">{props.Item}</div>
                <div className="d-flex justify-content-between align-items-center">
                    <div className="fs-5 fw-bold dt-red me-3">{props.Total}</div>
                    {open ? <i className="fa-solid fa-angle-up text-secondary"></i> : <i className="fa-solid fa-angle-down text-secondary"></i>}
                </div>
            </div>
            <Collapse in={open}>
                <div>
                    <FeeItem key={"default"} Content={''} Amount={''} underLine={true} />
                    {props.Detail.map((item, i) => {
                        return <FeeItem key={i} Content={item.Content} Amount={item.Amount} underLine={item.underLine} />
                    })
                    }
                </div>
            </Collapse>
        </div>
    )
}


const FeeItem = (props: { Content: string, Amount: string, underLine: boolean }) => {
    return (
        <>
            <div className={`d-flex py-2 justify-content-between align-items-center ${props.underLine ? "border-0 border-bottom border-1 border-secondary border-o border-opacity-25" : ""}`}>
                <div className="fs-7">{props.Content || "項目"}</div>
                <div className="fs-7">{props.Amount || "金額 (HK$)"}</div>
            </div>
        </>
    )
}

function Spinner() {
    return (
        <div className="position-fixed start-50 top-50 translate-middle lds-spinner"><div></div><div></div><div></div>
            <div></div><div></div><div></div><div></div><div></div><div>
            </div><div></div><div></div><div></div>
        </div>
    )
}


const connector = connect(MaptoState,MaptoDispatch)
export default withRouter(connector(Page5Index));